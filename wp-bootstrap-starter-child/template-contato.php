<?php
/**
 * Template name: - Contatos Prestho
 */

get_header(); ?>

<section id="primary" class="content-area col-sm-12 col-lg-12">
    <main id="main" class="site-main" role="main">

        <?php
			while ( have_posts() ) : the_post(); ?>

        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

            <header class="entry-header">
                <h1>Contatos Prestho</h1>
            </header><!-- .entry-header -->

            <div class="entry-content" id="conteudo-sobre">

                <?php the_content(); ?>

                <ul class="lista-contatos">
                    <li>
                        <a href="tel:3433041481">
                            <span>(34) 3304-1481</span>
                            <small>Telefone</small>
                        </a>
                    </li>
                    <li>
                        <a href="mailto:contato@prestho.com.br">
                            <span>contato@prestho.com.br</span>
                            <small>E-mail</small>
                        </a>
                    </li>
                    <li>
                        <a href="https://m.me/Prestho.Digital" target="_blank">
                            <span>@Prestho.Digital</span>
                            <small>Facebook Messenger</small>
                        </a>
                    </li>
                    <!-- <li>
                        <a href="https://api.whatsapp.com/send?phone=5534999859559&text" target="_blank">
                            <span>(34) 9 9985-9559</span>
                            <small>Zap Zap</small>
                        </a>
                    </li> -->
                </ul>

                <div class="botoes amarelo outline">
                    <a href="#" onclick="back(); return false;">Voltar</a>
                </div>

            </div><!-- .entry-content -->

        </article><!-- #post-## -->
        <?php
			endwhile; // End of the loop.
			?>

    </main><!-- #main -->
</section><!-- #primary -->

<?php get_template_part( 'template-parts/content', 'script-fixed-menu' ); ?>
<?php
get_footer();
