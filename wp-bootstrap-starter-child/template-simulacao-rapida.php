<?php
/**
 * Template name: - Simulação Rápida
 */
get_header();

// if (isset($_REQUEST)) {
// 	$value = $_REQUEST['valor'];
// 	$agreement = $_REQUEST['convenio'];
// } else {
// 	$value = 5000;
// 	$agreement = 1;
// }



// $search = new Loan;
// $return = $search->loanSimulation($value, $agreement);


if (isset($_REQUEST['message'])) {
?>
<!-- Modal erro -->
<div class="modal fade fundo-amarelo" id="modal-erro" tabindex="-1" role="dialog" aria-labelledby="outro-valor-abel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <a href="<?php echo get_home_url(); ?>" class="link-home">
            <svg><use xlink:href="#logo-prestho"></use></svg>
        </a>
      </div>
      <div class="modal-body">
        <h5>Opa!</h5>
        <h6>Algo saiu errado.</h6>
        <svg class="alerta"><use xlink:href="#alerta"></use></svg>
        <p>Estamos com um problema técnico. 
        Tente novamente mais tarde.</p>

        <a class="link-voltar" href="<?php echo get_home_url(); ?>">VOLTAR</a>
        <button class="link-info" type="button" data-toggle="collapse" data-target="#erro-api" aria-expanded="false">
		    <svg><use xlink:href="#info"></use></svg>
		</button>

		<div class="collapse" id="erro-api">
		  <div class="card card-body">
		    <pre style="white-space: pre-line;">
		    	<?php print_r($_REQUEST); ?>
		    </pre>
		  </div>
		</div>
      </div>
    </div>
  </div>
</div>

<script>
	jQuery(function($){
		$('#modal-erro').appendTo('body').modal('show');
	});
</script>
<?php
} else {

?>


<section id="primary" class="content-area col-sm-12 col-lg-12 simulacao-rapida"> 
	<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
				<div class="entry-content">
					<div class="prestho-card">
						<h1>Empréstimo Consignado</h1>
						<p><span>R$<?php echo number_format($_REQUEST['valorEmprestimo'], 2, ',', '.'); ?></span></p>
						<p><?php echo $_REQUEST['prazo']; ?> parcelas de</p>
						<p class="limite">R$<?php echo number_format($_REQUEST['valorParcela'], 2, ',', '.'); ?></p>
						<hr/>
						<div class="botoes-acao">
							<a href="#" class="repetir-simulacao" data-toggle="modal" data-target="#outro-valor">Outro Valor</a><a href="#" class="contratar-agora" data-value="<?php echo $_REQUEST['valorEmprestimo']; ?>">Contratar Agora</a>
						</div>
					</div>
					<p class="observacao">*Valores sujeitos à análise de crédito, cadastro e margem consignável disponível.</p>
				</div><!-- .entry-content -->

			</article><!-- #post-## -->
		<?php
		endwhile; // End of the loop.
		?>

	</main><!-- #main -->
</section><!-- #primary -->

<?php get_template_part( 'template-parts/content', 'script-fixed-menu' ); ?>
<?php
}

get_footer();