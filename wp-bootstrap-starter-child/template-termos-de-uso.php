<?php
/**
 * Template name: - Termos de Uso
 */

get_header(); ?>

	<section id="primary" class="content-area col-sm-12 col-lg-12">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					
				    <header class="entry-header">
						<h1>TERMOS DE USO E POLÍTICAS DE PRIVACIDADE DA PLATAFORMA DE SERVIÇOS FINANCEIROS E CRÉDITO CONSIGNADO – 
						PRESTHO DIGITAL LTDA</h1>
					</header><!-- .entry-header -->
				   
					<div class="entry-content" id="conteudo-sobre">
						
						<?php the_content(); ?>
					</div><!-- .entry-content -->
					<div class="row">
						<div class="col-4 offset-4">
							<a class="link-voltar-auto" href="#">Voltar</a>
						</div>
					</div>
				</article><!-- #post-## -->
			<?php
			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php get_template_part( 'template-parts/content', 'script-fixed-menu' ); ?>
<?php
get_footer();
