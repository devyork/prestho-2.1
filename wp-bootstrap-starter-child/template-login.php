<?php
/**
 * Template name: - Login
 */

get_header(); 

if(isset($_REQUEST['autologin'])) {
	echo '<div class="autologin" data-loginCliente="'.$_SESSION['pedido']['loginCliente'].'" data-passwordCliente="'.$_SESSION['pedido']['passwordCliente'].'"></div>';
}
?>

	<section id="primary" class="content-area col-sm-12 col-lg-12">
		<main id="main" class="site-main" role="main" style="max-width: 100% !important;">

			<?php
			while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
					<div class="entry-content">
						<div class="container p-0">
							<div class="row p-0">
								<div class="col-md-12 d-none d-lg-block col-lg-4 mr-lg-2 offset-lg-0 apps-login pl-lg-5">
									<h2>Acesse sua área
										na Prestho e
										acompanhe a 
										situação de suas
										solicitações de
										empréstimo.</h2>
										<p>Empréstimo Consignado 100% online. <strong>Em suas mãos pro que der e vier</strong></p>
										<p><strong>Clique e baixe grátis o APP e tenha a ajuda que precisa a qualquer hora</strong></p>
										<?php get_template_part( 'template-parts/content', 'apps-links' ); ?>
								</div>
								<div class="col-md-12 col-lg-4 ml-lg-4">
									<form action="#" method="post" class="prestho-form" id="login-prestho">
										<div class="box-login">
											<h1>Minha Conta <span>Entrar</span></h1>
											<fieldset>
												<label for="field-cpf" class="field">
													<span>CPF</span>
													<input autocomplete="off" type="tel" pattern="\d{3}\.\d{3}\.\d{3}-\d{2}" name="usuario" id="field-cpf" autocomplete="wewrwer" required>
													<b class="response">CPF Inválido</b>
												</label>
												<label for="field-senha" class="field field-senha info">
													<span>4 últimos dígitos do celular</span>
													<input autocomplete="off" type="password" pattern="\d{4}" name="senha" id="field-senha" maxlength="4" placeholder="" autocomplete="wewrwer" required>
													<a href=""  title="4 últimos dígitos do celular" data-content="É importante que você informe o mesmo celular informado no ato da contratação do seu empréstimo." class="open-toastr"></a>
												</label>
												<a href="#" id="recuperar-senha">Esqueci Minha Senha</a>
											</fieldset>
										</div>

										<div class="botoes-acao">
											<button type="submit">Continuar</button><a href="<?php echo get_home_url(); ?>">Fazer uma simulação de empréstimo</a>
											<p>Fique tranquilo, nossa plataforma é segura.</p>
										</div>
									</form>

									<div class="cookies">
										<p>Nossos sistemas utilizam cookies para promover uma melhor navegação para você. Ao continuar, você concorda com sua utilização, <a href="<?php echo get_home_url(); ?>/termos-de-uso-politicas-de-privacidade/" target="_blank">Política de Privacidade e Termos de Uso do Serviço</a></p>
									</div>

									<div class="problemas">
										<a href="#" class="open-chat">Problemas para acessar a Prestho?</a>
									</div>
								</div>
							</div>
						</div>
					</div><!-- .entry-content -->

				</article><!-- #post-## -->
			<?php
			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php get_template_part( 'template-parts/content', 'script-fixed-menu' ); ?>
<?php
get_footer();
