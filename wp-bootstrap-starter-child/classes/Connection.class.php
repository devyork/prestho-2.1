<?php 	
/**
 * Classe Connection
 *
 * Responsável por realizar a conexão com a API do Finaz
 *
 * @author Adriano dos Santos <adriano@york.digital>
 * @copyright 2019 York Digital
 */

class Connection
{

    protected $url = FINAZ_URL;
    protected $key = FINAZ_KEY;
    protected $clientKey = FINAZ_CLIENT_KEY;

	private $ch;
    private $data;
    private $chReturn;

    protected function getData($endpoint, $data = '') {
        if (gettype($data) == 'array') {

            $this->ch = curl_init($this->url . $endpoint);
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                'Finaz-Auth : ' . $this->key, 
                'Content-Type: application/json'
            ));
            curl_setopt($this->ch, CURLOPT_TIMEOUT, 120);
            curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, 120);
            curl_setopt($this->ch, CURLOPT_POST, 1);
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);

            return json_decode(curl_exec($this->ch));
        } else {

            $this->ch = curl_init($this->url . $endpoint);
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                'Finaz-Auth : ' . $this->key
            ));
            curl_setopt($this->ch, CURLOPT_TIMEOUT, 120);
            curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, 120);
            curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);

            return json_decode(curl_exec($this->ch));

        }
    }

    protected function login($data) {
        $this->ch = curl_init($this->url . '/Cliente/Auth');
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'Finaz-Auth : ' . $this->clientKey, 
            'Content-Type: application/json'
        ));
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 120);
        curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, 120);
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);

        return json_decode(curl_exec($this->ch));

        // return json_encode($data);
    }


    protected function returnAccount($token) {
        $this->ch = curl_init($this->url . '/Cliente/Cadastro');
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'Finaz-Auth : ' . $this->clientKey, 
            'Cliente-Auth : ' . $token, 
            'Content-Type: application/json'
        ));
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 120);
        curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, 120);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);

        return json_decode(curl_exec($this->ch));

        // return json_encode($data);
    }


    protected function returnUserLoans($token) {
        $this->ch = curl_init($this->url . '/Cliente/Emprestimos');
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'Finaz-Auth : ' . $this->clientKey, 
            'Cliente-Auth : ' . $token, 
            'Content-Type: application/json'
        ));
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 120);
        curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, 120);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);

        return json_decode(curl_exec($this->ch));

        // return json_encode($data);
    }


    protected function setData($endpoint, $data = '') {
        if (gettype($data) == 'array') {
            
            $this->ch = curl_init($this->url . $endpoint);
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                'Finaz-Auth : ' . $this->key, 
                'Content-Type: application/json'
            ));
            curl_setopt($this->ch, CURLOPT_TIMEOUT, 120);
            curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, 120);
            curl_setopt($this->ch, CURLOPT_POST, 1);
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);

            return json_decode(curl_exec($this->ch));
        } else {

            $this->ch = curl_init($this->url . $endpoint);
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
                'Finaz-Auth : ' . $this->key
            ));
            curl_setopt($this->ch, CURLOPT_TIMEOUT, 120);
            curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, 120);
            curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);

            return json_decode(curl_exec($this->ch));

        }
    }


    protected function uploadFile($endpoint, $data) {
        $this->ch = curl_init($this->url . $endpoint);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'Finaz-Auth : ' . $this->key
        ));
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 600);
        curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, 600);
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);

        return json_decode(curl_exec($this->ch));
    }

}

?>