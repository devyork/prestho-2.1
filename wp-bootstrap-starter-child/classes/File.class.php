<?php 
/**
 * Classe File
 *
 * Realiza a manipulação de arquivos
 *
 * @author Adriano dos Santos <adriano@york.digital>
 * @copyright 2019 York Digital
 */

class File
{
	protected $handle;

	function createTxtFile($filename, $data) {
		
		if(file_exists($filename)) {
			unlink($filename);
		}

		$this->handle = fopen($filename, 'w') or die('Cannot open file:  '.$filename);
		
		fwrite($this->handle, $data);
		fclose($this->handle);
	}
}
?>