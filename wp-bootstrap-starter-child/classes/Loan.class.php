<?php 	
/**
 * Classe Loan
 *
 * Realiza todas as funções referentes a empréstimos
 *
 * @author Adriano dos Santos <adriano@york.digital>
 * @copyright 2019 York Digital
 */

class Loan extends Connection
{
    /*
        1, 19 = OLÉ CONSIGNADO
        22 = SAQUE COMPLEMENTAR
        28 = CARTAO + SAQUE
    */ 
    protected $availableProd = array(1,19,22,28);
    protected $customNames = array(
                                1  => 'Empréstimo Consignado',
                                19 => 'Empréstimo Consignado',
                                22 => 'Saque Complementar',
                                28 => 'Cartão de Crédito Consignado'
                            );

    public    $value;
	public    $cpf;
	public    $agreement;
    protected $loanObj;
    protected $arr = array();
    protected $loanLimit;




    function groupByBenefits($loanObj, $filter, $count = 0) {
        
        foreach ($loanObj['produtos'] as $key => $product) {

            if (in_array($product['tipoOperacaoId'], $filter)) { 

               

               foreach ($product['valores'] as $k => $value) {

                $count++;
                    
                    $this->arr[$value['beneficio']][$key.$k]['descricao'] = $this->customNames[$product['tipoOperacaoId']];
                    $this->arr[$value['beneficio']][$key.$k]['produto'] = $value;
                    $this->arr[$value['beneficio']][$key.$k]['produto']['prodId'] = $product['tipoOperacaoId'];

                    if ($product['tipoOperacaoId'] == 1 || $product['tipoOperacaoId'] == 19) {
                        $this->arr[$value['beneficio']][$key.$k]['produto']['tipo'] = "valorEmprestimo";
                    } else {
                        $this->arr[$value['beneficio']][$key.$k]['produto']['tipo'] = "valorSaque";
                    }
                    ksort($this->arr, SORT_NUMERIC);
                    
                }   
            }

        }

        return array('numResultados' => $count, 'conjuntoId' => $loanObj['conjuntoId'], $this->arr);
    }

    function calculateLimit($obj, $filter, $val = 0) {

        foreach ($obj as $product) {

            if (in_array($product['tipoOperacaoId'], $filter)) { 
               foreach ($product['valores'] as $value) {
                    if ($value['valorLiquidoSeguro'] !== null) {
                        $val += $value['valorLiquidoSeguro'];
                    } else {
                        $val += $value['valorLiquido'];
                    }
               }
            }
        }

        return $val;
    }

    function loanReturnLimit($cpf, $agreement = 1, $filter = null) {

        if ($filter == null) {
            $filter = $this->availableProd;
        }

        $this->loanObj = json_decode(json_encode($this->getData('/Simulacao/'.$cpf.'/'.$agreement)), True);
        $this->loanLimit =  $this->calculateLimit($this->loanObj['produtos'], $filter);
        $this->loanObj = $this->groupByBenefits($this->loanObj, $filter);

        return array('limit' => $this->loanLimit, 'benefits' => $this->loanObj);

        // return $this->loanLimit;
    }


    function loanOptions($cpf, $agreement = 1, $filter = null) {

        if ($filter == null) {
            $filter = $this->availableProd;
        }

        $this->loanObj = json_decode(json_encode($this->getData('/Simulacao/'.$cpf.'/'.$agreement)), True);
        return $this->groupByBenefits($this->loanObj, $filter);

    }

    function loanSimulation($value = 300, $agreement = 1) {
        $this->loanObj = $this->getData('/Simulacao/' . $agreement . '/novo', array('valorEmprestimo' => $value));
        $this->loanObj = json_decode(json_encode($this->loanObj), True);
        
        return $this->loanObj;

    }

    function changeLoanValue($type, $value = 300, $conjID, $simID) {
        $this->loanObj = $this->getData('/Simulacao/' . $conjID . '/' . $simID, array($type => $value));
        $this->loanObj = json_decode(json_encode($this->loanObj), True);
        
        return $this->loanObj;

    }

    function sendLoanProposal($data) {
        $this->loanObj = $this->setData('/Proposta/', $data);
        $this->loanObj = json_decode(json_encode($this->loanObj), True);
        
        return $this->loanObj;
    }

    function sendDocuments($id, $data) {
        $this->loanObj = $this->uploadFile('/Documento/AnexarImagem/Negociacao/' . $id, $data);
        $this->loanObj = json_decode(json_encode($this->loanObj), True);
        
        return $this->loanObj;
    }

    function sendDocumentsByOp($id, $data) {
        $this->loanObj = $this->uploadFile('/Documento/AnexarImagem/Operacao/' . $id, $data);
        $this->loanObj = json_decode(json_encode($this->loanObj), True);
        
        return $this->loanObj;
    }

    function sendTextFile($id, $data) {
        $this->loanObj = $this->uploadFile('/Documento/AnexarTexto/Negociacao/' . $id, $data);
        $this->loanObj = json_decode(json_encode($this->loanObj), True);
        
        return $this->loanObj;
    }
    
}

?>