<?php 	
/**
 * Classe User
 *
 * Realiza as consultas referentes ao usuário cadastrado
 *
 * @author Adriano dos Santos <adriano@york.digital>
 * @copyright 2019 York Digital
 */

class User extends Connection
{

    function checkCredentials($user, $password) {        
        return $this->login(array('login' => $user, 'password' => $password));
    }

    function dataAccount($token) {        
        return $this->returnAccount($token);
    }

    function dataLoan($token) {        
        return $this->returnUserLoans($token);
    }
}

?>