<?php
/**
 * Template name: - Meus Empréstimos
 */

get_header('area-cliente'); 

$loanData = new User;
$returnLoan = $loanData->dataLoan($_REQUEST['token']);
$returnLoan = json_decode(json_encode($returnLoan), True);

/****************

	[situacao] => Array (
	    [codigo] => 84057
	    [descricao] => Proposta Reprovada
	)

	84057 - Proposta Reprovada
	84058 - Proposta Aprovada
	84059 - Proposta em Análise
	84061 - Solicitação Recebida
	88433 - Crédito Enviado
	102223 - Proposta Pendente - Selfie
	102224 - Proposta Pendente - RG/CNH
	102225 - Proposta Pendente - Selfie e RG/CNH

******************/ 
$mock = false;
$mock_status = 102224;

$banco = array(
  "001"=> "Banco do Brasil",
  "003"=> "Banco da Amazônia",
  "004"=> "Banco do Nordeste",
  "021"=> "Banestes",
  "025"=> "Banco Alfa",
  "027"=> "Besc",
  "029"=> "Banerj",
  "031"=> "Banco Beg",
  "033"=> "Banco Santander Banespa",
  "036"=> "Banco Bem",
  "037"=> "Banpará",
  "038"=> "Banestado",
  "039"=> "BEP",
  "040"=> "Banco Cargill",
  "041"=> "Banrisul",
  "044"=> "BVA",
  "045"=> "Banco Opportunity",
  "047"=> "Banese",
  "062"=> "Hipercard",
  "063"=> "Ibibank",
  "065"=> "Lemon Bank",
  "066"=> "Banco Morgan Stanley Dean Witter",
  "069"=> "BPN Brasil",
  "070"=> "Banco de Brasília – BRB",
  "072"=> "Banco Rural",
  "073"=> "Banco Popular",
  "074"=> "Banco J. Safra",
  "075"=> "Banco CR2",
  "076"=> "Banco KDB",
  "077"=> "Banco Inter",
  "096"=> "Banco BMF",
  "104"=> "Caixa Econômica Federal",
  "107"=> "Banco BBM",
  "116"=> "Banco Único",
  "121"=> "Banco Agibank",
  "151"=> "Nossa Caixa",
  "175"=> "Banco Finasa",
  "184"=> "Banco Itaú BBA",
  "204"=> "American Express Bank",
  "208"=> "Banco Pactual",
  "212"=> "Banco Matone",
  "213"=> "Banco Arbi",
  "214"=> "Banco Dibens",
  "217"=> "Banco Joh Deere",
  "218"=> "Banco Bonsucesso",
  "222"=> "Banco Calyon Brasil",
  "224"=> "Banco Fibra",
  "225"=> "Banco Brascan",
  "229"=> "Banco Cruzeiro",
  "230"=> "Unicard",
  "233"=> "Banco GE Capital",
  "237"=> "Bradesco",
  "241"=> "Banco Clássico",
  "243"=> "Banco Stock Máxima",
  "246"=> "Banco ABC Brasil",
  "248"=> "Banco Boavista Interatlântico",
  "249"=> "Investcred Unibanco",
  "250"=> "Banco Schahin",
  "252"=> "Fininvest",
  "254"=> "Paraná Banco",
  "260"=> "Nubank",
  "263"=> "Banco Cacique",
  "265"=> "Banco Fator",
  "266"=> "Banco Cédula",
  "300"=> "Banco de la Nación Argentina",
  "318"=> "Banco BMG",
  "320"=> "Banco Industrial e Comercial",
  "356"=> "ABN Amro Real",
  "341"=> "Itau",
  "347"=> "Sudameris",
  "351"=> "Banco Santander",
  "353"=> "Banco Santander Brasil",
  "366"=> "Banco Societe Generale Brasil",
  "370"=> "Banco WestLB",
  "376"=> "JP Morgan",
  "389"=> "Banco Mercantil do Brasil",
  "394"=> "Banco Mercantil de Crédito",
  "399"=> "HSBC",
  "409"=> "Unibanco",
  "412"=> "Banco Capital",
  "422"=> "Banco Safra",
  "453"=> "Banco Rural",
  "456"=> "Banco Tokyo Mitsubishi UFJ",
  "464"=> "Banco Sumitomo Mitsui Brasileiro",
  "477"=> "Citibank",
  "479"=> "Itaubank (antigo Bank Boston)",
  "487"=> "Deutsche Bank",
  "488"=> "Banco Morgan Guaranty",
  "492"=> "Banco NMB Postbank",
  "494"=> "Banco la República Oriental del Uruguay",
  "495"=> "Banco La Provincia de Buenos Aires",
  "505"=> "Banco Credit Suisse",
  "600"=> "Banco Luso Brasileiro",
  "604"=> "Banco Industrial",
  "610"=> "Banco VR",
  "611"=> "Banco Paulista",
  "612"=> "Banco Guanabara",
  "613"=> "Banco Pecunia",
  "623"=> "Banco Panamericano",
  "626"=> "Banco Ficsa",
  "630"=> "Banco Intercap",
  "633"=> "Banco Rendimento",
  "634"=> "Banco Triângulo",
  "637"=> "Banco Sofisa",
  "638"=> "Banco Prosper",
  "643"=> "Banco Pine",
  "652"=> "Itaú Holding Financeira",
  "653"=> "Banco Indusval",
  "654"=> "Banco A.J. Renner",
  "655"=> "Banco Votorantim",
  "707"=> "Banco Daycoval",
  "719"=> "Banif",
  "721"=> "Banco Credibel",
  "734"=> "Banco Gerdau",
  "735"=> "Banco Pottencial",
  "738"=> "Banco Morada",
  "739"=> "Banco Galvão de Negócios",
  "740"=> "Banco Barclays",
  "741"=> "BRP",
  "743"=> "Banco Semear",
  "745"=> "Banco Citibank",
  "746"=> "Banco Modal",
  "747"=> "Banco Rabobank International",
  "748"=> "Banco Cooperativo Sicredi",
  "749"=> "Banco Simples",
  "751"=> "Dresdner Bank",
  "752"=> "BNP Paribas",
  "753"=> "Banco Comercial Uruguai",
  "755"=> "Banco Merrill Lynch",
  "756"=> "Banco Cooperativo do Brasil",
  "757"=> "KEB"
);
?>

	<section id="primary" class="content-area col-sm-12 col-lg-12">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
					<div class="entry-content">
						
		                <div id="accordion">

						<?php 
							$aviso_fotos = false;

							foreach ($returnLoan as $key => $value) {

								if($mock == true) {
									// /* mock do status */
									$value['situacao'] = array(
										"codigo" => $mock_status,
										"descricao" => ""
									);
								}
						?>

							<?php
								// selfie
								if($value['situacao']['codigo'] == 102223 && $aviso_fotos == false):
							?>

							<div class="card alerta-fotos">
								<div class="card-header">
								<a href="<?php echo get_home_url(); ?>/documentos/" class="send-photos" data-missing="selfie" data-operation="<?php echo $value['operacaoId']; ?>"></a>
									<h4>Para aprovação de sua proposta, envie aqui pra gente uma selfie e as fotos dos seus documentos.</h4>
									<span class="camera">
										<i></i>
									</span>
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/svg/curva-branca.svg" alt="">
									<div class="curva-amarela"></div>
								</div>
							</div>
							<?php $aviso_fotos = true; ?>

							<?php
								// rg
								elseif($value['situacao']['codigo'] == 102224 && $aviso_fotos == false):
							?>

							<div class="card alerta-fotos">
								<div class="card-header">
								<a href="<?php echo get_home_url(); ?>/documentos/" class="send-photos" data-missing="rg" data-operation="<?php echo $value['operacaoId']; ?>"></a>
									<h4>Para aprovação de sua proposta, envie aqui pra gente uma selfie e as fotos dos seus documentos.</h4>
									<span class="camera">
										<i></i>
									</span>
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/svg/curva-branca.svg" alt="">
									<div class="curva-amarela"></div>
								</div>
							</div>
							<?php $aviso_fotos = true; ?>

							<?php
								// selfie e rg
								elseif($value['situacao']['codigo'] == 102225 && $aviso_fotos == false):
									
							?>

							<div class="card alerta-fotos">
								<div class="card-header">
									<a href="<?php echo get_home_url(); ?>/documentos/" class="send-photos" data-missing="selfie-e-rg" data-operation="<?php echo $value['operacaoId']; ?>"></a>
									<h4>Para aprovação de sua proposta, envie aqui pra gente uma selfie e as fotos dos seus documentos.</h4>
									<span class="camera">
										<i></i>
									</span>
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/svg/curva-branca.svg" alt="">
									<div class="curva-amarela"></div>
								</div>
							</div>
							<?php $aviso_fotos = true; ?>

							<?php
								else:
							?>
							<?php
								endif;
							?>

						<?php
							}

		                	foreach ($returnLoan as $key => $value) {

		                		/****************

		                			[situacao] => Array (
		                			    [codigo] => 84057
		                			    [descricao] => Proposta Reprovada
		                			)

		                			84057 - Proposta Reprovada
		                			84058 - Proposta Aprovada
		                			84059 - Proposta em Análise
		                			84061 - Solicitação Recebida
									88433 - Crédito Enviado
									102223 - Proposta Pendente - Selfie
									102224 - Proposta Pendente - RG/CNH
									102225 - Proposta Pendente - Selfie e RG/CNH

		                		******************/
		                		if (empty($value['situacao'])) {
		                			$value['situacao'] = array(
									    "codigo" => "84061",
									    "descricao" => "Solicitação Recebida"
									);
								}
								
								if($mock == true) {
									// /* mock do status */
									$value['situacao'] = array(
										"codigo" => $mock_status,
										"descricao" => ""
									);
								}

								
								

		                		switch ($value['tipoOperacao']['codigo']) {
		                			case 1:
		                				$tipoOperacao = "Empréstimo Consignado";
		                				$style = "";
		                				break;
		                			
		                			case 19:
		                				$tipoOperacao = "Empréstimo Consignado";
		                				$style = "";
		                				break;
		                			
		                			
		                			case 22:
		                				$tipoOperacao = "Saque Complementar";
		                				$style = "";
		                				break;
		                			
		                			
		                			case 28:
		                				$tipoOperacao = "Cartão de Crédito Consignado";
		                				$style = "";
		                				break;
		                			
		                			
		                			case 31:
		                				$tipoOperacao = "Crédito Conta";
		                				$style = ' style="display: none !important;"';
		                				break;
		                			
		                			default:
		                				$tipoOperacao = "Empréstimo Consignado";
		                				$style = "";
		                				break;
		                		}

		                		switch ($value['convenio']) {
		                			case 1:
		                				$fontePagadora = "INSS";
		                				break;
		                			
		                			case 5:
		                				$fontePagadora = "SIAPE";
		                				break;

		                			default:
		                				$fontePagadora = "INSS";
		                				break;
		                		}

		                		$data = explode('T', $value['dataHora']);
		                		$data = $data[0];

		                		setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
								$data = ucfirst( utf8_encode( strftime("%d de %B de %Y", strtotime($data) ) ) );
								
		                		
						?>

		                	<!-- início card -->
		                	<div class="card"<?php echo $style; ?>>
		                	  <div class="card-header" id="<?php echo $value['operacaoId']; ?>">
		                	    <h5 class="mb-0">
		                	      <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-<?php echo $value['operacaoId']; ?>" aria-expanded="true" aria-controls="collapse-<?php echo $value['operacaoId']; ?>"><svg><use xlink:href="#seta-elemento"></use></svg></button>
		                	      <small>Data do empréstimo: <?php echo $data; ?></small>
		                	      <strong><?php echo $tipoOperacao; ?></strong>
		                	      <span><i>R$</i><u><?php echo number_format($value['valores']['valorLiquido'], 2, ',', '.'); ?></u></span>
		                	    </h5>


		                	    <?php 

		                	    	switch ($value['situacao']['codigo']) {
		                	    		case '84061':
		                	    			$classe1 = "inativo";
		                	    			$classe2 = "inativo";
		                	    			$classe3 = "inativo";
		                	    			$classe4 = "atual";
		                	    			$textoAprovacao = "Aprovada";
		                	    			break;

		                	    		case '84059':
		                	    			$classe1 = "inativo";
		                	    			$classe2 = "inativo";
		                	    			$classe3 = "atual";
		                	    			$classe4 = "";
		                	    			$textoAprovacao = "Aprovada";
		                	    			break;
		                	    			
		                	    		case '84058':
		                	    			$classe1 = "inativo";
		                	    			$classe2 = "aprovado";
		                	    			$classe3 = "";
		                	    			$classe4 = "";
		                	    			$textoAprovacao = "Aprovada";
		                	    			break;
		                	    			
		                	    		case '84057':
		                	    			$classe1 = "inexistente";
		                	    			$classe2 = "reprovado";
		                	    			$classe3 = "";
		                	    			$classe4 = "";
		                	    			$textoAprovacao = "Reprovada";
		                	    			break;
		                	    			
		                	    		case '88433':
		                	    			$classe1 = "atual";
		                	    			$classe2 = "";
		                	    			$classe3 = "";
		                	    			$classe4 = "";
		                	    			$textoAprovacao = "Aprovada";
		                	    			break;
		                	    		// Selfie
		                	    		case '102223':
		                	    			$classe1 = "inativo";
		                	    			$classe2 = "inativo";
		                	    			$classe3 = "inativo";
		                	    			$classe4 = "";
		                	    			$textoAprovacao = "Aprovada";
		                	    			break;
		                	    		// RG ou CNh
		                	    		case '102224':
		                	    			$classe1 = "inativo";
		                	    			$classe2 = "inativo";
		                	    			$classe3 = "inativo";
		                	    			$classe4 = "";
		                	    			$textoAprovacao = "Aprovada";
		                	    			break;
		                	    		// Selfie, RG ou CNH
		                	    		case '102225':
		                	    			$classe1 = "inativo";
		                	    			$classe2 = "inativo";
		                	    			$classe3 = "inativo";
		                	    			$classe4 = "";
		                	    			$textoAprovacao = "Aprovada";
		                	    			break;
		                	    		
		                	    		default:
		                	    			$classe1 = "inativo";
		                	    			$classe2 = "inativo";
		                	    			$classe3 = "inativo";
		                	    			$classe4 = "atual";
		                	    			$textoAprovacao = "Aprovada";
		                	    			break;
		                	    	}

		                	    ?>

		                	    <div class="situacao-proposta">
		                	    	<?php if($value['situacao']['codigo'] == 84057): ?>
		                	    	<div class="info-reprovacao">
		                	    		<a href="#">Entenda o porquê</a>
		                	    	</div>
		                	    	<?php endif; ?>

		                	    	<div class="timeline">
		                	    		<div class="entry-status <?php echo $classe1; ?>">
			                	    		<div class="title-status">
		                	    		      <p>Crédito Enviado</p>
		                	    		    </div>
		                	    		    <div class="info-status">
		                	    		    	<strong>Ótima notícia!</strong><br>
		                	    		    	<span>O Crédito de sua proposta já foi enviado para sua conta e está disponível para saque.</span>
		                	    		    </div>
		                	    		</div>
		                	    		<div class="entry-status <?php echo $classe2; ?>">
			                	    		<div class="title-status">
		                	    		      <p>Proposta <?php echo $textoAprovacao; ?></p>
		                	    		    </div>
		                	    		    <?php if($value['situacao']['codigo'] == 84057): ?>
		                	    		    <div class="info-status">
		                	    		    	<strong>Sua proposta foi reprovada.</strong><br>
		                	    		    	<span>Nossos especialistas podem dar detalhes.</span>
		                	    		    	<a href="#" class="open-chat">Chame no Chat</a>
		                	    		    </div>
		                	    		    <?php else: ?>
	                	    		    	<div class="info-status">
	                	    		    		<strong>Pode relaxar!</strong><br>
	                	    		    		<span>Sua proposta foi aprovada!</span>
	                	    		    	</div>
		                	    		    <?php endif; ?>
		                	    		</div>
		                	    		<div class="entry-status <?php echo $classe3; ?>">
			                	    		<div class="title-status">
		                	    		      <p>Proposta em Análise</p>
		                	    		    </div>
		                	    		    <div class="info-status">
		                	    		    	<strong>Oba! Sua proposta já está em análise.</strong><br>
		                	    		    	<span>Agora é só aguardar a aprovação do Banco e do órgão pagador.</span>
		                	    		    </div>
		                	    		</div>
										<?php
											if($value['situacao']['codigo'] == 102223):
										?>

										<div class="entry-status atual alerta">
			                	    		<div class="title-status">
		                	    		      <p>Aguardando fotos</p> 
		                	    		    </div>
		                	    		    <div class="info-status">
		                	    		    	<strong>Selfie</strong><br>
		                	    		    	<span>Para aprovação, envie uma foto do seu rosto pra gente.</span>
												<a href="<?php echo get_home_url(); ?>/documentos/" class="send-photos" data-missing="selfie" data-operation="<?php echo $value['operacaoId']; ?>">Enviar foto</a>
		                	    		    </div>
		                	    		</div>

										<?php
											elseif($value['situacao']['codigo'] == 102224):
										?>

										<div class="entry-status atual alerta">
			                	    		<div class="title-status">
		                	    		      <p>Aguardando fotos</p>
		                	    		    </div>
		                	    		    <div class="info-status">
		                	    		    	<strong>RG ou CNH</strong><br>
		                	    		    	<span>Para aprovação, envie foto legível do seu documento de identificação.</span>
												<a href="<?php echo get_home_url(); ?>/documentos/" class="send-photos" data-missing="rg" data-operation="<?php echo $value['operacaoId']; ?>">Enviar foto</a>
		                	    		    </div>
		                	    		</div>

										<?php
											elseif($value['situacao']['codigo'] == 102225):
										?>

										<div class="entry-status atual alerta">
			                	    		<div class="title-status">
		                	    		      <p>Aguardando fotos</p>
		                	    		    </div>
		                	    		    <div class="info-status">
		                	    		    	<strong>Selfie, RG ou CNH</strong><br>
		                	    		    	<span>Para aprovação, envie uma selfie (foto do rosto) e foto legível do seu documento de identificação.</span>
												<a href="<?php echo get_home_url(); ?>/documentos/" class="send-photos" data-missing="selfie-e-rg" data-operation="<?php echo $value['operacaoId']; ?>">Enviar foto</a>
		                	    		    </div>
		                	    		</div>

										<?php
											else:
										?>
										<?php
											endif;
										?>


		                	    		<div class="entry-status <?php echo $classe4; ?>">
			                	    		<div class="title-status">
		                	    		      <p>Solicitação Recebida</p>
		                	    		    </div>
		                	    		    <div class="info-status">
		                	    		    	<strong>Uhuuu!</strong>
		                	    		    	<span>A gente já recebeu sua proposta e logo seguirá para análise.</span>
		                	    		    </div>
		                	    		</div>
		                	    	</div>
		                	    </div>

		                	    <div class="center">
									<button class="btn btn-link" data-toggle="collapse" data-target="#collapse-<?php echo $value['operacaoId']; ?>" aria-expanded="true" aria-controls="collapse-<?php echo $value['operacaoId']; ?>">
										MAIS DETALHES
									</button>
								</div>
		                	  </div>

		                	  <div id="collapse-<?php echo $value['operacaoId']; ?>" class="collapse collapse-meus-emprestimos" aria-labelledby="<?php echo $value['operacaoId']; ?>">
		                	    <div class="card-body">

		                	      <div class="card-body-content">
		                	      	<h5>Detalhes do seu Empréstimo</h5>

		                	      	<div class="row detalhes-emprestimo no-gutters">
		                	      	  <div class="col-6">Depósito: <strong><?php echo $banco[$value['pagamento']['bancoCod']]; ?></strong></div>
		                	      	  <div class="col-6">Agência: <strong><?php echo $value['pagamento']['agencia']; ?></strong></div>
		                	      	  <div class="col-6">Conta: <strong><?php echo $value['pagamento']['conta'] . "-" . $value['pagamento']['digito']; ?></strong></div>
		                	      	  <div class="col-6">Fonte pagadora: <strong><?php echo $fontePagadora; ?></strong></div>
		                	      	</div>

		                	      	
		                	      	<!-- início proposta -->
		                	      	<div class="proposta">

		                	      	  <div class="item-proposta">
		                	      	    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/svg/bancos/<?php echo $value['bancoCod']; ?>.svg" class="logo-banco">
		                	      	    <p><?php echo $tipoOperacao; ?></p>

		                	      	    <div class="valor-total-emprestimo">
		                	      	      <i>R$</i><span><?php echo number_format($value['valores']['valorLiquido'], 2, ',', '.'); ?></span>
		                	      	    </div>
		                	      	    
		                	      	    <?php if($value['tipoOperacao']['codigo'] == 1 || $value['tipoOperacao']['codigo'] == 19): ?>
		                	      	    <div class="parcelamento"><span>Pagto. em <?php echo $value['valores']['prazo']; ?>x de R$<?php echo number_format($value['valores']['valorParcela'], 2, ',', '.'); ?></span></div>
		                	      	    <?php endif; ?>
										<?php if((int)$value['bancoCod'] == 955){ //955 == Olé, 318 == BMG ?>

		                	      	    <div class="taxas  d-none">
		                	      	      <div class="linha">
		                	      	        <div class="taxa"><span>2,05%</span><small>TAXA MÁXIMA (A.M.)</small></div>
		                	      	        <div class="taxa"><span>27,57%</span><small>TAXA MÁXIMA (A.A.)</small></div>
		                	      	        <!-- <div class="taxa"><span>1,75%</span><small>CET (A.M.)</small></div>
		                	      	        <div class="taxa"><span>23,54%</span><small>CET (A.A.)</small></div> -->
		                	      	      </div>
		                	      	    </div>

		                	      	    <?php } else {
		                	      	    ?>

		                	      	    <div class="taxas  d-none">
		                	      	      <div class="linha">
		                	      	        <div class="taxa"><span>3,63%</span><small>TAXA MÁXIMA (A.M.)</small></div>
		                	      	        <div class="taxa"><span>54,24%</span><small>TAXA MÁXIMA (A.A.)</small></div>
		                	      	        <!-- <div class="taxa"><span>1,75%</span><small>CET (A.M.)</small></div>
		                	      	        <div class="taxa"><span>23,54%</span><small>CET (A.A.)</small></div> -->
		                	      	      </div>
		                	      	    </div>

		                	      	    <?php
		                	      	    } ?>
		                	      	    <!-- 
		                	      	    <div class="seguro-prestamista">
		                	      	      <p>+ SEGURO PRESTAMISTA: <strong></strong></p>
		                	      	    </div>
		                	      	     -->
		                	      	  </div>
		                	      	</div>
		                	      	<!-- fim proposta -->

	      	                	    <div class="center">
	      								<button class="btn btn-link" data-toggle="collapse" data-target="#collapse-<?php echo $value['operacaoId']; ?>" aria-expanded="true" aria-controls="collapse-<?php echo $value['operacaoId']; ?>">
	      									VOLTAR
	      								</button>
	      							</div>
		                	      </div>

		                	    </div>
		                	  </div>
		                	</div>
		                	<!-- fim card -->

		                <?php
		                	}

		                ?>

		                </div>

					</div><!-- .entry-content -->

				</article><!-- #post-## -->
			<?php
			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</section><!-- #primary -->


<pre style="display: none;">
REQUEST:
<?php print_r($_REQUEST); ?>


returnValue:
<?php print_r($returnLoan); ?>
</pre>
<?php
get_footer();