<?php
require_once( __DIR__ . '/classes/Connection.class.php');
require_once( __DIR__ . '/classes/Loan.class.php');
require_once( __DIR__ . '/classes/File.class.php');
require_once( __DIR__ . '/classes/User.class.php');

function theme_enqueue_styles() {

    $parent_style = 'parent-style';

    wp_register_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Maven+Pro:400,500,700,900&display=swap', array(), '', 'all' );
    wp_enqueue_style( 'google-fonts' );

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );

    wp_enqueue_style( 'toastr-style',
        get_stylesheet_directory_uri() . '/css/toastr.css', array(), false, 'all'
    );
    
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css?v=2'.time(), array(), false, 'all'
    );
    
    wp_enqueue_style( 'child-style-blog',
        get_stylesheet_directory_uri() . '/style-blog.css?v=3'.time(), array(), false, 'all'
    );




    wp_enqueue_script( 'js-cookie',
        get_stylesheet_directory_uri() . '/js/js.cookie.js', array(), '2.2.1', true
    );

    wp_enqueue_script( 'popper-js',
        get_stylesheet_directory_uri() . '/js/popper.min.js', array(), '1.14.7', true
    );

    wp_enqueue_script( 'jquery-redirect',
        get_stylesheet_directory_uri() . '/js/jquery.redirect.js', array(), '1.1.3', true
    );
    wp_enqueue_script( 'jquery-mask',
        get_stylesheet_directory_uri() . '/js/jquery.mask.min.js', array(), '1.14.16', true
    );
    wp_enqueue_script( 'sweet-alert',
        get_stylesheet_directory_uri() . '/js/sweetalert.min.js', array(), '2.1.2', true
    );
    wp_enqueue_script( 'toastr-js',
        get_stylesheet_directory_uri() . '/js/toastr.min.js', array(), '2.1.3', true
    );
    wp_enqueue_script( 'form-js',
        get_stylesheet_directory_uri() . '/js/jquery.form.min.js', array(), '4.4.2', true
    );
    wp_enqueue_script( 'image-compressor-js',
        get_stylesheet_directory_uri() . '/js/image-compressor.min.js', array(), '4.4.2', true
    );
    wp_enqueue_script( 'load-image-js',
        get_stylesheet_directory_uri() . '/js/load-image.all.min.js', array(), '4.4.2', true
    );
    wp_enqueue_script( 'jquery-sticky-js',
        get_stylesheet_directory_uri() . '/js/jquery.sticky.js', array(), '1.0.4', true
    );
    wp_enqueue_script( 'custom-scripts',
        get_stylesheet_directory_uri() . '/js/custom.js?v=2', array(), '1.0.3', true
    );


}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

function is_multi_array( $arr ) { 
    rsort( $arr ); 
    return isset( $arr[0] ) && is_array( $arr[0] ); 
}

add_action('wp_ajax_load_posts_by_ajax', 'load_posts_by_ajax_callback');
add_action('wp_ajax_nopriv_load_posts_by_ajax', 'load_posts_by_ajax_callback');
function load_posts_by_ajax_callback() {
    check_ajax_referer('load_more_posts', 'security');
    $paged = $_POST['page'];
    $offset = $_POST['offset'];
    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'posts_per_page' => '8',
        'offset' => $offset,
        'paged' => $paged,
    );
    $my_posts = new WP_Query( $args );
    if ( $my_posts->have_posts() ) :
        ?>
<?php while ( $my_posts->have_posts() ) : $my_posts->the_post(); $post_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID));
            $textoWhats = rawurlencode(get_the_title() . '. Acesse em: ' . get_the_permalink());
            $facebookURL = rawurlencode(get_the_permalink());
            ?>
<div class="col-md-6 p-3">
    <div class="post-cont">
        <img src="<?php echo $post_image; ?>">
        <div class="post-box">
            <h3>
                <?php echo the_title(); ?>
            </h3>
            <p>
                <?php echo the_excerpt(); ?>
            </p>
            <div class="post-actions">
                <div class="share-post">
                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $facebookURL; ?>"
                        class="facebook-share" target="_blank">Facebook</a>
                    <a href="https://api.whatsapp.com/send?phone=&text=<?php echo $textoWhats; ?>&source=&data="
                        class="whatsapp-share" target="_blank">Whatsapp</a>
                </div>
                <a class="post-link" href="<?php echo the_permalink(); ?>">Continuar Lendo</a>
            </div>
        </div>
    </div>
</div>
<?php endwhile; ?>
<?php
    endif;

    wp_die();
}

add_action('wp_ajax_load_noticias_by_ajax', 'load_noticias_by_ajax_callback');
add_action('wp_ajax_nopriv_load_noticias_by_ajax', 'load_noticias_by_ajax_callback');
function load_noticias_by_ajax_callback() {
    check_ajax_referer('load_more_posts', 'security');
    $paged = $_POST['page'];
    $offset = $_POST['offset'];
    $args = array(
        'post_type' => 'noticias',
        'post_status' => 'publish',
        'posts_per_page' => '8',
        'offset' => $offset,
        'paged' => $paged,
    );
    $my_posts = new WP_Query( $args );
    if ( $my_posts->have_posts() ) :
        ?>
<?php while ( $my_posts->have_posts() ) : $my_posts->the_post(); $post_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID));
            $textoWhats = rawurlencode(get_the_title() . '. Acesse em: ' . get_the_permalink());
            $facebookURL = rawurlencode(get_the_permalink());

            ?>
<div class="col-md-6 p-3">
    <div class="post-cont">
        <img src="<?php echo $post_image; ?>">
        <div class="post-box">
            <h3>
                <?php echo the_title(); ?>
            </h3>
            <p>
                <?php echo the_excerpt(); ?>
            </p>
            <div class="post-actions">
                <div class="share-post">
                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $facebookURL; ?>"
                        class="facebook-share" target="_blank">Facebook</a>
                    <a href="https://api.whatsapp.com/send?phone=&text=<?php echo $textoWhats; ?>&source=&data="
                        class="whatsapp-share" target="_blank">Whatsapp</a>
                </div>
                <a class="post-link" href="<?php echo the_permalink(); ?>">Continuar Lendo</a>
            </div>
        </div>
    </div>
</div>
<?php endwhile; ?>
<?php
    endif;

    wp_die();
}

function shapeSpace_display_search_form() {
    return get_search_form(false);
}
add_shortcode('display_search_form', 'shapeSpace_display_search_form');

function prestho_display_cellphone_element(){
    return get_template_part( 'template-parts/content', 'cellphone' );
}
add_shortcode('display_cellphone_element', 'prestho_display_cellphone_element');


function prestho_display_news_element(){
    return get_template_part( 'template-parts/content', 'form-news' );
}
add_shortcode('display_news_element', 'prestho_display_news_element');

function prestho_display_app_links(){
?>

<div class="app-widget">
    <h3>Clique e baixe grátis o APP e tenha a ajuda que precisa a qualquer hora</h3>
    <?php echo get_template_part( "template-parts/content", "apps-links" ); ?>
</div>

<?php
}
add_shortcode('display_app_links', 'prestho_display_app_links');

function prestho_meta() {
    $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
    // if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
    //     $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
    // }

    $time_string = sprintf( $time_string,
        esc_attr( get_the_date( 'c' ) ),
        esc_html( get_the_date() ),
        esc_attr( get_the_modified_date( 'c' ) ),
        esc_html( get_the_modified_date() )
    );

    $posted_on = sprintf(
        esc_html_x( 'Publicado em %s', 'post date', 'fabulous-fluid-pro' ),
        '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
    );

    $byline = sprintf(
        esc_html_x( 'por %s', 'post author', 'fabulous-fluid-pro' ),
        '<span class="author vcard">' . esc_html( get_the_author() ) . '</span>'
    );

    echo '<span class="posted-on">' . $posted_on . '</span> | <span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

}


function prefix_add_content ($content){
    if (is_singular('post') || is_singular('noticias')) {
        
        $before = "";
        $after = '
            <footer class="entry-footer">
                <div class="cta-simulacao">
                    <div class="row">
                        <div class="col-md-6">
                            <p>Sabia que você pode saber na hora quando tem de crédito consignado?</p>
                        </div>
                        <div class="col-md-6">
                            <a href="#" class="faca-simulacao-post">Faça uma simulação grátis</a>
                        </div>
                    </div>
                </div>

                <div id="new-nav"></div>
            </footer><!-- .entry-footer -->
        ';
        $content = $before . $content . $after;
    }
 
    return $content;
}
add_filter ('the_content', 'prefix_add_content');


if (!is_admin()) {
    function wpb_search_filter($query) {
if ($query->is_search) {
    $query->set('post_type', 'post');
}
return $query;
}
add_filter('pre_get_posts','wpb_search_filter');
}

/**
 * Register support for Gutenberg wide images in your theme
 */
function image_alignment() {
  add_theme_support( 'align-wide' );
}
add_action( 'after_setup_theme', 'image_alignment' );

