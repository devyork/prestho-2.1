<?php
/**
 * Template name: - Sobre a Prestho
 */

get_header(); ?>

	<section id="primary" class="content-area col-sm-12 col-lg-12">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					
				    <header class="entry-header">
						<h1><?php echo get_the_title(); ?></h1>
					</header><!-- .entry-header -->
				   
					<div class="entry-content" id="conteudo-sobre">
						
						<?php the_content(); ?>

						<?php

							// check if the repeater field has rows of data
							if( have_rows('vantagens') ):

								echo '<ul class="lista-vantagens">';

							 	// loop through the rows of data
							    while ( have_rows('vantagens') ) : the_row();
							    	echo '<li>'.get_sub_field("texto").'</li>';
							    endwhile;

							    echo '</ul>';

							endif;

						        
						?>

						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/svg/selo.svg" alt="" class="selo-sobre-mobile"></a>

					</div><!-- .entry-content -->

				</article><!-- #post-## -->
			<?php
			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php get_template_part( 'template-parts/content', 'script-fixed-menu' ); ?>
<?php
get_footer();
