<?php
/**
 * Template name: - Contratar - Passo 3
 */
get_header();


?>

<section id="primary" class="content-area col-sm-12 col-lg-12 contratar">
	<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
				<div class="entry-content">
					<div class="prestho-form">
						<h1>
							<strong>Empréstimo Consignado</strong>
							<small>*Sujeito à análise de crédito</small>
						</h1>

						<p>
							Quase lá! Agora precisamos do seu endereço:
						</p>

						<form id="form-endereco" class="prestho-form" method="post" action="<?php echo home_url( $wp->request ); ?>?confirmacao=1">
							<?php
								if(isset($_SESSION['request']['step1']['cadastro']['email'])):
									echo '<input type="hidden" name="emailRD" value="'.$_SESSION['request']['step1']['cadastro']['email'].'">';
								else:
									$numFone = $_SESSION['request']['step1']['cadastro']['telefones'][0]['ddd'];
									$numFone .= $_SESSION['request']['step1']['cadastro']['telefones'][0]['numero'];
									echo '<input type="hidden" name="emailRD" value="'.$numFone.'@prestho.com.br">';	
								endif;
							?>
							<fieldset>
								<?php 

									if(isset($_SESSION['request']['step1']['cadastro']['endereco']['cep'])):
										$focus1 = " focus";
										$cep_cliente = $_SESSION['request']['step1']['cadastro']['endereco']['cep'];
									else:
										$focus1 = "";
										$cep_cliente = '';
									endif;

								?>

								<label for="cep" class="field smart f60<?php echo $focus1; ?>">
									<span>CEP</span>
									<input autocomplete="off" type="tel" name="endereco[cep]" id="cep" value="<?php echo $cep_cliente; ?>" required>
								</label>

								<a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" class="nao-sei-cep" target="_blank">NÃO SEI MEU CEP</a>
							</fieldset>

							<fieldset>
								<label for="tipoLogradouroId" class="field">
									<?php 

										if(isset($_SESSION['request']['step1']['cadastro']['endereco']['tipoLogradouroId'])):

											switch ($_SESSION['request']['step1']['cadastro']['endereco']['tipoLogradouroId']) {
												case '1':
													$tipo1 = ' selected="selected"';
													$tipo2 = '';
													$tipo3 = '';
													$tipo4 = '';
													$tipo5 = '';
													$tipo6 = '';
													$tipo7 = '';
													$tipo8 = '';
													$tipo9 = '';
													$tipo10 = '';
													break;

												case '2':
													$tipo1 = '';
													$tipo2 = ' selected="selected"';
													$tipo3 = '';
													$tipo4 = '';
													$tipo5 = '';
													$tipo6 = '';
													$tipo7 = '';
													$tipo8 = '';
													$tipo9 = '';
													$tipo10 = '';
													break;

												case '3':
													$tipo1 = '';
													$tipo2 = '';
													$tipo3 = ' selected="selected"';
													$tipo4 = '';
													$tipo5 = '';
													$tipo6 = '';
													$tipo7 = '';
													$tipo8 = '';
													$tipo9 = '';
													$tipo10 = '';
													break;

												case '4':
													$tipo1 = '';
													$tipo2 = '';
													$tipo3 = '';
													$tipo4 = ' selected="selected"';
													$tipo5 = '';
													$tipo6 = '';
													$tipo7 = '';
													$tipo8 = '';
													$tipo9 = '';
													$tipo10 = '';
													break;

												case '5':
													$tipo1 = '';
													$tipo2 = '';
													$tipo3 = '';
													$tipo4 = '';
													$tipo5 = ' selected="selected"';
													$tipo6 = '';
													$tipo7 = '';
													$tipo8 = '';
													$tipo9 = '';
													$tipo10 = '';
													break;

												case '6':
													$tipo1 = '';
													$tipo2 = '';
													$tipo3 = '';
													$tipo4 = '';
													$tipo5 = '';
													$tipo6 = ' selected="selected"';
													$tipo7 = '';
													$tipo8 = '';
													$tipo9 = '';
													$tipo10 = '';
													break;

												case '7':
													$tipo1 = '';
													$tipo2 = '';
													$tipo3 = '';
													$tipo4 = '';
													$tipo5 = '';
													$tipo6 = '';
													$tipo7 = ' selected="selected"';
													$tipo8 = '';
													$tipo9 = '';
													$tipo10 = '';
													break;

												case '8':
													$tipo1 = '';
													$tipo2 = '';
													$tipo3 = '';
													$tipo4 = '';
													$tipo5 = '';
													$tipo6 = '';
													$tipo7 = '';
													$tipo8 = ' selected="selected"';
													$tipo9 = '';
													$tipo10 = '';
													break;

												case '9':
													$tipo1 = '';
													$tipo2 = '';
													$tipo3 = '';
													$tipo4 = '';
													$tipo5 = '';
													$tipo6 = '';
													$tipo7 = '';
													$tipo8 = '';
													$tipo9 = ' selected="selected"';
													$tipo10 = '';
													break;

												case '10':
													$tipo1 = '';
													$tipo2 = '';
													$tipo3 = '';
													$tipo4 = '';
													$tipo5 = '';
													$tipo6 = '';
													$tipo7 = '';
													$tipo8 = '';
													$tipo9 = '';
													$tipo10 = ' selected="selected"';
													break;
												
												default:
													$tipo1 = '';
													$tipo2 = '';
													$tipo3 = '';
													$tipo4 = '';
													$tipo5 = '';
													$tipo6 = '';
													$tipo7 = '';
													$tipo8 = '';
													$tipo9 = '';
													$tipo10 = '';
													break;
											}
											
										else:
											$tipo1 = '';
											$tipo2 = '';
											$tipo3 = '';
											$tipo4 = '';
											$tipo5 = '';
											$tipo6 = '';
											$tipo7 = '';
											$tipo8 = '';
											$tipo9 = '';
											$tipo10 = '';
										endif;

									?>
									<span class="active">Logradouro <svg><use xlink:href="#seta-elemento"></use></svg></span>
									<select autocomplete="off" name="endereco[logradouroTipo]" id="tipoLogradouroId" required="required">
										<option value="0">Selecione... (Rua, avenida, praça, etc.)</option>
										<option value="1"<?php echo $tipo1; ?>>Rua</option>
										<option value="2"<?php echo $tipo2; ?>>Avenida</option>
										<option value="3"<?php echo $tipo3; ?>>Rodovia</option>
										<option value="4"<?php echo $tipo4; ?>>Travessa</option>
										<option value="5"<?php echo $tipo5; ?>>Praça</option>
										<option value="6"<?php echo $tipo6; ?>>Alameda</option>
										<option value="7"<?php echo $tipo7; ?>>Quadra</option>
										<option value="8"<?php echo $tipo8; ?>>Estrada</option>
										<option value="9"<?php echo $tipo9; ?>>Beco</option>
										<option value="10"<?php echo $tipo10; ?>>Conjunto</option>
									</select>
								</label>
							</fieldset>

							<fieldset>
								<?php 

									if(isset($_SESSION['request']['step1']['cadastro']['endereco']['logradouro'])):
										$focus2 = " focus";
										$logradouro_cliente = $_SESSION['request']['step1']['cadastro']['endereco']['logradouro'];
									else:
										$focus2 = "";
										$logradouro_cliente = '';
									endif;
								?>
								<label for="logradouro" class="field<?php echo $focus2; ?>">
									<span>Endereço</span>
									<input autocomplete="off" type="text" name="endereco[logradouro]" id="logradouro" value="<?php echo $logradouro_cliente; ?>" maxlength="100" required>
								</label>
								
								<?php 

									if(isset($_SESSION['request']['step1']['cadastro']['endereco']['numero'])):
										$focus3 = " focus";
										$numero_cliente = $_SESSION['request']['step1']['cadastro']['endereco']['numero'];
									else:
										$focus3 = "";
										$numero_cliente = '';
									endif;
								?>
								<label for="numero" class="field f50l<?php echo $focus3; ?>">
									<span>Número</span>
									<input autocomplete="off" type="tel" name="endereco[numero]" id="numero" value="<?php echo $numero_cliente; ?>" maxlength="10" required>
								</label>
								
								<?php 

									if(isset($_SESSION['request']['step1']['cadastro']['endereco']['complemento'])):
										$focus4 = " focus";
										$complemento_cliente = $_SESSION['request']['step1']['cadastro']['endereco']['complemento'];
									else:
										$focus4 = "";
										$complemento_cliente = '';
									endif;
								?>
								<label for="complemento" class="field f50r<?php echo $focus4; ?>">
									<span>Complemento</span>
									<input autocomplete="off" type="text" name="endereco[complemento]" value="<?php echo $complemento_cliente; ?>" id="complemento" maxlength="50">
								</label>
								
								<?php 

									if(isset($_SESSION['request']['step1']['cadastro']['endereco']['bairro'])):
										$focus5 = " focus";
										$bairro_cliente = $_SESSION['request']['step1']['cadastro']['endereco']['bairro'];
									else:
										$focus5 = "";
										$bairro_cliente = '';
									endif;
								?>
								<label for="bairro" class="field<?php echo $focus5; ?>">
									<span>Bairro</span>
									<input autocomplete="off" type="text" name="endereco[bairro]" id="bairro" value="<?php echo $bairro_cliente; ?>" maxlength="100" required>
								</label>

								<?php 

										if(isset($_SESSION['request']['step1']['cadastro']['endereco']['uf'])):
											switch ($_SESSION['request']['step1']['cadastro']['endereco']['uf']) {
												case 'AC':
													$valAC = ' selected="selected"';
													$valAL = '';
													$valAP = '';
													$valAM = '';
													$valBA = '';
													$valCE = '';
													$valDF = '';
													$valES = '';
													$valGO = '';
													$valMA = '';
													$valMT = '';
													$valMS = '';
													$valMG = '';
													$valPA = '';
													$valPB = '';
													$valPR = '';
													$valPE = '';
													$valPI = '';
													$valRJ = '';
													$valRN = '';
													$valRS = '';
													$valRO = '';
													$valRR = '';
													$valSC = '';
													$valSP = '';
													$valSE = '';
													$valTO = '';
													break;

													case 'AL':
													$valAC = '';
													$valAL = ' selected="selected"';
													$valAP = '';
													$valAM = '';
													$valBA = '';
													$valCE = '';
													$valDF = '';
													$valES = '';
													$valGO = '';
													$valMA = '';
													$valMT = '';
													$valMS = '';
													$valMG = '';
													$valPA = '';
													$valPB = '';
													$valPR = '';
													$valPE = '';
													$valPI = '';
													$valRJ = '';
													$valRN = '';
													$valRS = '';
													$valRO = '';
													$valRR = '';
													$valSC = '';
													$valSP = '';
													$valSE = '';
													$valTO = '';
													break;

													case 'AP':
													$valAC = '';
													$valAL = '';
													$valAP = ' selected="selected"';
													$valAM = '';
													$valBA = '';
													$valCE = '';
													$valDF = '';
													$valES = '';
													$valGO = '';
													$valMA = '';
													$valMT = '';
													$valMS = '';
													$valMG = '';
													$valPA = '';
													$valPB = '';
													$valPR = '';
													$valPE = '';
													$valPI = '';
													$valRJ = '';
													$valRN = '';
													$valRS = '';
													$valRO = '';
													$valRR = '';
													$valSC = '';
													$valSP = '';
													$valSE = '';
													$valTO = '';
													break;

													case 'AM':
													$valAC = '';
													$valAL = '';
													$valAP = '';
													$valAM = ' selected="selected"';
													$valBA = '';
													$valCE = '';
													$valDF = '';
													$valES = '';
													$valGO = '';
													$valMA = '';
													$valMT = '';
													$valMS = '';
													$valMG = '';
													$valPA = '';
													$valPB = '';
													$valPR = '';
													$valPE = '';
													$valPI = '';
													$valRJ = '';
													$valRN = '';
													$valRS = '';
													$valRO = '';
													$valRR = '';
													$valSC = '';
													$valSP = '';
													$valSE = '';
													$valTO = '';
													break;

													case 'BA':
													$valAC = '';
													$valAL = '';
													$valAP = '';
													$valAM = '';
													$valBA = ' selected="selected"';
													$valCE = '';
													$valDF = '';
													$valES = '';
													$valGO = '';
													$valMA = '';
													$valMT = '';
													$valMS = '';
													$valMG = '';
													$valPA = '';
													$valPB = '';
													$valPR = '';
													$valPE = '';
													$valPI = '';
													$valRJ = '';
													$valRN = '';
													$valRS = '';
													$valRO = '';
													$valRR = '';
													$valSC = '';
													$valSP = '';
													$valSE = '';
													$valTO = '';
													break;

													case 'CE':
													$valAC = '';
													$valAL = '';
													$valAP = '';
													$valAM = '';
													$valBA = '';
													$valCE = ' selected="selected"';
													$valDF = '';
													$valES = '';
													$valGO = '';
													$valMA = '';
													$valMT = '';
													$valMS = '';
													$valMG = '';
													$valPA = '';
													$valPB = '';
													$valPR = '';
													$valPE = '';
													$valPI = '';
													$valRJ = '';
													$valRN = '';
													$valRS = '';
													$valRO = '';
													$valRR = '';
													$valSC = '';
													$valSP = '';
													$valSE = '';
													$valTO = '';
													break;

													case 'DF':
													$valAC = '';
													$valAL = '';
													$valAP = '';
													$valAM = '';
													$valBA = '';
													$valCE = '';
													$valDF = ' selected="selected"';
													$valES = '';
													$valGO = '';
													$valMA = '';
													$valMT = '';
													$valMS = '';
													$valMG = '';
													$valPA = '';
													$valPB = '';
													$valPR = '';
													$valPE = '';
													$valPI = '';
													$valRJ = '';
													$valRN = '';
													$valRS = '';
													$valRO = '';
													$valRR = '';
													$valSC = '';
													$valSP = '';
													$valSE = '';
													$valTO = '';
													break;

													case 'ES':
													$valAC = '';
													$valAL = '';
													$valAP = '';
													$valAM = '';
													$valBA = '';
													$valCE = '';
													$valDF = '';
													$valES = ' selected="selected"';
													$valGO = '';
													$valMA = '';
													$valMT = '';
													$valMS = '';
													$valMG = '';
													$valPA = '';
													$valPB = '';
													$valPR = '';
													$valPE = '';
													$valPI = '';
													$valRJ = '';
													$valRN = '';
													$valRS = '';
													$valRO = '';
													$valRR = '';
													$valSC = '';
													$valSP = '';
													$valSE = '';
													$valTO = '';
													break;

													case 'GO':
													$valAC = '';
													$valAL = '';
													$valAP = '';
													$valAM = '';
													$valBA = '';
													$valCE = '';
													$valDF = '';
													$valES = '';
													$valGO = ' selected="selected"';
													$valMA = '';
													$valMT = '';
													$valMS = '';
													$valMG = '';
													$valPA = '';
													$valPB = '';
													$valPR = '';
													$valPE = '';
													$valPI = '';
													$valRJ = '';
													$valRN = '';
													$valRS = '';
													$valRO = '';
													$valRR = '';
													$valSC = '';
													$valSP = '';
													$valSE = '';
													$valTO = '';
													break;

													case 'MA':
													$valAC = '';
													$valAL = '';
													$valAP = '';
													$valAM = '';
													$valBA = '';
													$valCE = '';
													$valDF = '';
													$valES = '';
													$valGO = '';
													$valMA = ' selected="selected"';
													$valMT = '';
													$valMS = '';
													$valMG = '';
													$valPA = '';
													$valPB = '';
													$valPR = '';
													$valPE = '';
													$valPI = '';
													$valRJ = '';
													$valRN = '';
													$valRS = '';
													$valRO = '';
													$valRR = '';
													$valSC = '';
													$valSP = '';
													$valSE = '';
													$valTO = '';
													break;

													case 'MT':
													$valAC = '';
													$valAL = '';
													$valAP = '';
													$valAM = '';
													$valBA = '';
													$valCE = '';
													$valDF = '';
													$valES = '';
													$valGO = '';
													$valMA = '';
													$valMT = ' selected="selected"';
													$valMS = '';
													$valMG = '';
													$valPA = '';
													$valPB = '';
													$valPR = '';
													$valPE = '';
													$valPI = '';
													$valRJ = '';
													$valRN = '';
													$valRS = '';
													$valRO = '';
													$valRR = '';
													$valSC = '';
													$valSP = '';
													$valSE = '';
													$valTO = '';
													break;


													case 'MS':
													$valAC = '';
													$valAL = '';
													$valAP = '';
													$valAM = '';
													$valBA = '';
													$valCE = '';
													$valDF = '';
													$valES = '';
													$valGO = '';
													$valMA = '';
													$valMT = '';
													$valMS = ' selected="selected"';
													$valMG = '';
													$valPA = '';
													$valPB = '';
													$valPR = '';
													$valPE = '';
													$valPI = '';
													$valRJ = '';
													$valRN = '';
													$valRS = '';
													$valRO = '';
													$valRR = '';
													$valSC = '';
													$valSP = '';
													$valSE = '';
													$valTO = '';
													break;

													case 'MG':
													$valAC = '';
													$valAL = '';
													$valAP = '';
													$valAM = '';
													$valBA = '';
													$valCE = '';
													$valDF = '';
													$valES = '';
													$valGO = '';
													$valMA = '';
													$valMT = '';
													$valMS = '';
													$valMG = ' selected="selected"';
													$valPA = '';
													$valPB = '';
													$valPR = '';
													$valPE = '';
													$valPI = '';
													$valRJ = '';
													$valRN = '';
													$valRS = '';
													$valRO = '';
													$valRR = '';
													$valSC = '';
													$valSP = '';
													$valSE = '';
													$valTO = '';
													break;

													case 'PA':
													$valAC = '';
													$valAL = '';
													$valAP = '';
													$valAM = '';
													$valBA = '';
													$valCE = '';
													$valDF = '';
													$valES = '';
													$valGO = '';
													$valMA = '';
													$valMT = '';
													$valMS = '';
													$valMG = '';
													$valPA = ' selected="selected"';
													$valPB = '';
													$valPR = '';
													$valPE = '';
													$valPI = '';
													$valRJ = '';
													$valRN = '';
													$valRS = '';
													$valRO = '';
													$valRR = '';
													$valSC = '';
													$valSP = '';
													$valSE = '';
													$valTO = '';
													break;

													case 'PB':
													$valAC = '';
													$valAL = '';
													$valAP = '';
													$valAM = '';
													$valBA = '';
													$valCE = '';
													$valDF = '';
													$valES = '';
													$valGO = '';
													$valMA = '';
													$valMT = '';
													$valMS = '';
													$valMG = '';
													$valPA = '';
													$valPB = ' selected="selected"';
													$valPR = '';
													$valPE = '';
													$valPI = '';
													$valRJ = '';
													$valRN = '';
													$valRS = '';
													$valRO = '';
													$valRR = '';
													$valSC = '';
													$valSP = '';
													$valSE = '';
													$valTO = '';
													break;

													case 'PR':
													$valAC = '';
													$valAL = '';
													$valAP = '';
													$valAM = '';
													$valBA = '';
													$valCE = '';
													$valDF = '';
													$valES = '';
													$valGO = '';
													$valMA = '';
													$valMT = '';
													$valMS = '';
													$valMG = '';
													$valPA = '';
													$valPB = '';
													$valPR = ' selected="selected"';
													$valPE = '';
													$valPI = '';
													$valRJ = '';
													$valRN = '';
													$valRS = '';
													$valRO = '';
													$valRR = '';
													$valSC = '';
													$valSP = '';
													$valSE = '';
													$valTO = '';
													break;

													case 'PE':
													$valAC = '';
													$valAL = '';
													$valAP = '';
													$valAM = '';
													$valBA = '';
													$valCE = '';
													$valDF = '';
													$valES = '';
													$valGO = '';
													$valMA = '';
													$valMT = '';
													$valMS = '';
													$valMG = '';
													$valPA = '';
													$valPB = '';
													$valPR = '';
													$valPE = ' selected="selected"';
													$valPI = '';
													$valRJ = '';
													$valRN = '';
													$valRS = '';
													$valRO = '';
													$valRR = '';
													$valSC = '';
													$valSP = '';
													$valSE = '';
													$valTO = '';
													break;


													case 'PI':
													$valAC = '';
													$valAL = '';
													$valAP = '';
													$valAM = '';
													$valBA = '';
													$valCE = '';
													$valDF = '';
													$valES = '';
													$valGO = '';
													$valMA = '';
													$valMT = '';
													$valMS = '';
													$valMG = '';
													$valPA = '';
													$valPB = '';
													$valPR = '';
													$valPE = '';
													$valPI = ' selected="selected"';
													$valRJ = '';
													$valRN = '';
													$valRS = '';
													$valRO = '';
													$valRR = '';
													$valSC = '';
													$valSP = '';
													$valSE = '';
													$valTO = '';
													break;

													case 'RJ':
													$valAC = '';
													$valAL = '';
													$valAP = '';
													$valAM = '';
													$valBA = '';
													$valCE = '';
													$valDF = '';
													$valES = '';
													$valGO = '';
													$valMA = '';
													$valMT = '';
													$valMS = '';
													$valMG = '';
													$valPA = '';
													$valPB = '';
													$valPR = '';
													$valPE = '';
													$valPI = '';
													$valRJ = ' selected="selected"';
													$valRN = '';
													$valRS = '';
													$valRO = '';
													$valRR = '';
													$valSC = '';
													$valSP = '';
													$valSE = '';
													$valTO = '';
													break;

													case 'RN':
													$valAC = '';
													$valAL = '';
													$valAP = '';
													$valAM = '';
													$valBA = '';
													$valCE = '';
													$valDF = '';
													$valES = '';
													$valGO = '';
													$valMA = '';
													$valMT = '';
													$valMS = '';
													$valMG = '';
													$valPA = '';
													$valPB = '';
													$valPR = '';
													$valPE = '';
													$valPI = '';
													$valRJ = '';
													$valRN = ' selected="selected"';
													$valRS = '';
													$valRO = '';
													$valRR = '';
													$valSC = '';
													$valSP = '';
													$valSE = '';
													$valTO = '';
													break;

													case 'RS':
													$valAC = '';
													$valAL = '';
													$valAP = '';
													$valAM = '';
													$valBA = '';
													$valCE = '';
													$valDF = '';
													$valES = '';
													$valGO = '';
													$valMA = '';
													$valMT = '';
													$valMS = '';
													$valMG = '';
													$valPA = '';
													$valPB = '';
													$valPR = '';
													$valPE = '';
													$valPI = '';
													$valRJ = '';
													$valRN = '';
													$valRS = ' selected="selected"';
													$valRO = '';
													$valRR = '';
													$valSC = '';
													$valSP = '';
													$valSE = '';
													$valTO = '';
													break;

													case 'RO':
													$valAC = '';
													$valAL = '';
													$valAP = '';
													$valAM = '';
													$valBA = '';
													$valCE = '';
													$valDF = '';
													$valES = '';
													$valGO = '';
													$valMA = '';
													$valMT = '';
													$valMS = '';
													$valMG = '';
													$valPA = '';
													$valPB = '';
													$valPR = '';
													$valPE = '';
													$valPI = '';
													$valRJ = '';
													$valRN = '';
													$valRS = '';
													$valRO = ' selected="selected"';
													$valRR = '';
													$valSC = '';
													$valSP = '';
													$valSE = '';
													$valTO = '';
													break;

													case 'RR':
													$valAC = '';
													$valAL = '';
													$valAP = '';
													$valAM = '';
													$valBA = '';
													$valCE = '';
													$valDF = '';
													$valES = '';
													$valGO = '';
													$valMA = '';
													$valMT = '';
													$valMS = '';
													$valMG = '';
													$valPA = '';
													$valPB = '';
													$valPR = '';
													$valPE = '';
													$valPI = '';
													$valRJ = '';
													$valRN = '';
													$valRS = '';
													$valRO = '';
													$valRR = ' selected="selected"';
													$valSC = '';
													$valSP = '';
													$valSE = '';
													$valTO = '';
													break;

													case 'SC':
													$valAC = '';
													$valAL = '';
													$valAP = '';
													$valAM = '';
													$valBA = '';
													$valCE = '';
													$valDF = '';
													$valES = '';
													$valGO = '';
													$valMA = '';
													$valMT = '';
													$valMS = '';
													$valMG = '';
													$valPA = '';
													$valPB = '';
													$valPR = '';
													$valPE = '';
													$valPI = '';
													$valRJ = '';
													$valRN = '';
													$valRS = '';
													$valRO = '';
													$valRR = '';
													$valSC = ' selected="selected"';
													$valSP = '';
													$valSE = '';
													$valTO = '';
													break;

													case 'SP':
													$valAC = '';
													$valAL = '';
													$valAP = '';
													$valAM = '';
													$valBA = '';
													$valCE = '';
													$valDF = '';
													$valES = '';
													$valGO = '';
													$valMA = '';
													$valMT = '';
													$valMS = '';
													$valMG = '';
													$valPA = '';
													$valPB = '';
													$valPR = '';
													$valPE = '';
													$valPI = '';
													$valRJ = '';
													$valRN = '';
													$valRS = '';
													$valRO = '';
													$valRR = '';
													$valSC = '';
													$valSP = ' selected="selected"';
													$valSE = '';
													$valTO = '';
													break;

													case 'SE':
													$valAC = '';
													$valAL = '';
													$valAP = '';
													$valAM = '';
													$valBA = '';
													$valCE = '';
													$valDF = '';
													$valES = '';
													$valGO = '';
													$valMA = '';
													$valMT = '';
													$valMS = '';
													$valMG = '';
													$valPA = '';
													$valPB = '';
													$valPR = '';
													$valPE = '';
													$valPI = '';
													$valRJ = '';
													$valRN = '';
													$valRS = '';
													$valRO = '';
													$valRR = '';
													$valSC = '';
													$valSP = '';
													$valSE = ' selected="selected"';
													$valTO = '';
													break;

													case 'TO':
													$valAC = '';
													$valAL = '';
													$valAP = '';
													$valAM = '';
													$valBA = '';
													$valCE = '';
													$valDF = '';
													$valES = '';
													$valGO = '';
													$valMA = '';
													$valMT = '';
													$valMS = '';
													$valMG = '';
													$valPA = '';
													$valPB = '';
													$valPR = '';
													$valPE = '';
													$valPI = '';
													$valRJ = '';
													$valRN = '';
													$valRS = '';
													$valRO = '';
													$valRR = '';
													$valSC = '';
													$valSP = '';
													$valSE = '';
													$valTO = ' selected="selected"';
													break;

												default:
													$valAC = '';
													$valAL = '';
													$valAP = '';
													$valAM = '';
													$valBA = '';
													$valCE = '';
													$valDF = '';
													$valES = '';
													$valGO = '';
													$valMA = '';
													$valMT = '';
													$valMS = '';
													$valMG = '';
													$valPA = '';
													$valPB = '';
													$valPR = '';
													$valPE = '';
													$valPI = '';
													$valRJ = '';
													$valRN = '';
													$valRS = '';
													$valRO = '';
													$valRR = '';
													$valSC = '';
													$valSP = '';
													$valSE = '';
													$valTO = '';
													break;
											}
											
										else:
											$valAC = '';
											$valAL = '';
											$valAP = '';
											$valAM = '';
											$valBA = '';
											$valCE = '';
											$valDF = '';
											$valES = '';
											$valGO = '';
											$valMA = '';
											$valMT = '';
											$valMS = '';
											$valMG = '';
											$valPA = '';
											$valPB = '';
											$valPR = '';
											$valPE = '';
											$valPI = '';
											$valRJ = '';
											$valRN = '';
											$valRS = '';
											$valRO = '';
											$valRR = '';
											$valSC = '';
											$valSP = '';
											$valSE = '';
											$valTO = '';
										endif;

									?>

								<label for="uf" class="field">
									<span class="active">Estado <svg><use xlink:href="#seta-elemento"></use></svg></span>
									<select autocomplete="off" name="endereco[uf]" id="uf" required>
										<option value="0">Selecione...</option>
										<option value="AC"<?php echo $valAC; ?>>Acre</option>
										<option value="AL"<?php echo $valAL; ?>>Alagoas</option>
										<option value="AP"<?php echo $valAP; ?>>Amapá</option>
										<option value="AM"<?php echo $valAM; ?>>Amazonas</option>
										<option value="BA"<?php echo $valBA; ?>>Bahia</option>
										<option value="CE"<?php echo $valCE; ?>>Ceará</option>
										<option value="DF"<?php echo $valDF; ?>>Distrito Federal</option>
										<option value="ES"<?php echo $valES; ?>>Espírito Santo</option>
										<option value="GO"<?php echo $valGO; ?>>Goiás</option>
										<option value="MA"<?php echo $valMA; ?>>Maranhão</option>
										<option value="MT"<?php echo $valMT; ?>>Mato Grosso</option>
										<option value="MS"<?php echo $valMS; ?>>Mato Grosso do Sul</option>
										<option value="MG"<?php echo $valMG; ?>>Minas Gerais</option>
										<option value="PA"<?php echo $valPA; ?>>Pará</option>
										<option value="PB"<?php echo $valPB; ?>>Paraíba</option>
										<option value="PR"<?php echo $valPR; ?>>Paraná</option>
										<option value="PE"<?php echo $valPE; ?>>Pernambuco</option>
										<option value="PI"<?php echo $valPI; ?>>Piauí</option>
										<option value="RJ"<?php echo $valRJ; ?>>Rio de Janeiro</option>
										<option value="RN"<?php echo $valRN; ?>>Rio Grande do Norte</option>
										<option value="RS"<?php echo $valRS; ?>>Rio Grande do Sul</option>
										<option value="RO"<?php echo $valRO; ?>>Rondônia</option>
										<option value="RR"<?php echo $valRR; ?>>Roraima</option>
										<option value="SC"<?php echo $valSC; ?>>Santa Catarina</option>
										<option value="SP"<?php echo $valSP; ?>>São Paulo</option>
										<option value="SE"<?php echo $valSE; ?>>Sergipe</option>
										<option value="TO"<?php echo $valTO; ?>>Tocantins</option>
									</select>
								</label>

								<?php 
									if(isset($_SESSION['request']['step1']['cadastro']['endereco']['cidade'])):
										$focus6 = " focus";
										$cidade_cliente = $_SESSION['request']['step1']['cadastro']['endereco']['cidade'];
									else:
										$focus6 = "";
										$cidade_cliente = '';
									endif;
								?>

								<label for="cidade" class="field<?php echo $focus6; ?>">
									<span>Cidade</span>
									<input autocomplete="off" type="text" name="endereco[cidade]" id="cidade" value="<?php echo $cidade_cliente; ?>" maxlength="50" required>
								</label>
							</fieldset>

							<div class="botoes">
								<?php
									if (isset($_REQUEST['endereco']) && !empty($_REQUEST['endereco'])) {

									} else if (isset($_SESSION['request']['step1']['cadastro']['endereco']) && isset($_REQUEST['altera']['celular'])) {
									  
									} else if (isset($_SESSION['request']['step1']['cadastro']['endereco']) && isset($_REQUEST['altera']['email'])) {
									  
									} else {
								?>
								<a href="<?php echo get_home_url(); ?>/contratar/dados-bancarios/" class="link-voltar" data-href="<?php echo get_home_url(); ?>/contratar/dados-bancarios/">VOLTAR</a>
								<?php
									}
								?>
								<button class="confirmar-endereco" id="submit-form-endereco">CONTINUAR</button>
							</div>
						</form>
					</div>
				</div><!-- .entry-content -->

			</article><!-- #post-## -->
		<?php
		endwhile; // End of the loop.
		?>

	</main><!-- #main -->
</section><!-- #primary -->

<?php
get_footer();