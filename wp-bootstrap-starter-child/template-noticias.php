<?php
/**
 * Template name: - Notícias
 */

get_header('noticias'); ?>
<section id="primary" class="content-area col-sm-12 col-lg-8">
	<main id="main" class="site-main" role="main">		
		<div class="row prestho-posts">

			<?php
			$args = array(
				'post_type'		=> 'noticias',
				'posts_per_page' => 8,
				'paged' => 1
			);

			$the_query = new WP_Query($args);

			if($the_query->have_posts()):
				while ( $the_query->have_posts() ) : $the_query->the_post(); 
					$post_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID));
					?>											
					<div class="col-md-6 p-3">	
						<div class="post-cont">
							<a href="<?php echo the_permalink(); ?>"><img src="<?php echo $post_image; ?>"></a>
							<div class="post-box">
								<h3>
									<a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a>							
								</h3>
								<p>
									<a href="<?php echo the_permalink(); ?>"><?php echo wp_strip_all_tags( get_the_excerpt(), true ); ?></a>
								</p>
								<div class="post-actions">
									<div class="share-post">
										<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $facebookURL; ?>" class="facebook-share" target="_blank">Facebook</a>
										<a href="https://api.whatsapp.com/send?phone=&text=<?php echo $textoWhats; ?>&source=&data=" class="whatsapp-share" target="_blank">Whatsapp</a>
									</div>
									<a class="post-link" href="<?php echo the_permalink(); ?>">Continuar Lendo</a>
								</div>
							</div>	
						</div>												
					</div>


					<?php 
				endwhile;
			endif;
			wp_reset_query();

			?>
		</div>		
		<div class="ajax-button">
			<a href="#" class="loadmore">Carregar mais Publicações</a>
		</div>
	</main><!-- #main -->
</section><!-- #primary -->
<?php get_template_part( 'template-parts/content', 'script-fixed-menu' ); ?>
<?php
get_sidebar();
get_footer('blog');