<?php
/**
 * Template name: - Dúvidas Frequentes
 */

get_header(); ?>

	<section id="primary" class="content-area col-sm-12 col-lg-12">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					
				    <header class="entry-header">
						<h1>Dúvidas Frequentes</h1>
					</header><!-- .entry-header -->
				   

					<div class="entry-content">
						
						<input type="text" id="filtrar-duvidas" onkeyup="filtrarDuvidas()" placeholder="Pesquise por sua dúvida">

						<!-- Dúvidas -->
						<ul id="accordion-duvidas">

							<?php

							$perguntaId = 0;

							// check if the repeater field has rows of data
							if( have_rows('perguntas') ):

							 	// loop through the rows of data
							    while ( have_rows('perguntas') ) : the_row();

							        
							?>
							<!-- início card -->
							<li class="card">
							  <div class="card-header" id="pergunta-<?php echo $perguntaId; ?>">
							    <h5 class="mb-0">
							      <button class="btn btn-link" data-toggle="collapse" data-target="#collapse-pergunta-<?php echo $perguntaId; ?>" aria-expanded="true" aria-controls="collapse-pergunta-<?php echo $perguntaId; ?>"><svg><use xlink:href="#seta-elemento"></use></svg></button>
							      <span><?php the_sub_field('pergunta'); ?></span>
							    </h5>
							  </div>

							  <div id="collapse-pergunta-<?php echo $perguntaId; ?>" class="collapse" aria-labelledby="pergunta-<?php echo $perguntaId; ?>" data-parent="#accordion-duvidas">
							    <div class="card-body">
							      <?php the_sub_field('resposta'); ?>
							    </div>
							  </div>
							</li>
							<!-- fim card -->

							<?php

								$perguntaId++;

							    endwhile;

							else :

							    // no rows found

							endif;

							?>

							

						</ul>

					</div><!-- .entry-content -->

				</article><!-- #post-## -->
			<?php
			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php get_template_part( 'template-parts/content', 'script-fixed-menu' ); ?>
<?php
get_footer();
