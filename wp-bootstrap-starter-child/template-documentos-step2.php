<?php
/**
 * Template name: - Documentos - Passo 2
 */
get_header();


?>     
 
<section id="primary" class="content-area col-sm-12 col-lg-12 contratar">
	<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
				<div class="entry-content">
					<div class="prestho-form">
						<h1>
							<strong>Enviar fotos para confirmação</strong>
							<span>Selecione cada opção abaixo e nos envie os documentos correspondentes.</span>
						</h1>

						<h2>
							Fique atento! As fotos precisam estar <br>
							legíveis e sem cortes.
						</h2>

						
						<button class="videoModal" data-video="https://www.youtube.com/embed/IC36cEyQ34o?rel=0&amp;showinfo=0" data-target="#video-explicativo">É muito importante que seus documentos sejam fotografados da maneira correta. <strong>Assista ao nosso vídeo demonstrativo</strong></button>


						<div class="container-fluid">
							<form method="POST" id="form-docs" enctype="multipart/form-data" data-path="<?php echo get_stylesheet_directory_uri(); ?>/" data-user="<?php if(isset($_SESSION['request']['step1']['solicitacao']['convenio'])): echo $_SESSION['request']['step1']['solicitacao']['convenio']; endif; ?>">
								<input type="hidden" name="negociacaoId" value="<?php if(isset($_SESSION['pedido']['negociacao'])): echo $_SESSION['pedido']['negociacao']; endif; ?>">

								<?php if(isset($_SESSION['request']['step1']['cadastro']['email'])) {
								?>
								<input type="hidden" name="email" id="email-usuario" value="<?php echo $_SESSION['request']['step1']['cadastro']['email'];?>">
								<?php } else {
								?>
								<input type="hidden" name="email" id="email-usuario" value="<?php echo $_SESSION['request']['step1']['cadastro']['telefones']['0']['ddd'] . $_SESSION['request']['step1']['cadastro']['telefones']['0']['numero'];?>@prestho.com.br">
								<?php
								}
								?>

								
								<div class="upload-fields">

									<?php if(!isset($_SESSION['docFaltante']) || $_SESSION['docFaltante'] == 'selfie' || $_SESSION['docFaltante'] == 'selfie-e-rg'): ?>
									<label for="foto-rosto" class="square selfie">
										<span class="preview"></span>
										<div class="grupo">
											<span><svg><use xlink:href="#camera"></use></svg><strong>Foto do seu rosto</strong> (selfie)</span>
											<input type="file" class="input-files" name="foto-rosto" id="foto-rosto" accept="image/jpeg,image/png" capture="camera">
										</div>
									</label>
									<?php endif; ?>

									<?php if(!isset($_SESSION['docFaltante']) || $_SESSION['docFaltante'] == 'rg' || $_SESSION['docFaltante'] == 'selfie-e-rg'): ?>
									<label for="identidadeFrente" class="square identidade-frente">
										<span class="preview"></span>
										<div class="grupo">
											<span><svg><use xlink:href="#camera"></use></svg><strong>Foto RG ou CNH</strong> (frente)</span>
											<input type="file" class="input-files" name="identidadeFrente" id="identidadeFrente" accept="image/jpeg,image/png" capture="camera">
										</div>
									</label>
									<?php endif; ?>
									
									<?php if(!isset($_SESSION['docFaltante']) || $_SESSION['docFaltante'] == 'rg' || $_SESSION['docFaltante'] == 'selfie-e-rg'): ?>
									<label for="identidadeVerso" class="square identidade-verso">
										<span class="preview"></span>
										<div class="grupo">
											<span><svg><use xlink:href="#camera"></use></svg><strong>Foto RG ou CNH</strong> (verso)</span>
											<input type="file" class="input-files" name="identidadeVerso" id="identidadeVerso" accept="image/jpeg,image/png" capture="camera">
										</div>
									</label>
									<?php endif; ?>
									
									<?php if(!isset($_SESSION['docFaltante'])): ?>
									<label for="comprovanteResidencia" class="square residencia">
										<span class="preview"></span>
										<div class="grupo">
											<span><svg><use xlink:href="#camera"></use></svg><strong>Foto Comprovante de Residência</strong></span>
											<input type="file" class="input-files" name="comprovanteResidencia" id="comprovanteResidencia" accept="image/jpeg,image/png,application/pdf" capture="camera">
										</div>
									</label>
									<?php endif; ?>
									

									<?php if(!isset($_SESSION['docFaltante'])): ?>
									<?php
										if (isset($_SESSION['request']['step1']['solicitacao']['convenio']) && $_SESSION['request']['step1']['solicitacao']['convenio'] == 5) {
									?>
									<label for="contracheque" class="square residencia">
										<span class="preview"></span>
										<div class="grupo">
											<span><svg><use xlink:href="#camera"></use></svg><strong>Foto do último Contracheque</strong></span>
											<input type="file" class="input-files" name="contracheque" id="contracheque" accept="image/jpeg,image/png,application/pdf" capture="camera">
										</div>
									</label> 
									<?php
										}
									?>
									<?php endif; ?>
								</div>

								<div class="botoes full">
									<a href="<?php echo get_home_url(); ?>/acessar-area-cliente/" class="ir-area-cliente">Ir para área do cliente</a>
								</div>
							</form>
						</div>
					</div>
				</div><!-- .entry-content -->

			</article><!-- #post-## -->
		<?php
		endwhile; // End of the loop.
		?>

	</main><!-- #main -->
</section><!-- #primary -->



<?php
get_footer();