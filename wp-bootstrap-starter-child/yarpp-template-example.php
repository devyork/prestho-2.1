<?php 
/*
YARPP Template: Prestho
Author: Adriano dos Santos
*/
?><h4 class="related-title" style="color: #324F65;
    text-align: center;
    font-size: 21px;
    font-weight: bold;
    text-transform: uppercase;
    margin: 0;
    padding: 60px 0 30px;">Você também pode gostar de:</h4>
<?php if (have_posts()):?>
<div class="row">
	<?php while (have_posts()) : the_post(); ?>
	
	<?php
		$post_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID));
		$textoWhats = rawurlencode(get_the_title() . '. Acesse em: ' . get_the_permalink());
		$facebookURL = rawurlencode(get_the_permalink());
	?>											
	<div class="col-md-6 p-3">	
		<div class="post-cont">
			<a href="<?php echo the_permalink(); ?>"><img src="<?php echo $post_image; ?>"></a>
			<div class="post-box">
				<header class="entry-header">
					<?php the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' ); ?>
					<div class="entry-meta">
						<?php echo get_the_date(); ?> | <span class="reading-time"><?php echo do_shortcode('[rt_reading_time label="" postfix="min de leitura" postfix_singular="min de leitura"]'); ?></span>
					</div><!-- .entry-meta -->

				</header><!-- .entry-header -->
				<p>
					<a href="<?php echo the_permalink(); ?>"><?php echo wp_strip_all_tags( get_the_excerpt(), true ); ?></a>
				</p>
				<div class="post-actions">
					<div class="share-post">
						<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $facebookURL; ?>" class="facebook-share" target="_blank">Facebook</a>
						<a href="https://api.whatsapp.com/send?phone=&text=<?php echo $textoWhats; ?>&source=&data=" class="whatsapp-share" target="_blank">Whatsapp</a>
					</div>
					<a class="post-link" href="<?php echo the_permalink(); ?>">Continuar Lendo</a>
				</div>
			</div>	
		</div>												
	</div>
	<?php endwhile; ?>
</div>
<?php else: ?>
<p>No related posts.</p>
<?php endif; ?>
