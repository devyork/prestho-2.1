let $TOKEN_RD = '320533bf1b0f189ce7517ede48d90911',
    jsonBen,
    negociacao,
    operacoes,
    loginCliente,
    passwordCliente,
    uploadAtual,
    wpBase,
    wpPath;

// Ações ao carregar o DOM
jQuery(function($) {
    wpBase = $('body').data('base');
    wpPath = $('body').data('path');

    let urlPage = window.location.href;

    if (urlPage.includes('window')) {
        $('.link-voltar-auto').on('click', function() {
            window.location.href = $('body').data('base');
            return false;
        });
    } else {
        $('.link-voltar-auto').on('click', function() {
            window.history.back();
            return false;
        });
    }

    $('a.menu-conta, a.link-area, #chamada-acesse a, div#barra-acoes a:nth-child(4)').on('click', function() {
        let link = $(this).attr('href');
        openLoader('Aguarde um momento, por favor.');
        setTimeout(function() {
            window.location.href = link;
        }, 1500);
        return false;
    });




    window.addEventListener("scroll", function() {

        if (this.pageYOffset <= 0) {
            if ($('#balao-dica:hidden').length) {
                $('#balao-dica').css('display', 'block');
            }
        }
        if (this.pageYOffset > 10) {
            $('header#topo').addClass('sticked');
            $('body').addClass('menu-sticked');
        } else {
            $('header#topo').removeClass('sticked');
            $('body').removeClass('menu-sticked');;
        }
    })



    $('#container-page').on("scroll", function(e) {
        var elem = $(e.currentTarget);


        if (elem.scrollTop() <= 0) {
            if ($('#balao-dica:hidden').length) {
                $('#balao-dica').css('display', 'block');
            }
        }
        if (elem.scrollTop() > 10) {
            $('header#topo').addClass('sticked');
        } else {
            $('header#topo').removeClass('sticked');
        }
    })

    $('section#primary .entry-content .blocos-ajuda').sticky({ topSpacing: 0, zIndex: 99999 });
    $('.page-template-template-lp #help-bar .blocos-ajuda').sticky({ topSpacing: 0, zIndex: 99999 });

    $(document).on('click', '.open-alert-prestho', function() {
        let content = '<p>É preciso que você selecione uma dessas opções. Caso você não seja um aposentado, pensionista do INSS ou Servidor Federal, não conseguiremos te atender, mas esperamos que, em um futuro próximo, nossos serviços sejam estendidos para mais pessoas.</p>';

        let wrapper = document.createElement('div');
        wrapper.innerHTML = content;


        swal({
            title: "A Prestho foi criada para facilitar o acesso ao Crédito Consignado de  Aposentados, Pensionistas do INSS e Servidores Federais.",
            icon: "info",
            dangerMode: true,
            closeOnClickOutside: false,
            closeOnEsc: false,
            content: wrapper,
            buttons: {
                confirm: {
                    text: "Voltar",
                    value: true,
                    visible: true,
                    className: "secondary-button",
                    closeModal: true
                }
            }
        });


        return false;
    });



    $(document).on('click', '.open-termos-prestho', function() {

        $('body').addClass('blue-modal');
        $('#privacy-policy-modal').modal('show');

        return false;
    });





    $(document).on('click', '.open-alert-matricula', function() {
        let content = '<p>A matrícula pode ser encontrada no demonstrativo de pagamento (contracheque).</p>';

        let wrapper = document.createElement('div');
        wrapper.innerHTML = content;


        swal({
            title: "Número da matrícula - SIAPE",
            icon: "info",
            dangerMode: true,
            closeOnClickOutside: false,
            closeOnEsc: false,
            content: wrapper,
            buttons: {
                confirm: {
                    text: "Voltar",
                    value: true,
                    visible: true,
                    className: "secondary-button",
                    closeModal: true
                }
            }
        });


        return false;
    });



    $(document).on('click', '.open-alert-beneficio', function() {
        let content = '<p>O número do benefício pode ser encontrado no cartão do INSS ou extrato bancário.</p>';

        let wrapper = document.createElement('div');
        wrapper.innerHTML = content;


        swal({
            title: "Número do benefício - INSS",
            icon: "info",
            dangerMode: true,
            closeOnClickOutside: false,
            closeOnEsc: false,
            content: wrapper,
            buttons: {
                confirm: {
                    text: "Voltar",
                    value: true,
                    visible: true,
                    className: "secondary-button",
                    closeModal: true
                }
            }
        });


        return false;
    });




    $(document).on('click', '.open-alert-celular', function() {
        let content = '<p>É muito importante que seu celular seja preenchido corretamente. Através dele, você receberá um SMS de <strong>confirmação e autorização do seu Crédito Consignado</strong>.</p>';

        let wrapper = document.createElement('div');
        wrapper.innerHTML = content;


        swal({
            title: "Número do celular",
            icon: "info",
            dangerMode: true,
            closeOnClickOutside: false,
            closeOnEsc: false,
            content: wrapper,
            buttons: {
                confirm: {
                    text: "Voltar",
                    value: true,
                    visible: true,
                    className: "secondary-button",
                    closeModal: true
                }
            }
        });


        return false;
    });




    // $('.link-voltar').on('click', function(){ 
    //  // console.log(this.dataset.href);

    //  if (this.dataset.href !== "#") {
    //      back(this.dataset.href);
    //      return false;
    //  } else {
    //      if($(this).attr('href').length) {

    //      } else {
    //          back();
    //          return false;
    //      }

    //  }

    // });



    if (urlPage.includes('window')) {
        $('#primary .link-voltar').each(function() {
            $('nav .link-voltar').attr({ 'href': $('body').data('base') });
        })
    } else {
        $('#primary .link-voltar').each(function() {
            $('nav .link-voltar').attr({ 'href': $(this).data('href') });
        })
    }

    // if ($('.page-template-template-simulacao-rapida').length) {
    //  openLoader();

    //  setTimeout(function(){
    //      closeLoader();
    //  }, 3000);
    // }
    if ($('.autologin').length) {
        openLoader('Aguarde enquanto verificamos seus dados.');

        let cpf = $(document).find('.autologin').attr('data-loginCliente');
        let senha = $(document).find('.autologin').attr('data-passwordCliente');

        // console.log('cpf: ' +cpf, 'senha: '+senha);

        $.ajax({
            method: "POST",
            url: $('body').data('path') + "/helpers/Login.helper.php",
            data: {
                usuario: cpf,
                senha: senha
            }
        }).done(function(data) {

            data = JSON.parse(data);
            // console.log(data);

            if (data.hasOwnProperty('message')) {
                closeLoader();

                let content = '<p>Os dados informados não constam em nosso sistema.</p>';
                content += '<p>Por favor, verifique e tente novamente.</p>';

                let wrapper = document.createElement('div');
                wrapper.innerHTML = content;

                swal({
                    title: "Usuário ou senha inválidos",
                    icon: "warning",
                    closeOnClickOutside: false,
                    closeOnEsc: false,
                    content: wrapper,
                    buttons: {
                        cancel: {
                            text: "Voltar",
                            value: true,
                            visible: true,
                            className: "outlineBlue",
                            closeModal: true
                        }
                    }
                });
            } else {



                setTimeout(function() {
                    $('#modal-loading .modal-body p').text('Aguarde só mais um pouquinho, ok?');
                }, 1000);

                setTimeout(function() {
                    let url = $('body').data('base') + '/meus-emprestimos/';
                    $.redirect(url, {
                        token: data.token
                    });
                }, 1500);

            }

        });
    }

    $('.cta-simulacao a, .faca-simulacao-menu, .faca-simulacao-home, .faca-simulacao-extra, .faca-simulacao-app').on('click', function() {
        let elemento = $('#sugestoes-credito');
        let bodyW = $('body').innerWidth();
        $('#balao-dica').css('display', 'block');

        // console.log($('body').innerWidth());

        if (bodyW < 600) {
            // console.log('menor')

            $.redirect($('body').data('base') + '/simulacao-credito/');

        } else {
            // console.log('maior')

            $('html, body').animate({
                scrollTop: ((elemento.offset().top) - 100)
            }, 1000, function() {
                elemento.addClass('shake');

                setTimeout(function() {
                    elemento.removeClass('shake');
                }, 1200)
            });


        }

        return false;

    });

    $('#balao-dica').on('click', function() {
        let elemento = $('#sugestoes-credito');
        $('#balao-dica').fadeOut(500);

        setTimeout(function() {
            elemento.addClass('shake');
        }, 600)

        setTimeout(function() {
            elemento.removeClass('shake');
        }, 1600)


        return false;
    });

    if ($('.page-template-template-contratar-step3').length) {
        let complemento;

        if ($('body').hasClass('auto-open')) {
            setTimeout(function() {
                complemento = '/contratar/endereco/';
                $('.link-voltar').each(function() {
                    $(this).attr({ 'data-href': $('body').data('base') + complemento, 'href': $('body').data('base') + complemento });
                });
            }, 1000)
        }


    }

    if ($('.page-template-template-contratar-step3.auto-open').length) {
        let complemento = '/contratar/endereco/';

        $('.link-voltar').on('click', function() {
            $.redirect($('body').data('base') + complemento);
        });


    }

    $('#sugestoes-credito ul li a, #sugestoes-credito-lp ul li a').on('click', function() {
        let value = $(this).attr('data-valor');
        if (value != 'outro') {
            quickSimulation(value, 1);
            return false;
        }

    });

    $('#sugestoes-credito #destaque-cartao .center a').on('click', function() {

        let url = $('body').data('base') + '/contratar/';

        $.redirect(url, {
            cartao: 'sim'
        });
        return false;
    });

    if ($('#campo-outro-valor').length) {
        applyMoneyMask($('#campo-outro-valor span input'));
    }

    $('#outro-valor a.simular').on('click', function() {
        anotherValue();
        return false;
    });

    $('.botoes-acao a.contratar-agora').on('click', function() {
        requestLoan($(this).data('value'));
        return false;
    });

    $('.repetir-simulacao').on('click', function() {
        $('#outro-valor a.simular').text('Simular Novamente');
    });

    $('.field input').on('focusin', function() {
        $(this).parents('.field').addClass('focus');
    });

    $('.field input').on('focusout', function() {
        if ($(this).val() == '') {
            $(this).parents('.field').removeClass('focus');
        }
    });

    $('label.info a').each(function() {
        $(this).html('<svg><use xlink:href="#info"></use></svg>');
    });

    $('[data-toggle="popover"]').on('click', function() {
        return false;
    });

    $('[data-toggle="popover"]').popover({
        trigger: 'focus'
    });

    $('.abrir-detalhes').on('click', function() { toggleDetails(); return false; });

    if ($('#cpf-cliente').length) {
        applyCPFMask($('#cpf-cliente'));
    }

    // Completa o CPF com zeros a esquerda
    $('#field-cpf').not('input[type="hidden"]').on('focusout', function() {
        var numCPF = $(this).val().replace('.', '').replace('.', '').replace('-', '');
        var lng = numCPF.toString().length;
        var zeros = '';

        if (lng < 11) {
            var z = 11 - lng;
            for (var i = z - 1; i >= 0; i--) {
                zeros += '0';
            }
            $('#field-cpf').not('input[type="hidden"]').unmask();

            numCPF = zeros + numCPF;

            $('#field-cpf').not('input[type="hidden"]').val(numCPF);
            applyCPFMask($('#field-cpf'));
            checkCPF($('#field-cpf'));

        }

    });    

    if ($('.page-template-template-login').length) {
        applyCPFMask($('#field-cpf'));
    }


    function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    };

    if ($('#step1').length) {

        applyCelMask($('#field-celular'));

        applyCPFMask($('#field-cpf'));

        $('#field-beneficio').mask('#0', { reverse: true });
        applyMoneyMask($('#field-valor'));

        $('input').each(function() {
            if ($(this).val() !== '') {
                $(this).parents('label').addClass('focus');
            }
        });

        let submit_form_btn = document.getElementById('submitStep1');
        let form = document.getElementById('step1');        

        submit_form_btn.addEventListener('click', function(e) {
            e.preventDefault();
            checkCPF($('#field-cpf'));

            // caso os campos sejam válidos
            if (form.checkValidity()) {

                let postData, url;

                $('#submitStep1').attr({ 'disabled': 'disabled' }).text('AGUARDE...');

                openLoader();
                $('html, body').animate({
                    scrollTop: (($('body').offset().top) - 100)
                }, 1000);

                if ($('#consultaCartao').val() == 'sim') {
                    url = $('body').data('path') + "/helpers/CreditCard.helper.php";
                    console.log(url);
                    postData = {
                        cpf: $('#field-cpf').val(),
                        valor: $('input[name="valor"]').val(),
                        convenio: $('input[name="solicitacao[convenio]"]:checked').val(),
                        cartao: 'sim'
                    }
                } else {
                    url = $('body').data('path') + "/helpers/Limit.helper.php";
                    postData = {
                        cpf: $('#field-cpf').val(),
                        valor: $('input[name="valor"]').val(),
                        convenio: $('input[name="solicitacao[convenio]"]:checked').val(),
                        cartao: 'nao'
                    }
                }

                $.ajax({
                        method: "POST",
                        url: url,
                        data: postData
                    })
                    .done(function(data) {

                        let json = JSON.parse(data);
                        // console.log('##############CONSULTA DE LIMITE##############');
                        // console.log(json);

                        $('#submitStep1').removeAttr('disabled').text('CONTINUAR');

                        closeLoader();

                        // Pega o primeiro nome para utilizar na próxima tela
                        let firstName = $('input[name="cadastro[nome]"]').val();
                        firstName = firstName.split(' ');
                        firstName = firstName[0];


                        // console.log('###FIRST NAME: ', firstName);

                        // monta o array com os dados adicionais do form
                        let formData = {
                            "name": $('input[name="cadastro[nome]"]').val(),
                            "cel": $('input[name="cadastro[celular]"]').val(),
                            "email": $('input[name="cadastro[email]"]').val(),
                            "cpf": $('input[name="cadastro[cpf]"]').val(),
                            "agreement": $('input[name="solicitacao[convenio]"]').val()
                        }

                        // tratamento do retorno
                        // limitReturn(json.code, json.request, json.limit);
                        loanOptions(json.benefits, json.numResultados, json.conjuntoId, formData);

                        jsonBen = {
                            code: json.code,
                            request: json.request,
                            limit: json.limit,
                            data: {
                                nome: $('input[name="cadastro[nome]"]').val(),
                                celular: $('input[name="cadastro[celular]"]').val(),
                                email: $('input[name="cadastro[email]"]').val(),
                                cpf: $('input[name="cadastro[cpf]"]').val(),
                                convenio: $('input[name="solicitacao[convenio]"]:checked').val(),
                                beneficio: '',
                                valorBeneficio: '',
                                autorizacao: $('input[name="autorizacao"]').val(),
                                path: $('input[name="path"]').val()
                            }
                        }

                        // Exibe o modal para informação do bebefício
                        // $('#cadastro2 h5 span').text(', ' + firstName);
                        // $('#cadastro2').modal('show');

                        var margem = json.limit;
                        var novaMargem;
                        switch (margem) {
                            case 'No momento você não possui margem de crédito disponível. Mas fique tranquilo, após 30 dias você pode tentar novamente.':
                                novaMargem = "[rd] Sem margem";
                                break;
                            case 'Benefício não consignável':
                                novaMargem = "[rd] Não consignável";
                                break;
                            case 'Não foi localizado nenhum benefício para o CPF informado.':
                                novaMargem = "[rd] Sem benefício";
                                break;
                            case '0,00':
                                novaMargem = "[rd] Sem margem";
                                break;
                            default:
                                novaMargem = margem;
                        }

                        var nome = formData.name;
                        var cpf = formData.cpf;
                        var convenio = formData.agreement;
                        if (convenio == 1) {
                            convenio = 'Aposentado ou Pensionista';
                        } else {
                            convenio = 'Servidor Federal';
                        }

                        var celular = formData.cel;
                        var newsletter = 0;
                        if ($('#newsletter span input').is(':checked')) {
                            var newsletter = 1;
                        }
                        var tags = [];

                        email = formData.email;
                        // console.log(email);
                        if (email == '' || email == null) {
                            email = celular.replace('-', '');
                            email = email.replace(')', '');
                            email = email.replace('(', '');
                            email = email.replace(' ', '');
                            email = email + '@prestho.com.br'
                            tags = ['sem e-mail'];
                        }


                        var identificador = 'Simulação - Primeira Etapa';
                        var data = {
                            "token_rdstation": $TOKEN_RD,
                            "identificador": identificador,
                            "email": email,
                            "nome": nome,
                            "CPF": cpf,
                            "telefone": celular,
                            "convenio": convenio,
                            "valor_desejado": json.request,
                            "valor_margem": novaMargem,
                            "tags": tags
                        }

                        jsonBen = {
                            code: json.code,
                            request: json.request,
                            limit: json.limit,
                            data: data
                        }

                        // console.log(data);
                        $.ajax({
                            url: 'https://www.rdstation.com.br/api/1.3/conversions',
                            type: 'post',
                            headers: {
                                "Content-Type": "application/json"
                            },
                            data: JSON.stringify(data),
                            beforeSend: function() {
                                // console.log('Enviando CPF '+ cpf +' ao RDStation');
                            },
                            success: function(data) {
                                // console.log('Envio com sucesso ao RDStation');
                            },
                            error(xhr, status, error) {
                                // console.log(error);
                            }
                        });

                        // Exibe o modal com os produtos
                        limitReturn(jsonBen.code, jsonBen.request, jsonBen.limit);

                        // Cria o arquivo TXT dem os dados de benefício e vai pra próxima etapa
                        let submit_form_btn_ben = document.getElementById('submitBenefitNumber');
                        let form_ben = document.getElementById('stepBenefit');

                        $("#stepBenefit").on('submit', function(e) {
                            e.preventDefault();

                            jsonBen.data.beneficio = $('input[name="beneficio"]').val();
                            jsonBen.data.valorBeneficio = $('input[name="valorBeneficio"]').val();

                            $.ajax({
                                method: "POST",
                                url: $('body').data('path') + "/helpers/File.helper.php",
                                data: {
                                    nome: $('input[name="cadastro[nome]"]').val(),
                                    celular: $('input[name="cadastro[celular]"]').val(),
                                    email: $('input[name="cadastro[email]"]').val(),
                                    cpf: $('input[name="cadastro[cpf]"]').val(),
                                    convenio: $('input[name="solicitacao[convenio]"]:checked').val(),
                                    autorizacao: $('input[name="autorizacao"]').val(),
                                    path: $('input[name="path"]').val()
                                }
                            });

                            $('#cadastro2').modal('hide');
                            limitReturn(jsonBen.code, jsonBen.request, jsonBen.limit);
                        });


                    });
                
                // CRIAÇÃO DO ARQUIVO DE LOG SEM O VALOR DO BENEFÍCIO
                $.ajax({
                  method: "POST",
                  url: $('body').data('path')+"/helpers/File.helper.php",
                  data: { 
                    nome: $('input[name="cadastro[nome]"]').val(),
                    celular: $('input[name="cadastro[celular]"]').val(),
                    email: $('input[name="cadastro[email]"]').val(),
                    cpf: $('input[name="cadastro[cpf]"]').val(),
                    convenio: $('input[name="solicitacao[convenio]"]:checked').val(),
                    // beneficio: $('input[name="beneficio"]').val(),
                    // valorBeneficio: $('input[name="valorBeneficio"]').val(),
                    autorizacao: $('input[name="autorizacao"]').val(),
                    path: $('input[name="path"]').val()
                  }
                });


            }
            // caso os campos sejam inválidos
            else {
                $('input:valid').each(function() {
                    $(this).parents('label').removeClass('error');
                });

                checkCPF($('#field-cpf'));

                var i = 0;
                $('input:invalid').each(function() {
                    if (i == 0) {
                        $(this).parents('label').addClass('error').focus();
                        i++;
                    } else {
                        $(this).parents('label').addClass('error');
                    }

                });
                form.querySelector('button[type="submit"]').click();
            }
        }, false);


        var origem = getUrlParameter('origem');
        if (origem) {            

            openLoader();            

            var nome = getUrlParameter('nome');
            var celular = getUrlParameter('celular');
            var email = getUrlParameter('email');
            var cpf = getUrlParameter('cpf'); 
            var tipo = getUrlParameter('sou');      
            

            $('#consultaCartao').val('sim');
            $('#field-nome').val(nome);
            $('#field-celular').val(celular);
            $('#field-email').val(email);
            $('#field-cpf').val(cpf);           
            $('#field-cpf').trigger('input');

            $("#field-autorizacao").prop("checked", true); 
            if(tipo == 'Aposentado ou Pensionista do INSS'){
                $('#field-aposentado').attr('checked', 'checked');
            }else{
                $('#field-siape').attr('checked', 'checked');
            }     

            // Huggy.closeBox();

            $('#submitStep1').trigger('click');
            
        }

        // Cria o arquivo TXT e vai pra próxima etapa
        // let submit_form_btn_ben = document.getElementById('submitBenefitNumber');
        // let form_ben = document.getElementById('stepBenefit');

        // $("#stepBenefit").on('submit', function(e) {
        //     e.preventDefault();

        //     jsonBen.data.beneficio = $('input[name="beneficio"]').val();
        //     jsonBen.data.valorBeneficio = $('input[name="valorBeneficio"]').val();

        //     $.ajax({
        //         method: "POST",
        //         url: $('body').data('path') + "/helpers/File.helper.php",
        //         data: {
        //             nome: $('input[name="cadastro[nome]"]').val(),
        //             celular: $('input[name="cadastro[celular]"]').val(),
        //             email: $('input[name="cadastro[email]"]').val(),
        //             cpf: $('input[name="cadastro[cpf]"]').val(),
        //             convenio: $('input[name="solicitacao[convenio]"]:checked').val(),
        //             beneficio: $('input[name="beneficio"]').val(),
        //             valorBeneficio: $('input[name="valorBeneficio"]').val(),
        //             autorizacao: $('input[name="autorizacao"]').val(),
        //             path: $('input[name="path"]').val()
        //         }
        //     });

        //     $('#cadastro2').modal('hide');
        //     limitReturn(jsonBen.code, jsonBen.request, jsonBen.limit);
        // });
    }




    $(document).on('click', '.continuar-pedido', function() {

        $('#retorno-limite').modal('hide');
        $('#retorno-simulacao').modal('show');

        return false;
    });

    $(document).on('click', '.open-toastr', function() {

        openToastr($(this).attr('title'), $(this).data('content'));

        return false;
    });


    $(document).on('click', '#toast-container', function() {

        $('body').removeClass('toast-opened');
        toastr.clear();

        return false;
    });


    $(document).on('change', '.countable', function() {

        calcTotalLoan();

        return false;
    });


    $(document).on('change', '.seguro label input', function() {
        var el = $(this);
        if (!el.is(':checked')) {
            let content = '<p><strong>Desmarcando esta opção, seu cartão e sua família não estarão protegidos e você não receberá a assistência farmácia de R$ 150,00</strong> todo mês para compra de medicamentos.</p>';
            content += '<p>Tem certeza que deseja prosseguir sem estes benefícios?</p>';

            let wrapper = document.createElement('div');
            wrapper.innerHTML = content;


            swal({
                    title: "Atenção!",
                    icon: "warning",
                    dangerMode: true,
                    closeOnClickOutside: false,
                    closeOnEsc: false,
                    content: wrapper,
                    buttons: {
                        confirm: {
                            text: "Sim",
                            value: true,
                            visible: true,
                            className: "secondary-button",
                            closeModal: true
                        },
                        cancel: {
                            text: "Não. Quero ficar seguro.",
                            value: "cancel",
                            visible: true,
                            className: "primary-button",
                            closeModal: true,
                        }
                    },
                })
                .then((value) => {
                    switch (value) {

                        case "cancel":
                            el.attr({ 'checked': 'checked' });
                            el.parents('.produto').find('.valorSeguro').val(el.val());
                            break;

                        default:
                            el.parents('.produto').find('.valorSeguro').val('null');
                            break;

                    }
                });
        } else {
            el.parents('.produto').find('.valorSeguro').val(el.val());
        }


        return false;
    });


    $(document).on('click', '.valor-simulacao button', function() {
        let val = $(this).parents('.valor-simulacao').find('input').val();

        val = val.replace(".", "");
        val = val.replace(",", ".");



        if ($(this).hasClass('save')) {
            // console.log("val [ação clique] =>", val);
            saveLoanValue($(this), val);
        } else {
            editLoanValue($(this));
        }

        return false;
    });

    // $(document).on('focusout', '.valor-simulacao input', function(){

    //  let min = parseFloat($(this).data('min')).toFixed(2);
    //  let max = parseFloat($(this).data('max')).toFixed(2);
    //  let val = $(this).val() ? parseFloat($(this).val().replace(".", "").replace(",", ".")) : min;

    // console.log('pure val: ', $(this).val());
    // console.log('val: ', val);
    // console.log('min: ', min);
    // console.log('max: ', max);

    //  $(this).unmask();

    //  if (val <= min) {
    //      $(this).val((""+$(this).data('min')).replace(".", ","));
    //  } else if(val >= max) {
    //      $(this).val((""+parseFloat($(this).data('max')).toFixed(2)).replace(".", ","));         
    //  }

    //  applyMoneyMask($(this));
    // });

    applyMoneyMask($('.campo-valor input'));

    $(document).on('click', '.confirmar-valor', function() {
        // $(document).find('#ofertas').submit();

        let simulacoes = {};

        $('.countable:checked').each(function() {
            let name = $(this).attr('name').replace('][id]', '').replace('simulacao[', '');


            let tmp = {
                beneficioID: name.charAt(0),
                nomeProduto: $('input[name="simulacao[' + name + '][nomeProduto]"]').val(),
                valorSeguro: $('input[name="simulacao[' + name + '][valorSeguro]"]').val(),
                temSeguro: $('input[name="simulacao[' + name + '][temSeguro]"]').val(),
                conjuntoId: $('input[name="simulacao[' + name + '][conjuntoId]"]').val(),
                simulationId: $(this).val(),
                type: $('input[name="simulacao[' + name + '][type]"]').val(),
                bankID: $('input[name="simulacao[' + name + '][bankID]"]').val(),
                bankName: $('input[name="simulacao[' + name + '][bankName]"]').val(),
                parcelAmount: $('input[name="simulacao[' + name + '][parcelAmount]"]').val(),
                finalValue: $('input[name="simulacao[' + name + '][finalValue]"]').val(),
                parcelValue: $('input[name="simulacao[' + name + '][parcelValue]"]').val(),
                tipoFormalizacaoId: $('input[name="simulacao[' + name + '][tipoFormalizacao]"]').val()
            }
            simulacoes[name] = tmp;

        });

        let fullPhone = $('input[name="cadastro[celular]"]').val().split(" ");
        let area = fullPhone[0].replace('(', '').replace(')', '');
        let num = fullPhone[1].replace('-', '');
        let celular = [{
            ddd: area,
            numero: num,
            tipoTelefoneId: 2
        }];

        let cadastro = {
            nome: $('input[name="cadastro[nome]"]').val(),
            email: $('input[name="cadastro[email]"]').val(),
            cpf: $('input[name="cadastro[cpf]"]').val(),
            tipoDocumentoPessoaId: 1,
            documentoNumero: 123456,
            documentoOrgao: "SSP",
            documentoUf: "AC",
            sexo: "M",
            nacionalidade: "",
            brasileiro: true,
            ufNascimento: "MG",
            cidadeNascimento: "Uberlândia",
            estadoCivilId: 1,
            nomeConjuge: "",
            nomePai: "Não consta",
            nomeMae: "Não consta",
            telefones: celular
        }

        let solicitacao = {
            convenio: $('input[name="solicitacao[convenio]"]:checked').val()
        }

        let url = $('body').data('base') + '/contratar/dados-bancarios/';

        // console.log(simulacoes);

        $.redirect(url, {
            step1: {
                simulacoes: simulacoes,
                cadastro: cadastro,
                solicitacao: solicitacao
            }
        });

        return false;
    });


    if ($('#dados-bancarios').length) {
        let url = $('body').data('base') + '/contratar/endereco/';

        let submit_form_btn = document.getElementById('submit-dados-bancarios');
        let form = document.getElementById('dados-bancarios');

        submit_form_btn.addEventListener('click', function(e) {
            e.preventDefault();

            // caso os campos sejam válidos
            if (form.checkValidity()) {

                let dadosBancarios = {
                    banco: $('#select-banco').val(),
                    agencia: $('#field-agencia').val(),
                    agenciaDigito: $('#field-digito-agencia').val(),
                    tipoConta: $('input[name="dadosBancarios[tipoConta]"]:checked').val(),
                    conta: $('#field-conta').val(),
                    contaDigito: $('#field-digito-conta').val()
                };

                $.redirect(url, {
                    step2: {
                        dadosBancarios: dadosBancarios
                    }
                });


            }
            // caso os campos sejam inválidos
            else {
                $('input:valid').each(function() {
                    $(this).parents('label').removeClass('error');
                });

                var i = 0;
                $('input:invalid').each(function() {
                    if (i == 0) {
                        $(this).parents('label').addClass('error').focus();
                        i++;
                    } else {
                        $(this).parents('label').addClass('error');
                    }

                });
                form.querySelector('button[type="submit"]').click();
            }
        }, false);
    }


    if ($('#form-endereco').length) {
        let url = $('body').data('base') + '/contratar/endereco/';

        let submit_form_btn = document.getElementById('submit-form-endereco');
        let form = document.getElementById('form-endereco');

        submit_form_btn.addEventListener('click', function(e) {
            e.preventDefault();

            // caso os campos sejam válidos
            if (form.checkValidity()) {

                form.submit();


            }
            // caso os campos sejam inválidos
            else {
                $('input:valid').each(function() {
                    $(this).parents('label').removeClass('error');
                });

                var i = 0;
                $('input:invalid').each(function() {
                    if (i == 0) {
                        $(this).parents('label').addClass('error').focus();
                        i++;
                    } else {
                        $(this).parents('label').addClass('error');
                    }

                });
                form.querySelector('button[id="submit-form-endereco"]').click();
            }
        }, false);
    }



    $(document).on('change', '#field-aposentado', function() {
        var el = $(this);
        if (el.is(':checked')) {
            $('#field-beneficio').parents('label').find('span').text('Número do benefício');
            $('#field-valor').parents('label').find('span').html('Valor do benefício');

            $('#field-beneficio').parents('label').find('a').removeClass('open-alert-matricula').addClass('open-alert-beneficio');
            $('#cadastro2 h6').text('Como você é Aposentado ou Pensionista do INSS, precisamos que confirme abaixo as informações para continuarmos com sua proposta de crédito consignado:');

        }
    });

    $(document).on('change', '#field-siape', function() {
        var el = $(this);
        if (el.is(':checked')) {
            $('#field-beneficio').parents('label').find('span').text('Número da matrícula');
            $('#field-valor').parents('label').find('span').html('Valor do salário');
            $('#field-beneficio').parents('label').find('a').removeClass('open-alert-beneficio').addClass('open-alert-matricula');
            $('#cadastro2 h6').text('Como você é Servidor Federal, precisamos que confirme abaixo as informações para continuarmos com sua proposta de crédito consignado:');

        }
    });

    $("#cep").blur(function() {

        let cep = sanitizeCEP($(this).val());

        if (cep != "") {
            if (cepValidate(cep)) {
                waitForAddress();
                viacepWebservice(cep);
            } else {
                clearAddressFields();
                swal("Atenção!", "Formato de CEP inválido.", "error");
            }
        } else {
            clearAddressFields();
        }

    });

    if ($('.celular-cadastrado').text().length) {
        applyCelMask($('.celular-cadastrado'));
    }

    $('.alterar-celular').on('click', function() {
        $('#trocar-celular').modal('show');
        return false;
    });

    $('.alterar-email').on('click', function() {
        $('#trocar-email').modal('show');
        return false;
    });

    applyCelMask($('#novo-celular'));
    applyCEPMask($('#cep'));

    $('#salvar-novo-celular').on('click', function() {

        $('#trocar-celular').modal('hide');

        setTimeout(function() {
            $('body').addClass('blue-modal');
            $('#modal-confirmation-phone').modal('show');
        }, 500);

        return false;
    });

    $('#modal-confirmation-phone a').on('click', function() {
        $('#form-altera-celular').submit();
        return false;
    });

    $('#salvar-novo-email').on('click', function() {

        $('#trocar-email').modal('hide');

        setTimeout(function() {
            $('body').addClass('blue-modal');
            $('#modal-confirmation-email').modal('show');
        }, 500);


        return false;
    });

    $('#modal-confirmation-email a').on('click', function() {
        $('#form-altera-email').submit();
        return false;
    });


    // $('.confirmar-endereco').on('click', function(e){
    //  e.preventDefault(); //prevent the default action

    //  $('#form-endereco').submit();
    //  return false;
    // });

    $('.confirmar-proposta').on('click', function(e) {

        openLoader('Aguarde enquanto registramos sua proposta.');

        $.ajax({
            method: "POST",
            url: $('body').data('path') + "/helpers/Proposal.helper.php"
        }).done(function(data) {


            data = JSON.parse(data);
            // console.log('#############ENVIO DA PROPOSTA#############');
            // console.log(data);


            if (data.hasOwnProperty('message')) {
                closeLoader();

                let content = '<p>' + data.message + '</p>';
                content += '<p>Por favor, verifique e tente novamente.</p>';

                let wrapper = document.createElement('div');
                wrapper.innerHTML = content;

                swal({
                    title: "Opa!",
                    icon: "warning",
                    closeOnClickOutside: false,
                    closeOnEsc: false,
                    content: wrapper,
                    buttons: {
                        cancel: {
                            text: "Voltar",
                            value: true,
                            visible: true,
                            className: "outlineBlue",
                            closeModal: true
                        }
                    }
                });
            } else {

                $email = $('input[name="emailRD"]').val();

                finalizouContratacao($TOKEN_RD, $email);

                negociacao = data.negociacao;
                operacoes = data.operacoes;
                loginCliente = data.loginCliente;
                passwordCliente = data.passwordCliente;

                // console.log('#############ENVIANDO O ACEITE.....#############');
                $.ajax({
                    method: "POST",
                    url: $('body').data('path') + "/helpers/UploadAgreement.helper.php",
                    data: { negociacao: negociacao }
                }).done(function (data) {
                    data = JSON.parse(data);
                    // console.log('#############ENVIO DO ACEITE#############');
                    // console.log(data);
                });

                let content = '<p>Para validação e aprovação da sua proposta, envie agora na próxima tela as fotos de seus documentos.</p>';

                let wrapper = document.createElement('div');
                wrapper.innerHTML = content;

                swal({
                    title: "Atenção",
                    icon: "warning",
                    closeOnClickOutside: false,
                    closeOnEsc: false,
                    content: wrapper,
                    buttons: {
                        cancel: {
                            text: "ok",
                            value: true,
                            visible: true,
                            className: "yellowButton",
                            closeModal: true
                        }
                    }
                }).then((value) => {
                    $('#modal-loading').modal('hide');
                    $('#modal-proposta').modal('show');                    
                });

                



                // let url = $('body').data('base')+'/documentos/';
                // $.redirect(url, {
                //  pedido: {
                //      negociacao: data.negociacao,
                //      operacoes: data.operacoes,
                //      loginCliente: data.loginCliente,
                //      passwordCliente: data.passwordCliente
                //  }
                // });
            }


        });
        return false;
    });

    $(document).on('click', '.enviar-fotos-agora', function() {
        let url = $('body').data('base') + '/documentos/';
        $.redirect(url, {
            pedido: {
                negociacao: negociacao,
                operacoes: operacoes,
                loginCliente: loginCliente,
                passwordCliente: passwordCliente
            }
        });
        return false;
    });

    $(document).on('click', '.ir-area-cliente', function() {
        // console.log(loginCliente, passwordCliente);
        let url = $('body').data('base') + '/acessar-area-cliente/';
        $.redirect(url, {
            autologin: true,
            pedido: {
                loginCliente: loginCliente,
                passwordCliente: passwordCliente
            }
        });
        return false;
    });

    if ($('body').hasClass('auto-open')) {
        setTimeout(function() {
            toggleDetails();
        }, 700);
    }


    // Insere a miniatura do arquivo selecionado e comprime a imagem 
    $(".input-files:file").change(function() {
        let fileName = $(this).val();
        let fieldName = $(this).attr('name');
        let elementoPai = $(this).parents('.square').find('.preview');
        let theFile = this.files[0];

        if (fileName !== "") {
            // console.log(theFile.size);

            $(this).parents('.square').addClass('selected');

            // comprime a imagem
            new ImageCompressor(theFile, {
                quality: .3,
                success(result) {
                    const formData = new FormData();
                    $(this).val(result);
                    //   console.log(result);

                    //   openLoader();
                    uploadAtual = fieldName;
                    // console.log("UPLOAD ATUAL: ", fieldName);
                    $('#form-docs').submit();

                    // exibe a miniatura
                    var loadingImage = loadImage(
                        theFile,
                        function(img) {
                            elementoPai.html(img);
                        }, { maxWidth: 800, maxHeight: 800, cover: true, noRevoke: true, canvas: true, crop: true }
                    );

                },
                error(e) {
                    //   console.log(e.message);
                },
            });

        } else {
            $(this).parents('.square').removeClass('selected');
            var elements = $(this).parents('.square').find('.elements > div').clone();
            $(this).parents('.form-group').find('label').html(elements);

        }

        // verifica quantos arquivos foram selecionados
        //    let $count = 0;
        //    $(".input-files:file").each(function(){
        //         let fileName2 = $(this).val();
        //         if (fileName2 !== "") {
        //            $count++;
        //        }
        //  });

        // Se pelo menos um arquivo for selecionado, habilita o botão de upload
        // if ($count >= 1 ) {
        //  $('.botoes button').removeAttr('disabled');

        // } else {
        //  $('.botoes button').attr('disabled', 'disabled');
        // }

        // if ($('#form-docs').data('user') == 5) {
        // // Se os cinco arquivos foram selecionados, habilita o botão de envio
        //  if ($count == 5) {
        //    $('.botoes button').removeAttr('disabled');

        //  } else {
        //      $('.botoes button').attr('disabled', 'disabled');
        //  }
        // } else {
        //  // Se os quatro arquivos foram selecionados, habilita o botão de envio
        //  if ($count == 4) {
        //    $('.botoes button').removeAttr('disabled');

        //  } else {
        //      $('.botoes button').attr('disabled', 'disabled');
        //  }
        // }


    });


    var options = {
        url: $('body').data('path') + "/helpers/Upload.helper.php",
        type: "post",
        beforeSubmit: showRequest, // pre-submit callback 
        uploadProgress: progressBar,
        success: showResponse, // post-submit callback 
        error: showError, // post-submit callback 
        clearForm: true,
        resetForm: true

        // other available options: 
        //url:       url         // override for form's 'action' attribute 
        //type:      type        // 'get' or 'post', override for form's 'method' attribute 
        //dataType:  null        // 'xml', 'script', or 'json' (expected server response type) 
        //clearForm: true        // clear all form fields after successful submit 
        //resetForm: true        // reset the form after successful submit 

        // $.ajax options can be used here too, for example: 
        //timeout:   3000 
    };

    $('#form-docs').ajaxForm(options);


    $(document).on('click', 'a.info-seguro', function($) {

        return false;
    });


    $(document).on('click', '.infoEmprestimo', function() {

        let content = '<p>O Empréstimo Consignado tem uma das linhas de crédito mais baratas do mercado! </p>';
        content += '<p>Ele está disponível para Aposentados, Pensionistas do INSS e Servidores Federais. </p>';
        content += '<p>Nessa modalidade, as parcelas são descontadas diretamente do seu salário, pensão ou aposentadoria, o que oferece várias vantagens, como:</p>';
        content += '<ul>';
        content += '<li class="amarelo">taxas de juros menores</li>';
        content += '<li class="amarelo">facilidade para contratar</li>';
        content += '<li class="amarelo">prazos mais longos</li>';
        content += '</ul>';

        let wrapper = document.createElement('div');
        wrapper.innerHTML = content;

        swal({
            title: "O que é Empréstimo Consignado?",
            icon: "info",
            closeOnClickOutside: false,
            closeOnEsc: false,
            content: wrapper,
            buttons: {
                confirm: {
                    text: "Voltar",
                    value: true,
                    visible: true,
                    className: "outlineBlue",
                    closeModal: true
                }
            }
        });

        setTimeout(function() {
            if ($('.swal-modal').offset().top > 0) {
                $('.swal-overlay').animate({
                    scrollTop: (($('.swal-modal').offset().top) - 100)
                }, 1000);
            }

        }, 100);

        return false;
    });


    $(document).on('click', '.infoSaque', function() {

        let content = '<p>É uma forma rápida de ter dinheiro. <i>O limite do seu Cartão de Crédito Consignado</i> é transferido diretamente para a sua conta, mesmo que o Cartão ainda não tenha sido entregue em seu endereço.</p>';
        content += '<p><strong>Como realizar o pagamento?</strong><br>Um grande diferencial é que o saque não é parcelado. O valor mínimo da fatura é descontado no seu benefício / contracheque sem ultrapassar a margem de 5%. O restante pode ser liquidado através de pagamentos espontâneos, dando a você a autonomia de determinar o prazo de quitação da fatura.</p>';


        let wrapper = document.createElement('div');
        wrapper.innerHTML = content;

        swal({
            title: "O que é Saque Complementar?",
            icon: "info",
            closeOnClickOutside: false,
            closeOnEsc: false,
            content: wrapper,
            buttons: {
                confirm: {
                    text: "Voltar",
                    value: true,
                    visible: true,
                    className: "outlineBlue",
                    closeModal: true
                }
            }
        });

        setTimeout(function() {
            $('.swal-overlay').animate({
                scrollTop: (($('.swal-modal').offset().top) - 100)
            }, 1000);
        }, 100);

        return false;
    });


    $(document).on('click', '.infoCartao', function() {

        let content = '<p>Um Cartão internacional completo, ideal para fazer compras, pagar contas ou realizar saques em dinheiro.</p>';
        content += '<ul>';
        content += '<li>Grátis - sem adesão</li>';
        content += '<li>Sem consulta ao SPC/Serasa</li>';
        content += '<li>Sem taxa de anuidade</li>';
        content += '</ul>';
        content += '<p><strong>Saques em dinheiro do Cartão?</strong><br>É uma forma rápida de ter dinheiro. Após a aprovação do seu Cartão de Crédito Consignado, o limite é transferido diretamente para a sua conta. O Cartão não tem taxa de anuidade e será entregue em até 15 dias úteis, no endereço informado em seu cadastro.</p>';
        content += '<p><strong>Como realizar o pagamento?</strong><br>Um grande diferencial é que o saque não é parcelado. O valor mínimo da fatura é descontado no seu benefício sem ultrapassar a margem de 5%, o restante pode ser liquidado através de pagamentos espontâneos, dando a você a autonomia de determinar o prazo de quitação da fatura.</p>';


        let wrapper = document.createElement('div');
        wrapper.innerHTML = content;

        swal({
            title: "Cartão de Crédito Consignado",
            icon: "info",
            closeOnClickOutside: false,
            closeOnEsc: false,
            content: wrapper,
            buttons: {
                confirm: {
                    text: "Voltar",
                    value: true,
                    visible: true,
                    className: "outlineBlue",
                    closeModal: true
                }
            }
        });

        setTimeout(function() {
            $('.swal-overlay').animate({
                scrollTop: (($('.swal-modal').offset().top) - 100)
            }, 1000);
        }, 100);

        return false;
    });

    $('#recuperar-senha').on('click', function() {

        let content = '<p>Esqueceu seu dados de acesso?</p>';
        content += '<p>Não se preocupe! É só entrar em contato com a gente que vamos te ajudar.</p>';

        let wrapper = document.createElement('div');
        wrapper.innerHTML = content;

        swal({
            title: "Recuperação de usuário ou senha",
            icon: "info",
            closeOnClickOutside: false,
            closeOnEsc: false,
            content: wrapper,
            buttons: {
                cancel: {
                    text: "Voltar",
                    value: false,
                    visible: true,
                    className: "outlineBlue",
                    closeModal: true
                },
                confirm: {
                    text: "Chame no Chat",
                    value: true,
                    visible: true,
                    className: "yellowButton",
                    closeModal: true
                }
            }
        }).then((value) => {
            if (value == true) {
                Huggy.openBox();
            }
        });

        return false;
    });

    $('#login-prestho').on('click', 'button', function() {
        openLoader('Aguarde enquanto verificamos seus dados.');
        let cpf = $('#field-cpf').val();
        cpf = cpf.replace('.', '');
        cpf = cpf.replace('.', '');
        cpf = cpf.replace('-', '');

        $.ajax({
            method: "POST",
            url: $('body').data('path') + "/helpers/Login.helper.php",
            data: {
                usuario: cpf,
                senha: $('#field-senha').val()
            }
        }).done(function(data) {

            data = JSON.parse(data);
            // console.log(data);

            if (data.hasOwnProperty('message')) {
                closeLoader();

                let content = '<p>Os dados informados não constam em nosso sistema.</p>';
                content += '<p>Por favor, verifique e tente novamente.</p>';

                let wrapper = document.createElement('div');
                wrapper.innerHTML = content;

                swal({
                    title: "Usuário ou senha inválidos",
                    icon: "warning",
                    closeOnClickOutside: false,
                    closeOnEsc: false,
                    content: wrapper,
                    buttons: {
                        cancel: {
                            text: "Voltar",
                            value: true,
                            visible: true,
                            className: "outlineBlue",
                            closeModal: true
                        }
                    }
                });
            } else {

                // closeLoader();

                setTimeout(function() {
                    $('#modal-loading .modal-body p').text('Aguarde só mais um pouquinho, ok?');
                }, 1000);

                setTimeout(function() {
                    let url = $('body').data('base') + '/meus-emprestimos/';
                    $.redirect(url, {
                        token: data.token
                    });
                }, 1500);




                // $.ajax({
                //   method: "POST",
                //   url: $('body').data('path')+"/helpers/User.helper.php",
                //   data: {
                //      token: data.token
                //   }
                // }).done(function(data) {
                //  console.log(data);
                //  let url = $('body').data('base')+'/meus-emprestimos/';
                //  $.redirect(url);

                // });
            }

        });
        return false;
    });

    $('.link-saiba-mais, .fecha-menu').on('click', function() {
        $('body').toggleClass('open-menu');

        return false;
    });

    $('.page-template-template-meus-emprestimos #accordion .card .btn-link').on('click', function() {
        $(this).parents('.card').toggleClass('show');
    });

    $('.complemento-sugestoes a.faca-simulacao-home').on('click', function() {
        let elemento = $('#sugestoes-credito');
        elemento.addClass('shake');

        setTimeout(function() {
            elemento.removeClass('shake');
        }, 1000)
        return false;
    });

    $(".videoModal").click(function() {
        $('body').addClass('blue-modal');

        var theModal = $(this).data("target"),
            videoSRC = $(this).attr("data-video"),
            videoSRCauto = videoSRC + "?modestbranding=1&rel=0&controls=0&showinfo=0&html5=1&autoplay=1";
        $(theModal + ' iframe').attr('src', videoSRCauto);
        $(theModal + ' button.close').click(function() {
            $(theModal + ' iframe').attr('src', videoSRC);
        });

        $('#video-explicativo').modal('show');
    });

    $(document).on('click', '.send-photos', function() {
        let url = $(this).attr('href');
        let operation = $(this).data('operation');
        let missing = $(this).data('missing');

        $.redirect(url, {
            operacao: operation,
            docFaltante: missing
        });

        return false;
    });

    if ($('.alerta-fotos').length) {
        let missing = $('.alerta-fotos .send-photos').data('missing'),
            operation = $('.alerta-fotos .send-photos').data('operation'),
            html;

        if (missing == 'selfie-e-rg') {
            html = '<h6>DOCUMENTOS PARA APROVAÇÃO DO CRÉDITO</h6>';
            html += '<ul>';
            html += '  <li class="selfie missing">';
            html += '   <a href="' + wpBase + '/documentos/" class="send-photos" data-missing="' + missing + '" data-operation="' + operation + '">';
            html += '     <i class="ok"></i>';
            html += '     <i class="alerta"></i>';
            html += '     <i class="icone"></i>';
            html += '     <span>Selfie</span>';
            html += '   </a>';
            html += '  </li>';
            html += '  <li class="rg missing">';
            html += '   <a href="' + wpBase + '/documentos/" class="send-photos" data-missing="' + missing + '" data-operation="' + operation + '">';
            html += '     <i class="ok"></i>';
            html += '     <i class="alerta"></i>';
            html += '     <i class="icone"></i>';
            html += '     <span>Frente <br>RG/CNH</span>';
            html += '   </a>';
            html += '  </li>';
            html += '  <li class="rg2 missing">';
            html += '   <a href="' + wpBase + '/documentos/" class="send-photos" data-missing="' + missing + '" data-operation="' + operation + '">';
            html += '     <i class="ok"></i>';
            html += '     <i class="alerta"></i>';
            html += '     <i class="icone"></i>';
            html += '     <span>Verso <br>RG/CNH</span>';
            html += '   </a>';
            html += '  </li>';
            html += '</ul> ';
            html += '<div class="alert-message"><span>Há documentos pendentes de envio.<br>As fotos precisam estar legíveis e sem cortes.</span></div>';

            $('#documentos-faltantes').html(html);
        }

        if (missing == 'selfie') {
            html = '<h6>DOCUMENTOS PARA APROVAÇÃO DO CRÉDITO</h6>';
            html += '<ul>';
            html += '  <li class="selfie missing">';
            html += '   <a href="' + wpBase + '/documentos/" class="send-photos" data-missing="' + missing + '" data-operation="' + operation + '">';
            html += '     <i class="ok"></i>';
            html += '     <i class="alerta"></i>';
            html += '     <i class="icone"></i>';
            html += '     <span>Selfie</span>';
            html += '   </a>';
            html += '  </li>';
            html += '  <li class="rg sent">';
            html += '   <a href="' + wpBase + '/documentos/" class="send-photos" data-missing="' + missing + '" data-operation="' + operation + '">';
            html += '     <i class="ok"></i>';
            html += '     <i class="alerta"></i>';
            html += '     <i class="icone"></i>';
            html += '     <span>Frente <br>RG/CNH</span>';
            html += '   </a>';
            html += '  </li>';
            html += '  <li class="rg2 sent">';
            html += '   <a href="' + wpBase + '/documentos/" class="send-photos" data-missing="' + missing + '" data-operation="' + operation + '">';
            html += '     <i class="ok"></i>';
            html += '     <i class="alerta"></i>';
            html += '     <i class="icone"></i>';
            html += '     <span>Verso <br>RG/CNH</span>';
            html += '   </a>';
            html += '  </li>';
            html += '</ul> ';
            html += '<div class="alert-message"><span>Há documentos pendentes de envio.<br>As fotos precisam estar legíveis e sem cortes.</span></div>';

            $('#documentos-faltantes').html(html);
        }

        if (missing == 'rg') {
            html = '<h6>DOCUMENTOS PARA APROVAÇÃO DO CRÉDITO</h6>';
            html += '<ul>';
            html += '  <li class="selfie sent">';
            html += '   <a href="' + wpBase + '/documentos/" class="send-photos" data-missing="' + missing + '" data-operation="' + operation + '">';
            html += '     <i class="ok"></i>';
            html += '     <i class="alerta"></i>';
            html += '     <i class="icone"></i>';
            html += '     <span>Selfie</span>';
            html += '   </a>';
            html += '  </li>';
            html += '  <li class="rg missing">';
            html += '   <a href="' + wpBase + '/documentos/" class="send-photos" data-missing="' + missing + '" data-operation="' + operation + '">';
            html += '     <i class="ok"></i>';
            html += '     <i class="alerta"></i>';
            html += '     <i class="icone"></i>';
            html += '     <span>Frente <br>RG/CNH</span>';
            html += '   </a>';
            html += '  </li>';
            html += '  <li class="rg2 missing">';
            html += '   <a href="' + wpBase + '/documentos/" class="send-photos" data-missing="' + missing + '" data-operation="' + operation + '">';
            html += '     <i class="ok"></i>';
            html += '     <i class="alerta"></i>';
            html += '     <i class="icone"></i>';
            html += '     <span>Verso <br>RG/CNH</span>';
            html += '   </a>';
            html += '  </li>';
            html += '</ul> ';
            html += '<div class="alert-message"><span>Há documentos pendentes de envio.<br>As fotos precisam estar legíveis e sem cortes.</span></div>';

            $('#documentos-faltantes').html(html);
        }
    }
});






/*
 *************
 **
 ** Funções
 **
 *************
 */

function isOverflown(element) {
    return element.scrollHeight > element.clientHeight || element.scrollWidth > element.clientWidth;
}

function modalSeguro(valSeguro, $ = jQuery) {

    let content = '<p>O seguro é uma proteção extra para sua família. Ele <strong>quita a fatura do cartão</strong> em até R$ 4.000,00 para INSS e R$ 8.000,00 para Siape.</p>';
    content += '<ul>';
    content += '<li>A diferença entre o saldo devedor e o limite de indenização será paga para a sua família.</li>';
    content += '<li>A contratação não interfere no valor da sua margem consignável.</li>';
    content += '<li>Assistência farmácia: <strong>Remédio</strong> Genérico <strong>GRATUITO</strong> em casos de atendimento emergencial. <strong style="color: #e89b04;">TODO MÊS R$ 150,OO PARA VOCÊ COMPRAR REMÉDIOS.</strong></li>';
    content += '<li>Poteção de seus dados: Você Protegido (proteção dos seus dados pessoais e financeiros na internet). O serviço é liberado após o cadastro no site <a href="https://voceprotegido.com" target="_blank" style="color: #e89b04;">www.voceprotegido.com</a></li>';
    content += '<li>Sorteios mensais: <strong style="color: #e89b04;">TODO MÊS, DURANTE 1 ANO, VOCÊ CONCORRE A 2 MIL REAIS.</strong></li>';
    content += '</ul>';
    content += '<br>';
    content += '<p>Valor para contratar Seguro Prestamista: <strong style="color: #e89b04;">R$ ' + valSeguro + '</strong></p>';
    content += '<p>As condições gerais do seguro estão disponíveis para consulta no site <a href="https://www.generali.com.br" target="_blank" style="color: #e89b04;">www.generali.com.br</a></p>';

    let wrapper = document.createElement('div');
    wrapper.innerHTML = content;

    swal({
        title: "Seguro Prestamista",
        icon: "info",
        closeOnClickOutside: false,
        closeOnEsc: false,
        content: wrapper,
        buttons: {
            confirm: {
                text: "Voltar",
                value: true,
                visible: true,
                className: "outlineBlue",
                closeModal: true
            }
        }
    });

    setTimeout(function() {
        if ($('.swal-modal').offset().top > 0) {
            $('.swal-overlay').animate({
                scrollTop: (($('.swal-modal').offset().top) - 100)
            }, 1000);
        }

    }, 100);

    return false;
}

function filtrarDuvidas() {
    // Declare variables
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById('filtrar-duvidas');
    filter = input.value.toUpperCase();
    ul = document.getElementById("accordion-duvidas");
    li = ul.getElementsByTagName('li');

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("span")[0];
        txtValue = a.textContent || a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}

function finalizouContratacao($token, $email, $ = jQuery) {
    var data = {
            "token_rdstation": $token,
            "identificador": 'Finalizou processo de contratação',
            "email": $email
        }
        // console.log(data);
    $.ajax({
        url: 'https://www.rdstation.com.br/api/1.3/conversions',
        type: 'post',
        headers: {
            "Content-Type": "application/json"
        },
        data: JSON.stringify(data),
        beforeSend: function() {
            // console.log('Atualizando informações do e-mail ' + $email);
        },
        success: function(data) {
            // console.log('Envio com sucesso ao RDStation');
        },
        error(xhr, status, error) {
            // console.log(error);
        }
    });
}

function enviouImagem($token, $email, $tag, $ = jQuery) {
    var data = {
            "token_rdstation": $token,
            "identificador": 'Finalizou processo de contratação',
            "email": $email,
            "tags": [$tag]
        }
        // console.log(data);
    $.ajax({
        url: 'https://www.rdstation.com.br/api/1.3/conversions',
        type: 'post',
        headers: {
            "Content-Type": "application/json"
        },
        data: JSON.stringify(data),
        beforeSend: function() {
            // console.log('Atualizando informações do e-mail ' + $email);
        },
        success: function(data) {
            // console.log('Envio com sucesso ao RDStation');
        },
        error(xhr, status, error) {
            // console.log(error);
        }
    });
}


function openFileSuccess($ = jQuery) {
    $('body').addClass('blue-modal');
    $('#modal-upload').modal('show');
}

// pre-submit callback 
function showRequest(formData, jqForm, options, $ = jQuery) {

    // console.log('@@@@@@BEFORE SUBMIT');
    // console.log(uploadAtual);
    openLoader('<h4>Estamos enviando sua foto</h4><p style="max-width: 300px;">Estas fotos são importantes para aprovação do seu crédito.<br><br>Aguarde uns instantes…</p>', 'html');
    // $('.botoes button').attr('disabled', 'disabled');

    // openLoader();

    return true;
}

// progresso do upload
function progressBar(event, position, total, percentComplete, $ = jQuery) {
    var percentVal = percentComplete + '%';

    // console.log(percentVal);

    $('#modal-loading .modal-body p').html("<strong>" + percentVal + "</strong><br>Estamos enviando seus documentos.");
    if (percentComplete == 100) {
        $('#modal-loading .modal-body p').text("Aguarde só mais um pouquinho. Estamos finalizando o envio dos seus documentos.");
    }
}


// post-submit callback 
function showResponse(responseText, statusText, xhr, $form, $token = $TOKEN_RD, $ = jQuery) {
    // console.log('@@@@@@SUCCESS');
    // console.log(uploadAtual);

    // console.log('post-submit callback #######################'); 
    // console.log('responseText', responseText); 
    // console.log('statusText', statusText); 
    // console.log('xhr', xhr); 
    // console.log('$form', $form); 

    // console.log(responseText[1].message);

    // console.log($.parseJSON(responseText));

    var $r = $.parseJSON(responseText);

    var $message = '';
    var error = 0;
    var $email = $('#email-usuario').val();
    var $tag = $r[1][0].rd_return;
    // var $tag = responseText[1].rd_return;

    // $('.botoes button').removeAttr('disabled');

    if (statusText == 'success') {


        if ($r[1].hasOwnProperty('message')) {
            // console.log("SUCSESS, BUT ERROR FROM API ##########################");
            $message += $r[1].message + ' ';
            error++;
        } else {
            closeLoader();

            modalSucessoUpload();
            // openFileSuccess();

            enviouImagem($token, $email, $tag);
        }


    } else {
        // console.log("ERROR FROM API ##########################");
        // console.log($r);

        if ($r[1].hasOwnProperty('message')) {
            $message += $r[1].message + ' ';
            error++;
        }

        // if ($r[0].hasOwnProperty('message')) {
        //     $message += 'Erro ao enviar a selfie. '+$r[0].message+' ';
        //     error++;
        // }

        // if ($r[1].hasOwnProperty('message')) {
        //     $message += 'Erro ao enviar o RG ou CNH. '+$r[1].message+' ';
        //     error++;
        // }

        // if ($r[2].hasOwnProperty('message')) {
        //     $message += 'Erro ao enviar o comprovante de endereço. '+$r[1].message+' ';
        //     error++;
        // }


    }

    if (error == 0) {
        closeLoader();
        // openFileSuccess();
        modalSucessoUpload();

        enviouImagem($token, $email, $tag);

    } else {
        closeLoader();
        modalErroUpload();
    }

}

function modalSucessoUpload($ = jQuery) {
    let c = '',
        w = '',
        t = '',
        i = 'info';
    switch (uploadAtual) {
        case 'foto-rosto':
            c = '<p>Com esta foto, daremos andamento <br>na aprovação do seu crédito. <br>É para sua segurança!<br><br><strong>Agora, basta aguardar.</strong></p>';

            w = document.createElement('div');
            w.innerHTML = c;
            t = "Recebemos sua selfie";
            i = wpPath + "/images/alertIcons/selfie.svg";


            break;
        case 'identidadeFrente':
            c = '<p>Para aprovação de sua proposta, este documento será analisado. Obrigado!<br><br><strong>Agora, basta aguardar.</strong></p>';

            w = document.createElement('div');
            w.innerHTML = c;

            t = "Foto da frente recebida";
            i = wpPath + "/images/alertIcons/rgFrente.svg";

            break;
        case 'identidadeVerso':
            c = '<p>Para aprovação de sua proposta, este documento será analisado. Obrigado!<br><br><strong>Agora, basta aguardar.</strong></p>';

            w = document.createElement('div');
            w.innerHTML = c;

            t = "Foto do Verso recebida";
            i = wpPath + "/images/alertIcons/rgVerso.svg";

            break;
        case 'comprovanteResidencia':
            c = '<p>Analisamos estes documentos para sua segurança. <br>Fique tranquilo!<br><br><strong>Agora, basta aguardar.</strong></p>';

            w = document.createElement('div');
            w.innerHTML = c;

            t = "Foto do Comprovante de residência recebida.";
            i = wpPath + "/images/alertIcons/residencia.svg";

            break;
        case 'contracheque':
            c = '<p>Analisamos estes documentos para sua segurança. <br>Fique tranquilo!<br><br><strong>Agora, basta aguardar.</strong></p>';

            w = document.createElement('div');
            w.innerHTML = c;

            t = "Foto do Contracheque recebida.";
            i = wpPath + "/images/alertIcons/residencia.svg";

            break;
        default:
            c = '<p>Analisamos estes documentos para sua segurança. <br>Fique tranquilo!<br><br><strong>Agora, basta aguardar.</strong></p>';

            w = document.createElement('div');
            w.innerHTML = c;

            t = "Foto recebida.";
            i = "info";

            break;
    }

    swal({
        title: t,
        icon: i,
        closeOnClickOutside: false,
        closeOnEsc: false,
        content: w,
        buttons: {
            cancel: {
                text: "Próximo passo",
                value: true,
                visible: true,
                className: "yellowButton",
                closeModal: true
            }
        }
    });
}


function modalErroUpload($ = jQuery) {
    let c = '',
        w = '',
        t = '',
        i = 'alert';


    c = '<p>Aconteceu algo no envio de sua foto. <br>Tente novamente, por gentileza.</p>';

    w = document.createElement('div');
    w.innerHTML = c;
    t = "Opa. Houve algum problema";
    i = wpPath + "/images/alertIcons/alerta.svg";


    swal({
        title: t,
        icon: i,
        closeOnClickOutside: false,
        closeOnEsc: false,
        content: w,
        buttons: {
            cancel: {
                text: "Tentar novamente",
                value: true,
                visible: true,
                className: "yellowButton",
                closeModal: true
            }
        }
    });

    $('label[for="' + uploadAtual + '"]').removeClass('selected');
}

// error callback 
function showError(responseText, statusText, xhr, $form, $ = jQuery) {


    // console.log('error callback ##################'); 
    // console.log('responseText', responseText); 
    // console.log('statusText', statusText); 
    // console.log('xhr', xhr); 
    // console.log('$form', $form); 

    var errorMessage = 'statusText: ' + statusText + ' <br><br>responseText: ' + JSON.stringify(responseText) + ' <br><br>xhr: ' + xhr + ' <br><br>';

    swal({
        title: "Erro!",
        text: "Ocorreu um erro ao enviar seus arquivos. Por favor, tente novamente. [ERR01]" + errorMessage,
        icon: "error"
    }).then((value) => {
        closeLoader();
    });

    // $('.overlay-loading p.progress').html('');

    // $('.overlay-loading p:not(.progress)').text('Ocorreu um erro ao enviar seus arquivos. Por favor, tente novamente.');
    // $('.overlay-loading p:not(.progress)').html('statusText: '+statusText+' <br><br>responseText: '+JSON.stringify(responseText)+' <br><br>xhr: '+xhr+' <br><br>');
    // $('.overlay-loading').addClass('erro');
}




























function openLoader(text = 'Só um minutinho! Estamos analisando a melhor proposta de crédito pra você.', type = 'text', $ = jQuery) {
    $('.modal').modal('hide');
    setTimeout(function() {
        if (type == 'text') {
            $('body').addClass('blue-modal');
            $('#modal-loading .modal-body p').text(text);
            $('#modal-loading').modal('show');
        } else {
            $('body').addClass('blue-modal');
            $('#modal-loading .modal-body').html('');
            $('#modal-loading .modal-body').html(text);
            $('#modal-loading').modal('show');
        }
    }, 300)

}

function closeLoader($ = jQuery) {
    $('section#primary').css({ 'opacity': 1 });
    $('#modal-loading .modal-body').html('<p></p>');
    $('#modal-loading').modal('hide');
    $('body').removeClass('blue-modal');
}


Number.prototype.formatMoney = function(c, d, t) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

function applyCelMask(element, $ = jQuery) {
    let SPMaskBehavior = function(val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };

    element.mask(SPMaskBehavior, spOptions);
}

function applyCPFMask(element, $ = jQuery) {
    element.mask('000.000.000-00').on('keyup', function() {
        checkCPF($(this));
    });
}

function applyCEPMask(element, $ = jQuery) {
    element.mask('00000-000');
}

function applyMoneyMask(element, $ = jQuery) {
    element.mask("#.##0,00", { reverse: true });
}

function editLoanValue(element, $ = jQuery) {
    let target = element.parents('.valor-simulacao');
    target.addClass('editable');
    target.find('input').removeAttr('disabled');
    target.find('button').addClass('save').text('SALVAR');
}

function saveLoanValue(element, value, $ = jQuery) {
    var target = element.parents('.valor-simulacao').find('input');


    // Dados necessários para realizar a alteração de valor
    var dataContainer = element.parents('.produto');
    var conjuntoID = element.parents('.produto').find('.conjuntoId').val(),
        simulacaoID = element.parents('.produto').find('.countable').val(),
        tipoValor = element.parents('.produto').find('.type').val(),
        min = Number(target.data('min')).formatMoney(2, ",", "."),
        max = target.data('max').formatMoney(2, ",", ".");


    target.parents('.produto').addClass('disabled');

    console.log('tipoValor => ', tipoValor);
    console.log('value => ', value);

    $.ajax({
        method: "POST",
        url: $('body').data('path') + "/helpers/Loan.helper.php",
        data: {
            conjuntoID: conjuntoID,
            simulacaoID: simulacaoID,
            tipoValor: tipoValor,
            valor: value
        }
    }).done(function(data) {

        let json = JSON.parse(data);
        console.log(json);
        console.log("value DONE ===> ", value);
        console.log("min ===> ", min);
        console.log("max ===> ", max);

        if (json.hasOwnProperty('message')) {

            $.ajax({
                method: "POST",
                url: $('body').data('path') + "/helpers/Loan.helper.php",
                data: {
                    conjuntoID: conjuntoID,
                    simulacaoID: simulacaoID,
                    tipoValor: tipoValor,
                    valor: target.data('max')
                }
            }).done(function(data) {
                target.val(max);
                let json = JSON.parse(data);

                // console.log('###################TRATAMENTO VALOR FORA DO RANGE################');
                // console.log(json);
                dataContainer.find('.countable').val(json.simulacaoId);
                target.parents('.produto').find('.finalValue').val(target.data('max'));
                target.parents('.produto').find('.parcelAmount').val(json.prazo);
                target.parents('.produto').find('.parcelValue').val(json.valorParcela);
                target.parents('.produto').find('.parcelamento span').text('Pagamento em ' + json.prazo + 'x R$' + (json.valorParcela).formatMoney(2, ",", "."));
                target.parents('.produto').find('.valorSeguro').val(json.valorSeguro);
                target.parents('.produto').find('.bankID').val(json.bancoCod);
                target.parents('.produto').find('.bankName').val(json.bancoNome);
                calcTotalLoan();
            });

            swal({
                title: "Atenção!",
                text: "Você pode escolher um valor entre R$" + min + " e R$" + max,
                icon: "error"
            });

        } else {
            dataContainer.find('.countable').val(json.simulacaoId);
            target.parents('.produto').find('.finalValue').val(value);
            target.parents('.produto').find('.parcelAmount').val(json.prazo);
            target.parents('.produto').find('.parcelValue').val(json.valorParcela);
            target.parents('.produto').find('.parcelamento span').text('Pagamento em ' + json.prazo + 'x R$' + (json.valorParcela).formatMoney(2, ",", "."));
            target.parents('.produto').find('.valorSeguro').val(json.valorSeguro);
            target.parents('.produto').find('.bankID').val(json.bancoCod);
            target.parents('.produto').find('.bankName').val(json.bancoNome);
            calcTotalLoan();
        }



        target.parents('.valor-simulacao').removeClass('editable');
        target.attr({ 'disabled': 'disabled' });
        target.parents('.valor-simulacao').find('button').removeClass('save').text('EDITAR VALOR');
        target.parents('.produto').removeClass('disabled');

        // tratamento do retorno
        // limitReturn(json.code, json.request, json.limit);
        // loanOptions(json.benefits, json.numResultados);


    });


}

function checkSecure($ = jQuery) {
    $('.countable:checked').each(function() {
        $(this).parents('.produto').find('.seguro input').attr({ 'checked': 'checked' }).removeAttr('disabled');
    });

    $('.countable:not(:checked)').each(function() {
        $(this).parents('.produto').find('.seguro input').removeAttr('checked').attr({ 'disabled': 'disabled' });
    });
}

function calcTotalLoan($ = jQuery) {
    let sum = 0;
    checkSecure();

    $('.countable:not(:checked)').each(function() {
        $(this).parents('.produto').addClass('disabled');
    });

    $('.countable:checked').each(function() {
        // console.log($(this).parents('.produto').find('.finalValue').val());
        $(this).parents('.produto').removeClass('disabled');
        sum += Number($(this).parents('.produto').find('.finalValue').val());
    });


    $('#total-emprestimo span').text(sum.formatMoney(2, ",", "."));
}

function quickSimulation(value = 300, agreement = 1, $ = jQuery) {

    if (value != 'outro') {

        openLoader();

        $.ajax({
            method: "POST",
            url: $('body').data('path') + "/helpers/QuickSimulation.helper.php",
            data: {
                valor: value
            }
        }).done(function(data) {
            let json = JSON.parse(data);
            // console.log(data);
            let url = $('body').data('base') + '/simulacao-rapida/'
            $.redirect(url, json);

        });
    }

}

// function cardSimulation(value = 300, agreement = 1, $ = jQuery) {

//  if(value != 'outro') {

//      openLoader();

//      $.ajax({
//        method: "POST",
//        url: $('body').data('path')+"/helpers/CreditCard.helper.php",
//        data: { 
//          valor: value
//        }
//      }).done(function( data ) {
//          let json = JSON.parse(data);
//          // console.log(data);
//          let url = $('body').data('base')+'/simulacao-rapida/'
//          $.redirect(url, json);

//      });
//  }

// }

function requestLoan(value = 300, $ = jQuery) {

    let url = $('body').data('base') + '/contratar/'
    $.redirect(url, {
        valor: value
    });

}

function anotherValue($ = jQuery) {
    let min = Number($('#campo-outro-valor span input').data('min').replace('.', '').replace(',', '.'));
    let max = Number($('#campo-outro-valor span input').data('max').replace('.', '').replace(',', '.'));

    let value = Number($('#campo-outro-valor span input').val().replace('.', '').replace(',', '.'));

    if (value < min || value > max) {
        swal("Atenção!", $('#outro-valor .limites').text(), "error");
    } else {
        quickSimulation(value, 1);
    }
}

function back($url = false) {
    if ($url == false) {
        window.history.back();
    } else {
        window.location.href = $url;
    }
}

function toggleDetails($ = jQuery) {
    $('body').toggleClass('detalhes');

    $('#conteudo-detalhes').slideToggle(1000, function() {

        if ($('#conteudo-detalhes').css('opacity') == 0) {
            $('#conteudo-detalhes').css({ 'opacity': 1 });
        } else {
            $('#conteudo-detalhes').css({ 'opacity': 0 });
        }

    });

    setTimeout(function() {
        $('html, body').animate({
            scrollTop: (($('body').offset().top) - 100)
        }, 1000);
    }, 500);
    // setTimeout(function(){
    //  $('#content').toggleClass('d-none');
    // }, 900);
}

/*
    Abre a notificação Toast e popula com os dados
*/

function openToastr(title, text, $ = jQuery) {
    $('body').addClass('toast-opened');

    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-center",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "1",
        "hideDuration": "1",
        "timeOut": "0",
        "extendedTimeOut": "0",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }

    toastr.success(text + '<a href="#" class="close-toast">FECHAR</a>', title);
}

/*
    Trata o retorno e formata o HTML da consulta de limite
*/
function limitReturn(code, request, limit, $ = jQuery) {

    request = Number(request).toLocaleString('pt-br', { minimumFractionDigits: 2 });
    limit = Number(limit).toLocaleString('pt-br', { minimumFractionDigits: 2 });

    // console.log('####################Função limitReturn()######################');
    // console.log('Request: ', request);
    // console.log('Limit: ', limit);
    // console.log('####################Fim da função limitReturn()######################');

    if (code == 1) {
        // Altera o backdrop para azul sólido
        $('body').addClass('blue-modal');

        $('#retorno-limite').removeClass('no-limit');

        $('#retorno-limite .modal-header').html('<h5>Supimpa!</h5>');

        $('#retorno-limite .modal-body')
            .html('<h5>VOCÊ SOLICITOU <strong>R$' + request + '</strong></h5><p>e você tem disponível para contratação:</p><h6>' + limit + '</h6><p>Continue para detalhar mais como prefere organizar a sua solicitação:</p><hr/><div class="buttons"><button class="continuar-pedido">Continuar</button></div>');

        $('#retorno-limite').modal('show');

    } else if (code == 0) {

        // Altera o backdrop para azul sólido
        $('body').addClass('blue-modal');

        $('#retorno-limite').addClass('no-limit');

        $('#retorno-limite .modal-header').html('<h5>Opa!</h5>');

        $('#retorno-limite .modal-body')
            .html('<h5>O valor solicitado não está disponível pra você.</h5><p>Mas fique sossegado! Já identificamos uma proposta exclusiva para seu perfil no valor de:</p><h6>' + limit + '</h6><hr/><div class="buttons"><button class="continuar-pedido">Continuar</button></div>');

        $('#retorno-limite').modal('show');

    } else {
        $('#sem-beneficio').modal('show');
    }
}

/*
    Monta as opções de empréstimo após a consulta de limite
*/
function loanOptions(obj, totalItens, conjuntoId, formData, $ = jQuery) {
    var count = 1,
        count2 = 0,
        prod = '',
        valorTotal = 0,
        textoSimula, valorVariavel;

    if (totalItens <= 1) {
        textoSimula = 'Encontramos 01 oportunidade de crédito disponível para você.';
    } else {
        totalItens = (Number(totalItens) > 10) ? totalItens : '0' + totalItens;
        textoSimula = 'Encontramos ' + totalItens + ' oportunidades de crédito disponíveis para você.';
    }


    prod += '<form method="post" id="ofertas" action="' + $('body').data("base") + '/contratar/dados-bancarios/">';
    prod += '<input type="hidden" name="cadastro[nome]" value="' + formData["name"] + '">';
    prod += '<input type="hidden" name="cadastro[celular]" value="' + formData["cel"] + '">';
    prod += '<input type="hidden" name="cadastro[email]" value="' + formData["email"] + '">';
    prod += '<input type="hidden" name="cadastro[cpf]" value="' + formData["cpf"] + '">';
    prod += '<input type="hidden" name="solicitacao[convenio]" value="' + formData["agreement"] + '">';



    $('#retorno-simulacao .modal-header').html('<h5>Supimpa!</h5><h6>' + textoSimula + '</h6>');

    $.each(obj, function(key, item) {

        prod += '<div class="beneficio"><p class="titulo-beneficio">OFERTA 0' + count + ' <span>Selecione abaixo os itens desta oferta para contratação:</span></p><div class="produtos">';

        $.each(item, function(k, i) {
            valorVariavel = 0;
            // console.log(item);

            // se o produto não possui seguro prestamista
            if (i.produto.valorLiquidoSeguro == null) {
                valorTotal += i.produto.valorLiquido;
                valorVariavel = i.produto.valorLiquido;
            } else {
                valorTotal += i.produto.valorLiquidoSeguro;
                valorVariavel = i.produto.valorLiquidoSeguro;
            }

            // if (i.valorSeguro == null && i.produto.valorSeguro == null) {
            if (i.produto.prodId == 1 || i.produto.prodId == 19) {


                prod += '<div class="produto">';

                prod += '<input type="hidden" class="benefitID" name="benefitID" value="' + count + '">';
                prod += '<input type="hidden" class="conjuntoId" name="simulacao[' + k + '][conjuntoId]" value="' + conjuntoId + '">';
                prod += '<input type="hidden" class="nomeProduto" name="simulacao[' + k + '][nomeProduto]" value="' + i.descricao + '">';
                prod += '<input type="hidden" class="type" name="simulacao[' + k + '][type]" value="' + i.produto.tipo + '">';
                prod += '<input type="hidden" class="bankID" name="simulacao[' + k + '][bankID]" value="' + i.produto.bancoCod + '">';
                prod += '<input type="hidden" class="bankName" name="simulacao[' + k + '][bankName]" value="' + i.produto.bancoNome + '">';
                prod += '<input type="hidden" class="parcelAmount" name="simulacao[' + k + '][parcelAmount]" value="' + i.produto.prazo + '">';
                prod += '<input type="hidden" class="finalValue" name="simulacao[' + k + '][finalValue]" value="' + i.produto.valorLiquido + '">';
                prod += '<input type="hidden" class="valorSeguro" name="simulacao[' + k + '][valorSeguro]" value="' + i.produto.valorSeguro + '">';
                prod += '<input type="hidden" class="temSeguro" name="simulacao[' + k + '][temSeguro]" value="false">';
                prod += '<input type="hidden" class="parcelValue" name="simulacao[' + k + '][parcelValue]" value="' + i.produto.valorParcela + '">';
                prod += '<input type="hidden" class="tipoFormalizacao" name="simulacao[' + k + '][tipoFormalizacao]" value="' + i.produto.tiposFormalizacao[0] + '">';

                prod += '   <label>';
                prod += '       <input type="checkbox" name="simulacao[' + k + '][id]" value="' + i.produto.simulacaoId + '" checked="checked" class="countable"><span>' + i.descricao + '</span>';
                prod += '       <a href="#" class="info infoEmprestimo"><svg><use xlink:href="#info"></use></svg></a>';
                prod += '   </label>';
                prod += '   <div class="valor-simulacao"><span class="campo-valor"><input type="text" name="simulacao[' + k + '][valor]" value="' + (i.produto.valorLiquido).formatMoney(2, ",", ".") + '" data-min="300.00" data-max="' + i.produto.valorLiquido + '" maxlength="10" disabled></span>';
                prod += '       <button>EDITAR VALOR</button>';
                prod += '       <div class="alerta-valor">';
                prod += '           <svg class="alerta">';
                prod += '               <use xlink:href="#alerta"></use>';
                prod += '           </svg>Você pode escolher um valor entre <strong>R$300,00 e R$' + (i.produto.valorLiquido).formatMoney(2, ",", ".") + '</strong>';
                prod += '       </div>';
                prod += '   </div>';
                prod += '   <div class="parcelamento">';
                prod += '           <span>Pagamento em ' + i.produto.prazo + 'x R$' + (i.produto.valorParcela).formatMoney(2, ",", ".") + '</span>';
                prod += '   </div>';
                prod += '   <div class="taxas d-none">';
                prod += '       <div class="linha">';
                prod += '           <div class="taxa"><span>2,05%</span><small>TAXA MÁXIMA (A.M.)</small></div>';
                prod += '           <div class="taxa"><span>27,57%</span><small>TAXA MÁXIMA (A.A.)</small></div>';
                prod += '           <!--<div class="taxa"><span>1,75%</span><small>CET (A.M.)</small></div>';
                prod += '           <div class="taxa"><span>23,54%</span><small>CET (A.A.)</small></div>-->';
                prod += '       </div>';
                prod += '       <!--<div class="linha">';
                prod += '           <div class="taxa"><span>R$1.077,29</span><small>VALOR IOF (0,03%)</small></div>';
                prod += '       </div>-->';
                prod += '   </div>';
                prod += '</div>';
            } else {

                if (i.produto.prodId == 28) {
                    var complemento = ' <u>+ Saque do limite do cartão de crédito</u>';
                    var info = ' infoCartao';



                    if (i.produto.valorSeguro == null) {
                        var showSeguro = '';
                        var temSeguro = 'false';
                    } else {
                        var showSeguro = '  <div class="seguro">';
                        showSeguro += '     <label>';
                        showSeguro += '         <input type="checkbox" name="simulacao[' + k + '][seguro]" value="' + i.produto.valorSeguro + '" checked="checked"><span>+ SEGURO PRESTAMISTA <u>Garantia de tranqulidade para sua família</u></span>';
                        showSeguro += '         <a href="#" class="info info-seguro" onclick="modalSeguro(\'' + i.produto.valorSeguro + '\');"><svg><use xlink:href="#info"></use></svg></a>';
                        showSeguro += '     </label>';
                        showSeguro += '     <div class="valor-simulacao"></div>';
                        showSeguro += ' </div>';
                        var temSeguro = 'true';
                    }
                } else {
                    var complemento = ' <u>Saque do limite do cartão de crédito consignado</u>';
                    var info = ' infoSaque';
                    if (i.produto.valorSeguro == null) {
                        var showSeguro = '';
                        var temSeguro = 'false';
                    } else {
                        var showSeguro = '  <div class="seguro">';
                        showSeguro += '     <label>';
                        showSeguro += '         <input type="checkbox" name="simulacao[' + k + '][seguro]" value="' + i.produto.valorSeguro + '" checked="checked"><span>+ SEGURO PRESTAMISTA <u>Garantia de tranqulidade para sua família</u></span>';
                        showSeguro += '         <a href="#" class="info info-seguro" onclick="modalSeguro(\'' + i.produto.valorSeguro + '\');"><svg><use xlink:href="#info"></use></svg></a>';
                        showSeguro += '     </label>';
                        showSeguro += '     <div class="valor-simulacao"></div>';
                        showSeguro += ' </div>';
                        var temSeguro = 'true';
                    }

                }



                // monta o HTML do produto (SAQUE COMPLEMENTAR / CARTÃO + SAQUE)
                prod += '<div class="produto">';

                prod += '<input type="hidden" class="benefitID" name="benefitID" value="' + count + '">';
                prod += '<input type="hidden" class="conjuntoId" name="simulacao[' + k + '][conjuntoId]" value="' + conjuntoId + '">';
                prod += '<input type="hidden" class="nomeProduto" name="simulacao[' + k + '][nomeProduto]" value="' + i.descricao + '">';
                prod += '<input type="hidden" class="type" name="simulacao[' + k + '][type]" value="' + i.produto.tipo + '">';
                prod += '<input type="hidden" class="bankID" name="simulacao[' + k + '][bankID]" value="' + i.produto.bancoCod + '">';
                prod += '<input type="hidden" class="bankName" name="simulacao[' + k + '][bankName]" value="' + i.produto.bancoNome + '">';
                prod += '<input type="hidden" class="parcelAmount" name="simulacao[' + k + '][parcelAmount]" value="' + i.produto.prazo + '">';
                prod += '<input type="hidden" class="finalValue" name="simulacao[' + k + '][finalValue]" value="' + valorVariavel + '">';
                prod += '<input type="hidden" class="valorSeguro" name="simulacao[' + k + '][valorSeguro]" value="' + i.produto.valorSeguro + '">';
                prod += '<input type="hidden" class="temSeguro" name="simulacao[' + k + '][temSeguro]" value="' + temSeguro + '">'
                prod += '<input type="hidden" class="parcelValue" name="simulacao[' + k + '][parcelValue]" value="' + i.produto.valorParcela + '">';
                prod += '<input type="hidden" class="tipoFormalizacao" name="simulacao[' + k + '][tipoFormalizacao]" value="' + i.produto.tiposFormalizacao[0] + '">';

                prod += '   <label>';
                prod += '       <input type="checkbox" name="simulacao[' + k + '][id]" value="' + i.produto.simulacaoId + '" checked="checked" class="countable pack"><span>' + i.descricao + complemento + '</span>';
                prod += '       <a href="#" class="info' + info + '"><svg><use xlink:href="#info"></use></svg></a>';
                prod += '   </label>';
                prod += '   <div class="valor-simulacao"><span class="campo-valor"><input type="text" name="simulacao[' + k + '][valor]" value="' + (valorVariavel).formatMoney(2, ",", ".") + '" data-min="50.00" data-max="' + valorTotal + '" maxlength="10" disabled></span>';
                prod += '       <button>EDITAR VALOR</button>';
                prod += '       <div class="alerta-valor">';
                prod += '           <svg class="alerta">';
                prod += '               <use xlink:href="#alerta"></use>';
                prod += '           </svg>Você pode escolher um valor entre <strong>R$50,00 e R$' + (valorTotal).formatMoney(2, ",", ".") + '</strong>';
                prod += '       </div>';
                prod += '   </div>';
                prod += '   <div class="taxas d-none">';
                prod += '       <div class="linha">';
                prod += '           <div class="taxa"><span>2,05%</span><small>TAXA MÁXIMA (A.M.)</small></div>';
                prod += '           <div class="taxa"><span>27,57%</span><small>TAXA MÁXIMA (A.A.)</small></div>';
                prod += '           <!--<div class="taxa"><span>1,75%</span><small>CET (A.M.)</small></div>';
                prod += '           <div class="taxa"><span>23,54%</span><small>CET (A.A.)</small></div>-->';
                prod += '       </div>';
                prod += '       <!--<div class="linha">';
                prod += '           <div class="taxa"><span>R$1.077,29</span><small>VALOR IOF (0,03%)</small></div>';
                prod += '       </div>-->';
                prod += '   </div>';
                prod += '   <div class="clearfix"></div>';
                prod += showSeguro;
                prod += '</div>';

            }

            count2++;

        });

        prod += '</div></div>';

        count++;
    });

    prod += '</form>';

    $('#retorno-simulacao .modal-body').html(prod);
    $('#retorno-simulacao .modal-footer').html('<div id="total-emprestimo"><p>VALOR TOTAL SELECIONADO</p><i>R$</i><span>' + valorTotal.formatMoney(2, ",", ".") + '</span></div><div class="buttons"><button class="confirmar-valor">Eu quero!</button></div>');

}

// validação de CPF
function checkCPF(el) {
    // pego o valor do campo a ser verificado
    var value = el.val();

    // removo os pontos e traços
    value = value.replace(".", "");
    value = value.replace("-", "");
    value = value.replace(".", "");

    /*
        Aqui eu verifico algumas condições:
        - se o valor informado não tem 11 dígitos
        - se o valor informado é algum padrão burro (números repetidos)
    */
    if (value.length != 11 ||
        value == "00000000000" ||
        value == "11111111111" ||
        value == "22222222222" ||
        value == "33333333333" ||
        value == "44444444444" ||
        value == "55555555555" ||
        value == "66666666666" ||
        value == "77777777777" ||
        value == "88888888888" ||
        value == "99999999999") {

        // caso seja algum dos casos acima, adiciona a classe de erro na label do meu campo
        el.parents('label').addClass('error');

        // return false para parar a execução do código
        return false;
    } else {
        /* 
            caso passe na validação anterior, inicia a conta para conferir 
            o dígito verificador do CPF.

            Inicialmente, soma-se todos os números (exceto o dígito verificador), 
            multiplicando antes cada um deles de 10 a 2.

        */
        var soma = 0;
        // primeiro dígito * 10
        soma = soma + (parseInt(value.substring(0, 1))) * 10;
        // primeiro dígito + (segundo dígitop * 9)
        soma = soma + (parseInt(value.substring(1, 2))) * 9;
        // etc
        soma = soma + (parseInt(value.substring(2, 3))) * 8;
        soma = soma + (parseInt(value.substring(3, 4))) * 7;
        soma = soma + (parseInt(value.substring(4, 5))) * 6;
        soma = soma + (parseInt(value.substring(5, 6))) * 5;
        soma = soma + (parseInt(value.substring(6, 7))) * 4;
        soma = soma + (parseInt(value.substring(7, 8))) * 3;
        soma = soma + (parseInt(value.substring(8, 9))) * 2;
    }

    /*
        pega-se a noma anterior, multiplica-se por 10 e divide-se por 11.
        Aqui, usamos a função de módulo (%) em vez da divisão. 
        Isso porque o que realmente interessa ra gente é o resto da divisão
    */
    var resto1 = (soma * 10) % 11;

    /*
        se o resto da divisão for 10 ou 11, o primeiro´número do
        dígito verificador é 0
    */
    if ((resto1 == 10) || (resto1 == 11)) {
        resto1 = 0;
    }


    /*
        Agora, soma-se todos os números (exceto o último número do dígito verificador), 
        multiplicando antes cada um deles de 11 a 2.
    */
    var soma = 0;
    soma = soma + (parseInt(value.substring(0, 1))) * 11;
    soma = soma + (parseInt(value.substring(1, 2))) * 10;
    soma = soma + (parseInt(value.substring(2, 3))) * 9;
    soma = soma + (parseInt(value.substring(3, 4))) * 8;
    soma = soma + (parseInt(value.substring(4, 5))) * 7;
    soma = soma + (parseInt(value.substring(5, 6))) * 6;
    soma = soma + (parseInt(value.substring(6, 7))) * 5;
    soma = soma + (parseInt(value.substring(7, 8))) * 4;
    soma = soma + (parseInt(value.substring(8, 9))) * 3;
    soma = soma + (parseInt(value.substring(9, 10))) * 2;

    /*
        pega-se a noma anterior, multiplica-se por 10 e divide-se por 11.
        Aqui, usamos a função de módulo (%) em vez da divisão. 
        Isso porque o que realmente interessa ra gente é o resto da divisão
    */
    var resto2 = (soma * 10) % 11;

    /*
        se o resto da nossa segunda divisão for 10 ou 11, o segundo número do
        dígito verificador é 0
    */
    if ((resto2 == 10) || (resto2 == 11)) {
        resto2 = 0;
    }


    // verifica se os números encontrados do dígito verificado existem e se estão na posição correta
    if (
        (resto1 == (parseInt(value.substring(9, 10)))) &&
        (resto2 == (parseInt(value.substring(10, 11))))) {

        // cpf válido, remove-se a classe de erro
        el.parents('label').removeClass('error');
        return true;
    } else {

        // cpf inválido, adiciona-se a classe de erro
        el.parents('label').addClass('error');
        return false;
    }

}







function clearAddressFields($ = jQuery) {
    $("#rua").val("");
    $("#bairro").val("");
    $("#cidade").val("");
    $("#uf").val("");
    $("#ibge").val("");
}

function sanitizeCEP(c, $ = jQuery) {
    return c.replace(/\D/g, '');
}

function cepValidate(c, $ = jQuery) {
    let v = /^[0-9]{8}$/;
    return v.test(c);
}

function waitForAddress($ = jQuery) {
    $("#logradouro").val("...").parents('label').addClass('focus');
    $("#bairro").val("...").parents('label').addClass('focus');
    $("#cidade").val("...").parents('label').addClass('focus');
    $("#uf").val("...").parents('label').addClass('focus');
}

function viacepWebservice(cep, $ = jQuery) {
    $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function(dados) {
        if (!("erro" in dados)) {
            let dadosLogr = dados.logradouro;
            let logr = dadosLogr.split(' ');
            let idLogr;

            fillAddress(logr, dadosLogr, dados.bairro, dados.localidade, dados.uf);
        } else {
            clearAddressFields();
            swal("Atenção!", "CEP não encontrado.", "error");
        }
    });
}

function fillAddress(logr, dadosLogr, bairro, localidade, uf, $ = jQuery) {
    switch (logr[0]) {
        case "Rua":
            idLogr = "1";
            break;

        case "Avenida":
            idLogr = "2";
            break;

        case "Rodovia":
            idLogr = "3";
            break;

        case "Travessa":
            idLogr = "4";
            break;

        case "Praça":
            idLogr = "5";
            break;

        case "Alameda":
            idLogr = "6";
            break;

        case "Quadra":
            idLogr = "7";
            break;

        case "Estrada":
            idLogr = "8";
            break;

        case "Beco":
            idLogr = "9";
            break;

        case "Conjunto":
            idLogr = "10";
            break;

        default:
            idLogr = 0;
            break;
    }

    $("#tipoLogradouroId").val(idLogr);
    $("#logradouro").val(dadosLogr.substr(dadosLogr.indexOf(" ") + 1));
    $("#bairro").val(bairro);
    $("#cidade").val(localidade);
    $("#uf").val(uf);
}