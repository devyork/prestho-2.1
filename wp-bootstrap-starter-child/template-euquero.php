<?php
/**
 * Template name: - LP - Eu Quero
 */

get_header('euquero'); ?>

	<section id="primary" class="content-area col-sm-12 col-lg-12">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					
				    <header class="entry-header">
						<h1>Empréstimo Consignado para Servidores Federais, Aposentados e Pensionistas do INSS</h1>
					</header><!-- .entry-header -->
				   
					
					<div class="entry-content">
						<?php get_template_part( 'template-parts/content', 'help' ); ?>
						<div class="container-sugestoes">
							<?php get_template_part( 'template-parts/content', 'cellphone' ); ?>
							<div class="complemento-sugestoes">
								<h4>Seu sonho
								está em suas
								mãos.</h4>
								<p>Para fazer aquela viagem, comprar um carro novo ou reformar a casa, conte com a Prestho! Crédito consignado de maneira rápida, fácil e a qualquer momento.</p>
								<a href="#" class="faca-simulacao-home">Faça uma Simulação</a>
							</div>
						</div>
						
						
						<!-- bloco diferenciais -->
						<div id="diferenciais">
							<div class="container-diferenciais">
								<div class="imagem-diferenciais"></div>
								<div class="lista-diferenciais">
									<h2>Realize seus sonhos com a ajuda da Prestho</h2>
									<?php get_template_part( 'template-parts/content', 'lista-diferenciais' ); ?>
								</div>
							</div>
						</div>
						<!-- fim do bloco diferenciais -->

					</div><!-- .entry-content -->

				</article><!-- #post-## -->
			<?php
			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php get_template_part( 'template-parts/content', 'script-fixed-menu' ); ?>
<?php
get_footer();
