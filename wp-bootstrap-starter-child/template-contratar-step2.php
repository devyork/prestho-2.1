<?php
/**
 * Template name: - Contratar - Passo 2
 */
get_header();

if (isset($_SESSION['request']['step2']['dadosBancarios'])) {

	?>

	<script>
		jQuery(document).ready(function($){
		    var theValue = "<?php  echo $_SESSION['request']['step2']['dadosBancarios']['banco']; ?>";
		    $('option[value=' + theValue + ']')
		        .attr('selected',true);
		});
	</script>

	<?php

	$agencia_usuario = $_SESSION['request']['step2']['dadosBancarios']['agencia'];

	if (isset( $_SESSION['request']['step2']['dadosBancarios']['agenciaDigito'])) {
		$agencia_digito = $_SESSION['request']['step2']['dadosBancarios']['agenciaDigito'];
	} else {
		$agencia_digito = '';
	}
	
	$conta_usuario = $_SESSION['request']['step2']['dadosBancarios']['conta'];
	$digito_conta = $_SESSION['request']['step2']['dadosBancarios']['contaDigito'];

	if ($_SESSION['request']['step2']['dadosBancarios']['tipoConta'] == 1) {
		$corrente = ' checked="checked"';
		$poupanca = '';
	} else {
		$corrente = '';
		$poupanca = ' checked="checked"';
	}

	$focus = ' focus';

} else {
	$agencia_usuario = "";
	$agencia_digito = "";
	$conta_usuario = "";
	$digito_conta = "";
	$corrente = '';
	$poupanca = '';
	$focus = '';
}

?>


<section id="primary" class="content-area col-sm-12 col-lg-12 contratar">
	<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
				<div class="entry-content">
					<div class="prestho-form">
						<h1>
							<strong>Empréstimo Consignado</strong>
							<small>*Sujeito à análise de crédito</small>
						</h1>

						<?php 
							$texto_convenio = '';

							if (isset($_SESSION['request']['step1']['solicitacao'])) {
								if ($_SESSION['request']['step1']['solicitacao']['convenio'] == '1') {
									$texto_convenio = 'Deve ser a mesma conta em que você recebe o 
									<strong>benefício do INSS</strong>';
								} else {
									$texto_convenio = 'Deve ser a mesma conta que consta em seu contracheque para 
									<strong>pagamento do salário</strong>';
								}
							}
						?>

						<p>
							Legal!<br>
							Agora, informe a conta para depósito do crédito.<br>
							<?php echo $texto_convenio; ?>.
						</p>

						<form action="#" id="dados-bancarios">
							<fieldset>
								<label for="select-banco" class="field">
									<span class="active">Banco <svg><use xlink:href="#seta-elemento"></use></svg></span>
									<select autocomplete="off" name="dadosBancarios[bancoCod]" id="select-banco" required="required">
										<option value="0">Selecione...</option>
										<option value="001">001 - Banco do Brasil</option>
										<option value="003">003 - Banco da Amazônia</option>
										<option value="004">004 - Banco do Nordeste</option>
										<option value="021">021 - Banestes</option>
										<option value="025">025 - Banco Alfa</option>
										<option value="027">027 - Besc</option>
										<option value="029">029 - Banerj</option>
										<option value="031">031 - Banco Beg</option>
										<option value="033">033 - Banco Santander Banespa</option>
										<option value="036">036 - Banco Bem</option>
										<option value="037">037 - Banpará</option>
										<option value="038">038 - Banestado</option>
										<option value="039">039 - BEP</option>
										<option value="040">040 - Banco Cargill</option>
										<option value="041">041 - Banrisul</option>
										<option value="044">044 - BVA</option>
										<option value="045">045 - Banco Opportunity</option>
										<option value="047">047 - Banese</option>
										<option value="062">062 - Hipercard</option>
										<option value="063">063 - Ibibank</option>
										<option value="065">065 - Lemon Bank</option>
										<option value="066">066 - Banco Morgan Stanley Dean Witter</option>
										<option value="069">069 - BPN Brasil</option>
										<option value="070">070 - Banco de Brasília – BRB</option>
										<option value="072">072 - Banco Rural</option>
										<option value="073">073 - Banco Popular</option>
										<option value="074">074 - Banco J. Safra</option>
										<option value="075">075 - Banco CR2</option>
										<option value="076">076 - Banco KDB</option>
										<option value="077">077 - Banco Inter</option>
										<option value="096">096 - Banco BMF</option>
										<option value="104">104 - Caixa Econômica Federal</option>
										<option value="107">107 - Banco BBM</option>
										<option value="116">116 - Banco Único</option>
										<option value="121">116 - Agibank</option>
										<option value="151">151 - Nossa Caixa</option>
										<option value="175">175 - Banco Finasa</option>
										<option value="184">184 - Banco Itaú BBA</option>
										<option value="204">204 - American Express Bank</option>
										<option value="208">208 - Banco Pactual</option>
										<option value="212">212 - Banco Matone</option>
										<option value="213">213 - Banco Arbi</option>
										<option value="214">214 - Banco Dibens</option>
										<option value="217">217 - Banco Joh Deere</option>
										<option value="218">218 - Banco Bonsucesso</option>
										<option value="222">222 - Banco Calyon Brasil</option>
										<option value="224">224 - Banco Fibra</option>
										<option value="225">225 - Banco Brascan</option>
										<option value="229">229 - Banco Cruzeiro</option>
										<option value="230">230 - Unicard</option>
										<option value="233">233 - Banco GE Capital</option>
										<option value="237">237 - Bradesco</option>
										<option value="241">241 - Banco Clássico</option>
										<option value="243">243 - Banco Stock Máxima</option>
										<option value="246">246 - Banco ABC Brasil</option>
										<option value="248">248 - Banco Boavista Interatlântico</option>
										<option value="249">249 - Investcred Unibanco</option>
										<option value="250">250 - Banco Schahin</option>
										<option value="252">252 - Fininvest</option>
										<option value="254">254 - Paraná Banco</option>
										<option value="260">260 - Nubank</option>
										<option value="263">263 - Banco Cacique</option>
										<option value="265">265 - Banco Fator</option>
										<option value="266">266 - Banco Cédula</option>
										<option value="300">300 - Banco de la Nación Argentina</option>
										<option value="318">318 - Banco BMG</option>
										<option value="320">320 - Banco Industrial e Comercial</option>
										<option value="356">356 - ABN Amro Real</option>
										<option value="341">341 - Itau</option>
										<option value="347">347 - Sudameris</option>
										<option value="351">351 - Banco Santander</option>
										<option value="353">353 - Banco Santander Brasil</option>
										<option value="366">366 - Banco Societe Generale Brasil</option>
										<option value="370">370 - Banco WestLB</option>
										<option value="376">376 - JP Morgan</option>
										<option value="389">389 - Banco Mercantil do Brasil</option>
										<option value="394">394 - Banco Mercantil de Crédito</option>
										<option value="399">399 - HSBC</option>
										<option value="409">409 - Unibanco</option>
										<option value="412">412 - Banco Capital</option>
										<option value="422">422 - Banco Safra</option>
										<option value="453">453 - Banco Rural</option>
										<option value="456">456 - Banco Tokyo Mitsubishi UFJ</option>
										<option value="464">464 - Banco Sumitomo Mitsui Brasileiro</option>
										<option value="477">477 - Citibank</option>
										<option value="479">479 - Itaubank (antigo Bank Boston)</option>
										<option value="487">487 - Deutsche Bank</option>
										<option value="488">488 - Banco Morgan Guaranty</option>
										<option value="492">492 - Banco NMB Postbank</option>
										<option value="494">494 - Banco la República Oriental del Uruguay</option>
										<option value="495">495 - Banco La Provincia de Buenos Aires</option>
										<option value="505">505 - Banco Credit Suisse</option>
										<option value="600">600 - Banco Luso Brasileiro</option>
										<option value="604">604 - Banco Industrial</option>
										<option value="610">610 - Banco VR</option>
										<option value="611">611 - Banco Paulista</option>
										<option value="612">612 - Banco Guanabara</option>
										<option value="613">613 - Banco Pecunia</option>
										<option value="623">623 - Banco Panamericano</option>
										<option value="626">626 - Banco Ficsa</option>
										<option value="630">630 - Banco Intercap</option>
										<option value="633">633 - Banco Rendimento</option>
										<option value="634">634 - Banco Triângulo</option>
										<option value="637">637 - Banco Sofisa</option>
										<option value="638">638 - Banco Prosper</option>
										<option value="643">643 - Banco Pine</option>
										<option value="652">652 - Itaú Holding Financeira</option>
										<option value="653">653 - Banco Indusval</option>
										<option value="654">654 - Banco A.J. Renner</option>
										<option value="655">655 - Banco Votorantim</option>
										<option value="707">707 - Banco Daycoval</option>
										<option value="719">719 - Banif</option>
										<option value="721">721 - Banco Credibel</option>
										<option value="734">734 - Banco Gerdau</option>
										<option value="735">735 - Banco Pottencial</option>
										<option value="738">738 - Banco Morada</option>
										<option value="739">739 - Banco Galvão de Negócios</option>
										<option value="740">740 - Banco Barclays</option>
										<option value="741">741 - BRP</option>
										<option value="743">743 - Banco Semear</option>
										<option value="745">745 - Banco Citibank</option>
										<option value="746">746 - Banco Modal</option>
										<option value="747">747 - Banco Rabobank International</option>
										<option value="748">748 - Banco Cooperativo Sicredi</option>
										<option value="749">749 - Banco Simples</option>
										<option value="751">751 - Dresdner Bank</option>
										<option value="752">752 - BNP Paribas</option>
										<option value="753">753 - Banco Comercial Uruguai</option>
										<option value="755">755 - Banco Merrill Lynch</option>
										<option value="756">756 - Banco Cooperativo do Brasil</option>
										<option value="757">757 - KEB</option>
									</select>
								</label>

								<label for="field-agencia" class="field smart f60<?php echo $focus; ?>">
									<span>Agência <small>(sem dígito)</small></span>
									<input autocomplete="off" type="tel" name="dadosBancarios[agencia]" id="field-agencia" value="<?php echo $agencia_usuario; ?>" required>
								</label>

								<label for="field-digito-agencia" class="field smart f40<?php echo $focus; ?>">
									<span>Dígito <u>Agência</u></span>
									<input autocomplete="off" type="tel" name="dadosBancarios[digitoAgencia]" id="field-digito-agencia" value="<?php echo $agencia_digito; ?>" maxlength="2">
									<small class="info-digito">Você não precisa preencher este campo caso sua agência não possua dígito.</small>
								</label>
							</fieldset>

							<fieldset>
								<legend>Tipo de conta bancária:</legend>
								<label for="field-conta-corrente" class="radio info">
									<input autocomplete="off" type="radio" name="dadosBancarios[tipoConta]" value="1" id="field-conta-corrente"<?php echo $corrente; ?> required><span>Conta Corrente</span>
								</label>
								<label for="field-conta-poupanca" class="radio info">
									<input autocomplete="off" type="radio" name="dadosBancarios[tipoConta]" value="0" id="field-conta-poupanca"<?php echo $poupanca; ?> required><span>Conta Poupança</span>
								</label>
							</fieldset>

							<fieldset>
								<label for="field-conta" class="field smart f60<?php echo $focus; ?>">
									<span>Núm. da conta <small>(sem dígito)</small></span>
									<input autocomplete="off" type="tel" name="dadosBancarios[conta]" id="field-conta"value="<?php echo $conta_usuario; ?>" required>
								</label>

								<label for="field-digito-conta" class="field smart f40<?php echo $focus; ?>">
									<span>Dígito <u>da Conta</u></span>
									<input autocomplete="off" type="tel" name="dadosBancarios[digitoConta]" id="field-digito-conta" value="<?php echo $digito_conta; ?>" maxlength="2" required>
								</label>
							</fieldset>

							<div class="botoes">
								<a href="<?php echo get_home_url(); ?>/contratar/" class="link-voltar" data-href="<?php echo get_home_url(); ?>/contratar/">VOLTAR</a><button type="submit" class="confirmar-banco" id="submit-dados-bancarios">CONTINUAR</button>
							</div>
						</form>
					</div>
				</div><!-- .entry-content -->

			</article><!-- #post-## -->
		<?php
		endwhile; // End of the loop.
		?>

	</main><!-- #main -->
</section><!-- #primary -->

<?php
get_footer();