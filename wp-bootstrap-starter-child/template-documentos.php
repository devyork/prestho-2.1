<?php
/**
 * Template name: - Documentos - Passo 1
 */
get_header();


?>

<section id="primary" class="content-area col-sm-12 col-lg-12 contratar">
	<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
				<div class="entry-content">
					<div class="prestho-form">
						<h1>
							<strong>Para finalizar, precisamos de algumas fotos.</strong>
						</h1>

						<h2>
							Fique tranquilo, é para sua segurança.
						</h2>

						<p>
							<strong>Dicas importantes de como enviar as fotos:</strong>
						</p>

						<div class="row dicas-documentos">
							<div class="col-4 col-md-2">
								<div class="icone">
									<svg><use xlink:href="#rosto"></use></svg>
								</div>
								<p>
									Fotografe apenas seu rosto
								</p>
							</div>
							<div class="col-4 col-md-2">
								<div class="icone">
									<svg><use xlink:href="#luz"></use></svg>
								</div>
								<p>
									Escolha um local iluminado
								</p>
							</div>
							<div class="col-4 col-md-2">
								<div class="icone">
									<svg><use xlink:href="#chapeu"></use></svg>
								</div>
								<p>
									Não utilize chapéu ou boné
								</p>
							</div>
							<div class="col-4 col-md-2">
								<div class="icone">
									<svg><use xlink:href="#oculos"></use></svg>
								</div>
								<p>
									Não utilize óculos de sol ou de grau
								</p>
							</div>
							<div class="col-4 col-md-2">
								<div class="icone">
									<svg><use xlink:href="#doc-frente"></use></svg>
								</div>
								<p>
									As fotografias devem estar nítidas
								</p>
							</div>
							<div class="col-4 col-md-2">
								<div class="icone">
									<svg><use xlink:href="#comprovante"></use></svg>
								</div>
								<p>
									Comprovante de endereço em seu nome
								</p>
							</div>
						</div>

						<div class="botoes full">
							<a href="<?php echo get_home_url(); ?>/documentos/enviar-fotos">ENVIAR MINHAS FOTOS</a>
						</div>
					</div>
				</div><!-- .entry-content -->

			</article><!-- #post-## -->
		<?php
		endwhile; // End of the loop.
		?>

	</main><!-- #main -->
</section><!-- #primary -->

<?php
get_footer();