<?php
/**
 * Template name: - Contratar
 */
get_header();

if (isset($_REQUEST['valor'])) {
	$value = $_REQUEST['valor'];
} else {
	$value = 5000;
}



// $search = new Loan;
// $return = $search->loanReturnLimit('21252084404', 1);

if (isset($_SESSION['request']['step1']['cadastro'])) {
	$nome_usuario = $_SESSION['request']['step1']['cadastro']['nome'];
	$cpf_usuario = $_SESSION['request']['step1']['cadastro']['cpf'];
	$celular_usuario = $_SESSION['request']['step1']['cadastro']['telefones'][0]['ddd'] . $_SESSION['request']['step1']['cadastro']['telefones'][0]['numero'];

	if (isset($_SESSION['request']['step1']['cadastro']['email'])) {
		$email_usuario = $_SESSION['request']['step1']['cadastro']['email'];
	} else {
		$email_usuario = '';
	}

	if ($_SESSION['request']['step1']['solicitacao']['convenio'] == 1) {
		$inss = ' checked="checked"';
		$siape = '';
	} else {
		$inss = '';
		$siape = ' checked="checked"';
	}
	
} else {
	$nome_usuario = '';
	$cpf_usuario = '';
	$celular_usuario = '';
	$email_usuario = '';
	$inss = '';
	$siape = '';
}


?>



<section id="primary" class="content-area col-sm-12 col-lg-12 contratar">
    <main id="main" class="site-main" role="main">

        <?php
		while ( have_posts() ) : the_post(); ?>

        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

            <div class="entry-content">
                <div class="row no-gutters">
                    <div class=" d-none d-md-inline-block col-md-4">
                        <div class="tips">
                            <h2>Olá! Estamos aqui pra te <br>dar uma mãozinha.</h2>
                            <p>Está vendo as informações ao lado? Elas são muito importantes pra gente conseguir buscar
                                a melhor oportunidade de crédito pra você. <br>Por isso, é necessário que você preencha
                                os campos corretamente.</p>
                            <ul>
                                <li><strong>Seu nome</strong>, por exemplo, precisa estar completo, conforme consta em
                                    seu RG. Pra já começarmos com o pé direito!</li>
                                <li><strong>O número do seu celular</strong> também é muito importante! Os quatro
                                    últimos dígitos vão servir de senha pra você entrar na área do cliente. Além disso,
                                    é por meio dele que você receberá um SMS para confirmar e autorizar o seu Crédito
                                    Consignado. Ah, e também ofertas de novos créditos!</li>
                                <li><strong>Cadastrar seu e-mail é opcional, mas a gente indica muito!</strong> Será
                                    ótimo para termos um contato bem mais próximo. Vamos te enviar informações
                                    relevantes sobre seu crédito, dicas e novidades. O que acha?</li>
                                <li><strong>E, quanto ao seu CPF</strong>, pode ficar tranquilo! Cuidamos muito bem de
                                    seus dados. Ele é importante para conseguirmos fazer seu cadastro e consultar seu
                                    crédito disponível. Fichinha né!</li>
                            </ul>
                            <!-- <p>Se ficou alguma dúvida é só <a
                                    href="https://api.whatsapp.com/send?phone=5534999859559&text"
                                    target="_blank"><strong>mandar um zap</strong></a>.</p> -->
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4 offset-md-1">
                        <div class="prestho-form">
                            <h1>
                                <span>Vamos continuar?</span>
                                Informe seus dados:
                            </h1>

                            <form action="#" id="step1">
                                <input type="hidden" name="valor" value="<?php echo $value; ?>">
                                <?php if (isset($_REQUEST['cartao'])): ?>
                                <input type="hidden" name="cartao" value="sim" id="consultaCartao">
                                <?php else: ?>
                                <input type="hidden" name="cartao" value="nao" id="consultaCartao">
                                <?php endif ?>
                                <input type="hidden" name="path" value="<?php echo get_stylesheet_directory_uri(); ?>">
                                <fieldset>
                                    <label for="field-nome" class="field">
                                        <span>Nome Completo</span>
                                        <input autocomplete="off" type="text" name="cadastro[nome]" id="field-nome"
                                            pattern="^\S+\s+.*" value="<?php echo $nome_usuario; ?>" maxlength="100"
                                            required>
                                        <b class="response">Seu nome completo</b>
                                    </label>
                                    <label for="field-celular" class="field info">
                                        <span>Celular</span>
                                        <input autocomplete="off" type="tel" name="cadastro[celular]" id="field-celular"
                                            value="<?php echo $celular_usuario; ?>" required>
                                        <a href="#" class="open-alert-celular"></a>
                                        <b class="response">Celular inválido</b>
                                    </label>
                                    <label for="field-email" class="field opcional">
                                        <span>E-mail <i>(opcional)</i></span>
                                        <input autocomplete="off" type="email" name="cadastro[email]" id="field-email"
                                            value="<?php echo $email_usuario; ?>" maxlength="100">
                                        <b class="response">E-mail inválido</b>
                                    </label>
                                    <label for="field-cpf" class="field">
                                        <span>CPF</span>
                                        <input autocomplete="off" type="tel" pattern="\d{3}\.\d{3}\.\d{3}-\d{2}"
                                            name="cadastro[cpf]" id="field-cpf" value="<?php echo $cpf_usuario; ?>"
                                            required>
                                        <b class="response">CPF Inválido</b>
                                    </label>
                                </fieldset>
                                <fieldset>
                                    <legend style="font-weight: bold;">Eu sou:</legend>
                                    <label for="field-aposentado" class="radio info">
                                        <input autocomplete="off" type="radio" name="solicitacao[convenio]" value="1"
                                            id="field-aposentado" <?php echo $inss; ?> required><span>Aposentado ou
                                            Pensionista do INSS</span>
                                        <a href="" class="open-alert-prestho"></a>
                                    </label>
                                    <label for="field-siape" class="radio info">
                                        <input autocomplete="off" type="radio" name="solicitacao[convenio]" value="5"
                                            id="field-siape" <?php echo $siape; ?> required><span>Servidor
                                            Federal</span>
                                        <a href="" class="open-alert-prestho"></a>
                                    </label>
                                </fieldset>
                                <fieldset>
                                    <label for="field-autorizacao" class="check info">
                                        <input autocomplete="off" type="checkbox" name="autorizacao" value="Sim"
                                            id="field-autorizacao" required><span>Li e concordo com os Termos de Uso e
                                            Política de
                                            Privacidade.</span>
                                        <a href="" class="open-termos-prestho"></a>
                                    </label>
                                </fieldset>

                                <div class="botoes">
                                    <a href="<?php echo get_home_url(); ?>" class="link-voltar"
                                        data-href="<?php echo get_home_url(); ?>">VOLTAR</a><button type="submit"
                                        id="submitStep1">CONTINUAR</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div><!-- .entry-content -->

        </article><!-- #post-## -->
        <?php
		endwhile; // End of the loop.
		?>

    </main><!-- #main -->
</section><!-- #primary -->
<div id="content-termos" aria-hidden="true" style="display: none;">

</div>
<?php get_template_part( 'template-parts/content', 'script-fixed-menu' ); ?>
<?php
get_footer();
