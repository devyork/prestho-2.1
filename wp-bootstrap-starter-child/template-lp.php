<?php
/**
 * Template name: - Landing Page
 */

get_header('lp'); ?>
	<div id="help-bar"><?php get_template_part( 'template-parts/content', 'help' ); ?></div>
	<section id="primary" class="content-area col-sm-12 col-lg-12">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					
					<div class="entry-content">
						
						<div id="diferenciais-lp">
							<div class="container">
								<h3>Empréstimo Consignado online, rápido para realizar seus sonhos</h3>
								<ul class="d-flex justify-content-md-center">
									<li class="px-2">
										<span><svg class="ico-dinheiro"><use xlink:href="#dinheiro"></use></svg></span>
										<p><strong>
											Empréstimo 
											Consignado</strong></p>
									</li>
									<li class="px-2">
										<span><svg class="ico-arroba"><use xlink:href="#arroba"></use></svg></span>
										<p><strong>
											100% Digital</strong></p>
									</li>
									<li class="px-2">
										<span><svg class="ico-celular"><use xlink:href="#celular"></use></svg></span>
										<p><strong>
											Acessível pela 
											internet ou celular</strong></p>
									</li>
									<li class="px-2">
										<span><svg class="ico-burocracia"><use xlink:href="#burocracia"></use></svg></span>
										<p><strong>
											Sem burocracia 
											para negativados</strong></p>
									</li>
									<li class="px-2">
										<span><svg class="ico-cartao"><use xlink:href="#cartao"></use></svg></span>
										<p><strong>
											Cartão de Crédito 
											Consignado</strong></p>
									</li>
									<li class="px-2">
										<span><svg class="ico-aposentados"><use xlink:href="#aposentados"></use></svg></span>
										<p><strong>
											Empréstimo para 
											aposentados</strong></p>
									</li>
									<li class="px-2">
										<span><svg class="ico-consulta"><use xlink:href="#consulta"></use></svg></span>
										<p><strong>
											Sem avalista e sem 
											consulta ao SPC e Serasa</strong></p>
									</li>
									<li class="px-2">
										<span><svg class="ico-relogio"><use xlink:href="#relogio"></use></svg></span>
										<p><strong>
											Prático, seguro e rápido 
											para pensionistas</strong></p>
									</li>
									<li class="px-2">
										<span><svg class="ico-taxas"><use xlink:href="#taxas"></use></svg></span>
										<p><strong>
											Menores taxas do 
											mercado</strong></p>
									</li>
								</ul>
							</div>

						</div>

						<?php get_template_part( 'template-parts/content', 'alert' ); ?>

						<?php get_template_part( 'template-parts/desktop/content', 'texto-consignado' ); ?>

					</div><!-- .entry-content -->

				</article><!-- #post-## -->
			<?php
			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php get_template_part( 'template-parts/content', 'script-fixed-menu' ); ?>
<?php
get_footer('lp');
