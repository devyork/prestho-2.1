<?php 
	header("Access-Control-Allow-Origin: *");
	require_once('../classes/Connection.class.php');
	require_once('../classes/Loan.class.php');

	// $value = $_REQUEST['valor'] ? number_format((float)$_REQUEST['valor'], 2, '.', '') : 300;
	if ($_REQUEST['valor']) {
		$requestedValue = number_format((float)$_REQUEST['valor'], 2, '.', '');
	} else {
		$requestedValue = number_format(300, 2, '.', '');
	}
	$cpf = $_REQUEST['cpf'] ? $_REQUEST['cpf'] : null;
	$loans = array();

	if (!empty($_REQUEST['convenio'])) {
		$agreement = $_REQUEST['convenio'];
	} else {
	 	$agreement = 1;
	}

	$search = new Loan;
	$loanLimit = $search->loanReturnLimit($cpf, $agreement);
	$loanLimitObj = $loanLimit['benefits'];
	$loanLimitVal = number_format((float)$loanLimit['limit'], 2, '.', '');
	$loanOptions = $search->loanOptions($cpf, $agreement);

	if($loanLimitVal == null || $loanLimitVal == 0.00){
		$code = -1;
	} else if ($requestedValue > $loanLimitVal) {
		$code = 0;
	} else {
		$code = 1;
	}

	if (!empty($loanOptions[0])) {
		foreach ($loanOptions[0] as $key => $value) {
			array_push($loans, array_values($value));
		}
	}

	$return = array(
				'code' => $code, 
				'request' => $requestedValue, 
				'limit' => number_format((float)$loanLimitVal, 2, '.', ''),
				'numResultados' => $loanOptions['numResultados'],
				'conjuntoId' => $loanOptions['conjuntoId'],
				'benefits' => $loanOptions[0],
				'benefitsApp' => $loans,
				'raw' => $loanOptions,
				'agreementID' => $agreement,
				'postData' => $_REQUEST,
				'loanLimitObj' => $loanLimitObj
			  );

	echo json_encode($return, JSON_PRETTY_PRINT);
	

?>