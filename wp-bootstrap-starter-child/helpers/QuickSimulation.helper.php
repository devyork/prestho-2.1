<?php 
	header("Access-Control-Allow-Origin: *");
	session_start();

	require_once('../classes/Connection.class.php');
	require_once('../classes/Loan.class.php');

	$value = $_REQUEST['valor'] ? $_REQUEST['valor'] : 5000;
	$agreement = $_REQUEST['convenio'] ? $_REQUEST['convenio'] : 1;

	$search = new Loan;
	echo json_encode($search->loanSimulation($value, $agreement), JSON_PRETTY_PRINT);

?>