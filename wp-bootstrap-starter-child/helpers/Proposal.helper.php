<?php 
	header("Access-Control-Allow-Origin: *");
	session_start();

	require_once('../classes/Connection.class.php');
	require_once('../classes/Loan.class.php');

	$cadastro = $_SESSION['request']['step1']['cadastro'];
	$conjuntoId = '';
	$simulacoes = array();
	$formaPagamentoId = 1;
	$dadosBancarios = $_SESSION['request']['step2']['dadosBancarios'];

	if ($dadosBancarios['tipoConta'] == 1) {
		$contaCorrente = true;
	} else {
		$contaCorrente = false;
	}

	foreach ($_SESSION['request']['step1']['simulacoes'] as $key => $value) {
		$conjuntoId = $value['conjuntoId'];

		// if ($value['valorSeguro'] == 'null' || $value['valorSeguro'] == '') {
		// 	$seguro = false;
		// } else {
		// 	$seguro = true;
		// }

		array_push($simulacoes, array(
			"simulacaoId" => $value['simulationId'],
			"tipoFormalizacaoId" => (int)$value['tipoFormalizacaoId'],
			"seguro" => $value['temSeguro'],
			"dadosBancarios" => array(
				"formaPagamentoId" => $formaPagamentoId,
				"bancoCod" => $dadosBancarios['banco'],
				"agencia" => $dadosBancarios['agencia'],
				"agenciaDigito" => $dadosBancarios['agenciaDigito'],
				"conta" => $dadosBancarios['conta'],
				"contaDigito" => $dadosBancarios['contaDigito'],
				"contaCorrente" => $contaCorrente

			)
		));
	}

	$proposta = array(
		"conjuntoId" => $conjuntoId,
		"simulacoes" => $simulacoes,
		"cadastro" => $cadastro
	);

	$search = new Loan;
	$returnValue = $search->sendLoanProposal($proposta);

	echo json_encode($returnValue, JSON_PRETTY_PRINT);

?>