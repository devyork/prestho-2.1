<?php 
header("Access-Control-Allow-Origin: *");
session_start();

require_once('../classes/Connection.class.php');
// require_once('../classes/File.class.php');
require_once('../classes/Loan.class.php');

/*
	5:   Comprovante
	122: Documentos
	234: Selfie
*/

$retornos = array();

if (isset($_SESSION["pedido"]["negociacao"])) {
	$negID = $_SESSION["pedido"]["negociacao"];
} elseif (isset($_SESSION["status"]["operacao"])) {
	$negID = $_SESSION["status"]["operacao"];
} elseif (isset($_REQUEST["negociacao"])) {
	$negID = $_REQUEST["negociacao"];
} else {
	$negID = 0;
}


array_push($retornos, $negID);


/*
	Upload do arquivo de aceite
*/
$nameTxt = realpath('../temp') . '/' . $_SESSION["file"];
$cfile = curl_file_create($nameTxt);

$dadosEnvioTxt = array(
	"tipoDocumentoId" => 234, 
	"aceite" => $cfile
);

$jsonDataTxt = $dadosEnvioTxt;

$sendTxt = new Loan;
$returnTxt = $sendTxt->sendTextFile($negID, $jsonDataTxt);

array_push($retornos, $returnTxt);

unlink($nameTxt);

echo json_encode($retornos, JSON_UNESCAPED_UNICODE);

?>