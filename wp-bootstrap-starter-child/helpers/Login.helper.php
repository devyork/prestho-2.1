<?php 
	header("Access-Control-Allow-Origin: *");
	require_once('../classes/Connection.class.php');
	require_once('../classes/User.class.php');

	$user = $_REQUEST['usuario'] ? $_REQUEST['usuario'] : '';
	$password = $_REQUEST['senha'] ? $_REQUEST['senha'] : '';

	$login = new User;
	$returnValue = $login->checkCredentials($user, $password);

	echo json_encode($returnValue, JSON_PRETTY_PRINT);

?>