<?php 
header("Access-Control-Allow-Origin: *");
@ini_set( 'upload_max_size' , '256M' );
@ini_set( 'post_max_size', '256M');
@ini_set( 'max_execution_time', '300' );

require_once('../classes/Connection.class.php');
require_once('../classes/File.class.php');
require_once('../classes/Loan.class.php');

$retornos = array();
$log = array();

$negID = $_REQUEST["negociacao"];
$docID = $_REQUEST["tipoDocumentoId"];
$label = $_REQUEST["label"];
$fileUploaded = $_REQUEST["file"];

/*
	Função para gerar nome randômico
*/ 
$n=10; 
function getName($n) { 
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
    $randomString = ''; 
  
    for ($i = 0; $i < $n; $i++) { 
        $index = rand(0, strlen($characters) - 1); 
        $randomString .= $characters[$index]; 
    } 
  
    return $randomString; 
} 

/*

	Converte a imagem base64 em blob

*/ 
$image = $fileUploaded;  // your base64 encoded
$image = str_replace('data:image/jpeg;base64,', '', $image);
$image = str_replace(' ', '+', $image);
$imageName = getName($n).'.'.'jpg';

// Armazena a imagem criada no FTP
$file = $folderPath . $imageName;
file_put_contents($file, base64_decode($image));

// Transforma a imagem em um objeto cURL válido
$cfile = curl_file_create($imageName, 'image/jpeg', $file);

// Monta o array com os dados a serem enviados
$dadosEnvioSelfie = array(
	"tipoDocumentoId" => $docID, 
	$label => $cfile
);

// Realiza o envio dos dados para o serviço
$sendSelfie = new Loan;
$returnSelfie = $sendSelfie->sendDocuments($negID, $dadosEnvioSelfie);

// Remove a imagem armazenada no FTP
unlink($file);

// Retorna os dados no formato jSON
echo json_encode($returnSelfie, JSON_UNESCAPED_UNICODE);

/*
	Debug
*/

// array_push($retornos, $dadosEnvioSelfie);
// array_push($retornos, $negID);
// array_push($retornos, $docID);
// array_push($retornos, $returnSelfie);

// $upload_dir = realpath('../temp') . '/';
// $random_name= 'teste' . "."."txt";
// $my_file = $upload_dir.$random_name;

// $file = new File;
// $handle = $file->createTxtFile($my_file, json_encode($retornos, JSON_UNESCAPED_UNICODE));

?>