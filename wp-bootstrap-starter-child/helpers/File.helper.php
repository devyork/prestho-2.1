<?php
	header("Access-Control-Allow-Origin: *");
	require_once('../classes/File.class.php');
	session_start();

	date_default_timezone_set('America/Sao_Paulo');

	$ip = $_SERVER['REMOTE_ADDR'] ? $_SERVER['REMOTE_ADDR'] : null;

	$start2 = date('d-m-Y H:i:s', time());
	$dStart2 = new DateTime($start2, new DateTimeZone('America/Sao_Paulo'));
	$dStart2->modify('-1 hours');
	$alt = (array)$dStart2;
	$alt = $alt['date'];

	$timeRequest = date('d-m-Y H:i:s', strtotime($alt));
	// $timeRequest = date('d/m/Y h:i:s a', time());
	$name = $_REQUEST['nome'] ? $_REQUEST['nome'] : null;
	$phone = $_REQUEST['celular'] ? $_REQUEST['celular'] : null;
	$email = $_REQUEST['email'] ? $_REQUEST['email'] : null;
	$cpf = $_REQUEST['cpf'] ? $_REQUEST['cpf'] : null;
	$agreement = $_REQUEST['convenio'] ? $_REQUEST['convenio'] : null;
	// $benefit = $_REQUEST['beneficio'] ? $_REQUEST['beneficio'] : null;
	// $benefitValue = $_REQUEST['valorBeneficio'] ? $_REQUEST['valorBeneficio'] : null;
	$authorization = $_REQUEST['autorizacao'] ? $_REQUEST['autorizacao'] : null;
	$path = $_REQUEST['path'] ? $_REQUEST['path'] : null;

	$fileName = str_replace(array('-', '.'), '', $cpf) . '-' . $ip;

	switch ($agreement) {
		case 1:
			$agreement = "INSS";
			break;

		case 5:
			$agreement = "SIAPE";
			break;
		
		default:
			$agreement = "Não identificado";
			break;
	}

	$data = "[".$ip."] - " . $timeRequest;
	$data .= "\n NOME: ".$name;
	$data .= "\n TELEFONE: ".$phone;
	$data .= "\n EMAIL: ".$email;
	$data .= "\n CPF: ".$cpf;
	// $data .= "\n NÚMERO DO BENEFÍCIO: ".$benefit;
	// $data .= "\n VALOR DO BENEFÍCIO: ".$benefitValue;
	$data .= "\n AUTORIZAÇÃO PARA RECEBIMENTO DE MENSAGENS: ".$authorization;


	$upload_dir = realpath('../temp') . '/';
	$random_name= $fileName . "-" . "."."txt";
	$my_file = $upload_dir.$random_name;

	$_SESSION['file'] = $random_name;

	$file = new File;
	$handle = $file->createTxtFile($my_file, $data);

?>