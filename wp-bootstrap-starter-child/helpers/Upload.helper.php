<?php 
header("Access-Control-Allow-Origin: *");
session_start();

require_once('../classes/Connection.class.php');
// require_once('../classes/File.class.php');
require_once('../classes/Loan.class.php');

/*
	5:   Comprovante
	122: Documentos
	234: Selfie
*/

$retornos = array();

if (isset($_SESSION["pedido"]["negociacao"])) {
	$type = "Negociacao";
	$negID = $_SESSION["pedido"]["negociacao"];

	
	array_push($retornos, $negID);

} elseif (isset($_SESSION["operacao"])) {
	$type = "Operacao";
	$opID = $_SESSION["operacao"];

	
	array_push($retornos, $opID);

} else {
	$type = "Negociacao";
	$negID = 0;

	
	array_push($retornos, $negID);
}





/*
	Upload do arquivo de aceite
*/
// $nameTxt = realpath('../temp') . '/' . $_SESSION["file"];
// $cfile = curl_file_create($nameTxt);

// $dadosEnvioTxt = array(
// 	"tipoDocumentoId" => 234, 
// 	"aceite" => $cfile
	
// );

// $jsonDataTxt = $dadosEnvioTxt;

// $sendTxt = new Loan;
// $returnTxt = $sendTxt->sendTextFile($negID, $jsonDataTxt);

// unlink($nameTxt);


/*
	Upload da Selfie
*/
if(isset($_FILES['foto-rosto']["name"])) {
	$name1 = $_FILES['foto-rosto']["name"];
	$cfile = curl_file_create($_FILES['foto-rosto']["tmp_name"], $_FILES['foto-rosto']["type"], $name1);

	$dadosEnvioSelfie = array(
		"tipoDocumentoId" => 240, 
		"selfie" => $cfile
		
	);

	$jsonDataSelfie = $dadosEnvioSelfie;
	$sendSelfie = new Loan;

	if ($type == "Operacao") {
		$returnSelfie = $sendSelfie->sendDocumentsByOp($opID, $jsonDataSelfie);
	} else {
		$returnSelfie = $sendSelfie->sendDocuments($negID, $jsonDataSelfie);
	}
	

	array_push($returnSelfie, array('rd_return' => 'selfie'));
	array_push($retornos, $returnSelfie);
}



/*
	Upload Documentos
*/
if(isset($_FILES['identidadeFrente']["name"])) {
	$name2 = $_FILES['identidadeFrente']["name"];
	$cfile2 = curl_file_create($_FILES['identidadeFrente']["tmp_name"], $_FILES['identidadeFrente']["type"], $name2);

	// Era 122. Alterado em 15/05/202 às 18:20
	$dadosEnvioDocs = array(
		"tipoDocumentoId" => 238,
		"documento" => $cfile2
	);
	
	$jsonDataDocs = $dadosEnvioDocs;
	
	$sendDocs = new Loan;

	if ($type == "Operacao") {
		$returnDocs = $sendDocs->sendDocumentsByOp($opID, $jsonDataDocs);
	} else {
		$returnDocs = $sendDocs->sendDocuments($negID, $jsonDataDocs);
	}

	
	
	array_push($returnDocs, array('rd_return' => 'documentoFrente'));
	array_push($retornos, $returnDocs);
}

if(isset($_FILES['identidadeVerso']["name"])) {
	$name3 = $_FILES['identidadeVerso']["name"];
	$cfile3 = curl_file_create($_FILES['identidadeVerso']["tmp_name"], $_FILES['identidadeVerso']["type"], $name3);

	// Era 122. Alterado em 15/05/202 às 18:20 
	$dadosEnvioDocs = array(
		"tipoDocumentoId" => 238,
		"documento2" => $cfile3
	);

	$jsonDataDocs = $dadosEnvioDocs;

	$sendDocs = new Loan;

	if ($type == "Operacao") {
		$returnDocs = $sendDocs->sendDocumentsByOp($opID, $jsonDataDocs);
	} else {
		$returnDocs = $sendDocs->sendDocuments($negID, $jsonDataDocs);
	}
	

	array_push($returnDocs, array('rd_return' => 'documentoVerso'));
	array_push($retornos, $returnDocs);
	
}







/*
	Upload Comprovante de residência
*/

if(isset($_FILES['comprovanteResidencia']["name"])) {
	$name4 = $_FILES['comprovanteResidencia']["name"];

	$cfile4 = curl_file_create($_FILES['comprovanteResidencia']["tmp_name"], $_FILES['comprovanteResidencia']["type"], $name4);

	$dadosEnvioComprovante = array(
		"tipoDocumentoId" => 5, 
		"comprovante" => $cfile4
		
	);

	$jsonDataComprovante = $dadosEnvioComprovante;

	$sendCompr = new Loan;

	if ($type == "Operacao") {
		$returnCompr = $sendCompr->sendDocumentsByOp($opID, $jsonDataComprovante);
	} else {
		$returnCompr = $sendCompr->sendDocuments($negID, $jsonDataComprovante);
	}
	

	array_push($returnCompr, array('rd_return' => 'comprovante'));
	array_push($retornos, $returnCompr);
}






/*
	Caso seja SIAPE, envia também o contracheque
*/
if (isset($_FILES['contracheque']["name"])) {

	$name5 = $_FILES['contracheque']["name"];
	$cfile5 = curl_file_create($_FILES['contracheque']["tmp_name"], $_FILES['contracheque']["type"], $name5);

	$dadosEnvioConreacheque = array(
		"tipoDocumentoId" => 216, 
		"contracheque" => $cfile5
	);

	$jsonDataContracheque = $dadosEnvioConreacheque;

	$sendExtra = new Loan;

	if ($type == "Operacao") {
		$returnExtra = $sendExtra->sendDocumentsByOp($opID, $jsonDataContracheque);
	} else {
		$returnExtra = $sendExtra->sendDocuments($negID, $jsonDataContracheque);
	}
	

	array_push($returnExtra, array('rd_return' => 'contracheque'));
	array_push($retornos, $returnExtra);


}



echo json_encode($retornos, JSON_UNESCAPED_UNICODE);


// $upload_dir = realpath('../temp') . '/';
// $random_name= 'teste-prod' . "."."txt";
// $my_file = $upload_dir.$random_name;

// $file = new File;
// $handle = $file->createTxtFile($my_file, json_encode($dadosEnvioSelfie, JSON_UNESCAPED_UNICODE));

// session_unset();
// session_destroy();


?>