<?php 
	header("Access-Control-Allow-Origin: *");
	require_once('../classes/Connection.class.php');
	require_once('../classes/Loan.class.php');

	$value = $_REQUEST['valor'] ? number_format((float)$_REQUEST['valor'], 2, '.', '') : 300;
	$cpf = $_REQUEST['cpf'] ? $_REQUEST['cpf'] : null;
	if (!empty($_REQUEST['convenio'])) {
		$agreement = $_REQUEST['convenio'];
	} else {
	 	$agreement = 1;
	}

	$search = new Loan;
	$loanLimit = $search->loanReturnLimit($cpf, $agreement, array(22, 28));
	$loanLimit = number_format((float)$loanLimit['limit'], 2, '.', '');
	$loanOptions = $search->loanOptions($cpf, $agreement, array(22, 28));

	if($loanLimit == null || $loanLimit == 0.00){
		$code = -1;
	} else if ($value > $loanLimit) {
		$code = 0;
	} else {
		$code = 1;
	}

	$return = array(
				'code' => $code, 
				'request' => number_format((float)$value, 2, '.', ''), 
				'limit' => number_format((float)$loanLimit, 2, '.', ''),
				'numResultados' => $loanOptions['numResultados'],
				'conjuntoId' => $loanOptions['conjuntoId'],
				'benefits' => $loanOptions[0],
				'raw' => $loanOptions,
				'agreementID' => $agreement,
				'postData' => $_REQUEST
			  );

	echo json_encode($return, JSON_PRETTY_PRINT);
	

?>