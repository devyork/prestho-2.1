<?php 
	header("Access-Control-Allow-Origin: *");
	require_once('../classes/Connection.class.php');
	require_once('../classes/Loan.class.php');

	$conjID = $_REQUEST['conjuntoID'] ? $_REQUEST['conjuntoID'] : '';
	$simID = $_REQUEST['simulacaoID'] ? $_REQUEST['simulacaoID'] : '';
	$valueType = $_REQUEST['tipoValor'] ? $_REQUEST['tipoValor'] : '';
	$value = $_REQUEST['valor'] ? number_format((float)$_REQUEST['valor'], 2, '.', '') : 300;

	$search = new Loan;
	$returnValue = $search->changeLoanValue($valueType, $value, $conjID, $simID);

	echo json_encode($returnValue, JSON_PRETTY_PRINT);

?>