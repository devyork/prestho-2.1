<?php 
	header("Access-Control-Allow-Origin: *");
	session_start();
	require_once('../classes/Connection.class.php');
	require_once('../classes/User.class.php');

	$token = $_REQUEST['token'] ? $_REQUEST['token'] : '';
	$_SESSION['token'] = $token;

	$registerData = new User;
	$returnValue = $registerData->dataAccount($token);
	$returnValue = json_decode(json_encode($returnValue), True);

	$loanData = new User;
	$returnLoan = $loanData->dataLoan($token);
	$returnLoan = json_decode(json_encode($returnLoan), True);

	if (is_array($returnValue[0])) {
		$userData = $returnValue[0];
	} else {
		$userData = $returnValue;
	}
	

	$_SESSION['nome'] = $userData['nome'];
	$_SESSION['cpf'] = substr($userData['cnpjCpf'], 3);
	$_SESSION['endereco'] = $userData['tipoLogradouro'] . " " . $userData['logradouro'] . ", " . $userData['numero'] . " " . $userData['complemento'] . "<br>" . $userData['cidade'] . "/" . $userData['uf'];
	$_SESSION['telefone'] = "(" . $userData['telefones'][0]['ddd'] . ") " . $userData['telefones'][0]['numero'];
	$_SESSION['emprestimos'] = $returnLoan;

	print_r($_SESSION);

?>