<p>A plataforma www.prestho.com.br pertence à PRESTHO DIGITAL LTDA, CNPJ: 10.299.040/0001-30, com sede na Avenida Rondon
    Pacheco, 3338 CEP: 38408-404, Uberlândia, Minas Gerais, Brasil. Para entrar em contato, utilize o e-mail:
    contato@prestho.com.br.</p>

<p>A Prestho apenas intermedia as operações de consignado, entre os usuários e os bancos parceiros. A análise de crédito
    e a liberação do valor na conta do cliente é feita única e exclusivamente pelos bancos parceiros. A aprovação de
    créditos consignados descritos nesta plataforma está sujeita a margem consignada e averbação por parte do orgão
    pagador. As taxas de juros praticadas no empréstimo consignado e no cartão de crédito consignado são determinadas
    pelos bancos parceiros, conforme suas políticas e serão, sempre confirmadas e exibidas antes de qualquer finalização
    de contratação. A quantidade de parcelas para pagamento do empréstimo no convenio INSS será de 72 nas contratações
    feitas pela plataforma ou App.</p>

<?php /*
<p>Exemplo representativo utilizando as taxas máximas de contratação para aposentados e pensionistas do INSS: um
    empréstimo de R$ 1.000,00, parcelado em 72 meses, terá parcelas de R$ 28,57, com um valor final do contrato de R$
    2.057,04, tendo como dados adicionais para esse exemplo o IOF, com o valor de R$ 31,24, sem tarifa de cadastro,
    juros de 2,08% a.m. (28,0230% a.a.) e o CET, de 2,19% a.m. (30,13% a.a).</p>

<p>Taxas do cartão consignado: 3,00% a.m / 43,28% a.a / CET: 3,63% a.m / CET: 54,24% a.a. Sem taxa de anuidade. O valor
    do saque do limite do cartão será financiado pelo emissor, estando sujeito à cobrança de encargos e tarifas que
    serão previamente informadas ao titular e descrito na respectiva fatura.</p>
*/ ?>

<p>Exemplo representativo utilizando as taxas máximas de contratação para aposentados e pensionistas do INSS: um empréstimo de R$ 1.000,00, parcelado em 84 meses, terá parcelas de R$ 23,46, com um valor final do contrato de R$ 1.970,64, tendo como dados adicionais para esse exemplo o IOF, sem tarifa de cadastro, juros de 1,78% a.m. (23,58% a.a.) e o CET, de 1,80% a.m. (23,94% a.a).</p>

<p>Taxas do cartão consignado: 2,70% a.m / 38,28% a.a / CET: 2,70% a.m / CET: 38,28% a.a. Sem taxa de anuidade. O valor do saque do limite do cartão será financiado pelo emissor, estando sujeito à cobrança de encargos e tarifas que serão previamente informadas ao titular e descrito na respectiva fatura.</p>

<p>Utilize seu crédito de forma consciente e somente em caso de necessidade.</p>

<p>Os produtos oferecidos estão sujeitos a alterações utilizando taxas mais vantajosas para o cliente, a depender de
    fatores determinados pelos bancos parceiros.</p>
