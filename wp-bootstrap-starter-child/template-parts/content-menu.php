<nav class="menu-mobile">
    <?php if(!is_page( array('documentos', 'enviar-fotos') )): ?>
    <a href="<?php echo get_home_url(); ?>" class="link-voltar">
        <svg><use xlink:href="#seta-navegacao"></use></svg>
    </a>
    <?php else: ?>
    <a href="#" class="link-voltar" style="opacity: 0; visibility: hidden;">
        <svg><use xlink:href="#seta-navegacao"></use></svg>
    </a>
    <?php endif; ?>

    <a href="<?php echo get_home_url(); ?>/meus-emprestimos/" class="link-saiba-mais">Saiba Mais</a>

    <?php if(!is_page( array('documentos', 'enviar-fotos') )): ?>
    <a href="<?php echo get_home_url(); ?>" class="link-home">
        <svg><use xlink:href="#logo-prestho"></use></svg>
    </a>
    <?php else: ?>
        <a href="#" class="link-home">
            <svg><use xlink:href="#logo-prestho"></use></svg>
        </a>
    <?php endif; ?>

    <a href="<?php echo get_home_url(); ?>/meus-emprestimos/" class="link-area">Minha Conta</a>

    <a href="#" class="link-seguro">
        <svg><use xlink:href="#cadeado"></use></svg>SEGURO
    </a>
    
    <?php
    /*
    wp_nav_menu(array(
    'theme_location'    => 'primary',
    'container'       => 'div',
    'container_id'    => 'main-nav',
    'container_class' => 'collapse navbar-collapse justify-content-end',
    'menu_id'         => false,
    'menu_class'      => 'navbar-nav',
    'depth'           => 3,
    'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
    'walker'          => new wp_bootstrap_navwalker()
    ));
    */
    ?>

</nav>

<div class="container p-0">
    <nav class="menu-desktop">
        <a href="<?php echo get_home_url(); ?>" class="logo-menu pl-0"><svg><use xlink:href="#logo-prestho"></use></svg></a>
        <a href="<?php echo get_home_url(); ?>/sobre-a-prestho/" class="menu-sobre">SOBRE A PRESTHO</a>
        <a href="<?php echo get_home_url(); ?>/duvidas-frequentes/" class="menu-duvidas">DÚVIDAS FREQUENTES</a>
        <a href="<?php echo get_home_url(); ?>/contato/" class="menu-contato">CONTATO</a>
        <a href="<?php echo get_home_url(); ?>/blog/" class="menu-blog">BLOG</a>
        <a href="<?php echo get_home_url(); ?>/noticias/" class="menu-noticias">NOTÍCIAS</a>
        <a class="selo-menu align-self-end"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/svg/selo.svg" alt="" class="selo-desktop-menu"></a></a>
        <a href="<?php echo get_home_url(); ?>/meus-emprestimos/" class="menu-conta">Minha Conta</a>
        <a href="<?php echo get_home_url(); ?>" class="faca-simulacao-menu">Faça uma Simulação</a>
        <a href="#" onclick="return false" class="link-seguro mr-0 pr-0"><svg><use xlink:href="#cadeado"></use></svg></a>

    </nav>
</div>