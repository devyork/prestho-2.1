<?php 

	/*
		Essa lista foi duplicada na Landing Page,
		pois por questões de design não foi possível
		aproveitar esse componente.
	*/ 

?>
<div class="row p-3">
	<div class="col-4 p-1">
		<span><svg class="ico-dinheiro"><use xlink:href="#dinheiro"></use></svg></span>
		<p><strong>Empréstimo <br>Consignado</strong></p>
	</div>
	<div class="col-4 p-1">
		<span><svg class="ico-arroba"><use xlink:href="#arroba"></use></svg></span>
		<p><strong>100% Digital</strong></p>
	</div>
	<div class="col-4 p-1">
		<span><svg class="ico-celular"><use xlink:href="#celular"></use></svg></span>
		<p><strong>Acessível pela internet ou celular</strong></p>
	</div>
	<div class="col-4 p-1">
		<span><svg class="ico-burocracia"><use xlink:href="#burocracia"></use></svg></span>
		<p><strong>Sem burocracia para negativados</strong></p>
	</div>
	<div class="col-4 p-1">
		<span><svg class="ico-cartao"><use xlink:href="#cartao"></use></svg></span>
		<p><strong>Cartão de Crédito Consignado</strong></p>
	</div>
	<div class="col-4 p-1">
		<span><svg class="ico-aposentados"><use xlink:href="#aposentados"></use></svg></span>
		<p><strong>Empréstimo para aposentados</strong></p>
	</div>
	<div class="col-4 p-1">
		<span><svg class="ico-consulta"><use xlink:href="#consulta"></use></svg></span>
		<p><strong>Sem avalista e sem consulta ao SPC e Serasa</strong></p>
	</div>
	<div class="col-4 p-1">
		<span><svg class="ico-relogio"><use xlink:href="#relogio"></use></svg></span>
		<p><strong>Prático, seguro e rápido para pensionistas</strong></p>
	</div>
	<div class="col-4 p-1">
		<span><svg class="ico-taxas"><use xlink:href="#taxas"></use></svg></span>
		<p><strong>Menores taxas do mercado</strong></p>
	</div>
</div>