<div id="institucional-consignado">
  <div class="container">
    <div class="row">
      <div class="col-md-6 px-5">
        <h4>O que é o Empréstimo Consignado?</h4>
        <p>O Empréstimo Consignado é a modalidade de crédito pessoal em que o valor das parcelas é descontado automática e mensalmente no contracheque ou no benefício do INSS.</p>
        <p><strong>Confira algumas vantagens:</strong></p>
        <ul>
          <li><p>É o empréstimo pessoal mais barato do Brasil: taxas de juros de no máximo 1,80% para INSS e 2,05% para Federal (ao mês).</p></li>
          <li><p>Sem consulta ao SPC e Serasa;</p></li>
          <li><p>Crédito fácil e sem burocracia: é possível contratar online com poucos cliques;</p></li>
          <li><p>Parcelas fixas do início ao fim do contrato;</p></li>
          <li><p>Prazos para pagamento de até 96 meses para Servidor Federal e 84 meses para Aposentado e Pensionista do INSS.</p></li>
        </ul>
      </div>
      <div class="col-md-6 px-5">
        <h4>Quem pode contratar o Consignado na Prestho?</h4>
        <p>Servidores Públicos Federais (SIAPE), Aposentados e Pensionistas do INSS, que além do empréstimo também podem contratar o Cartão de Crédito Consignado mesmo negativados (o crédito é liberado sem consulta ao SPC ou SERASA).</p>

        <h4>O que é Cartão de Crédito Consignado?</h4>
        <p>Ele funciona como o cartão de crédito comum, porém com muito mais benefícios e com uma taxa até 5x vezes inferior aos demais cartões. Não tem taxa de anuidade, nem adesão. Pode ser usado para fazer compras no comércio à vista ou parceladas, saques em dinheiro e até mesmo pagar contas. É um cartão em que o valor mínimo da fatura é descontado automaticamente no contracheque ou no benefício do INSS.</p>
        
      </div>
    </div>
  </div>
</div>