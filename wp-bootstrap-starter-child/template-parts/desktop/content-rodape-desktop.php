<div id="rodape-desktop">
    <div class="container">
        <div class="row">
            <div class="col-md-6 pl-md-2 area1">
                <svg>
                    <use xlink:href="#logo-prestho"></use>
                </svg>
                <?php get_template_part( 'template-parts/content', 'links-privacidade' ); ?><br><br>
                <?php get_template_part( 'template-parts/content', 'site-info' ); ?>

                <!-- <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/svg/selo.svg?v=2" alt="" class="selo-desktop"></a> -->

            </div>
            <div class="col-md-6 d-flex justify-content-end align-items-end px-md-4">
                <ul class="redes-sociais-rp-desktop">
                    <li><a href="https://www.facebook.com/Prestho.Digital/" target="_blank"><img
                                src="<?php echo get_stylesheet_directory_uri(); ?>/images/desktop/fb.svg" alt=""></a>
                    </li>
                    <li><a href="https://www.instagram.com/prestho.digital/" target="_blank"><img
                                src="<?php echo get_stylesheet_directory_uri(); ?>/images/desktop/insta.svg" alt=""></a>
                    </li>
                    <!-- <li><a href="https://api.whatsapp.com/send?phone=5534999859559&text" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/desktop/whatsapp-share.svg" alt=""></a></li> -->
                </ul>
            </div>
            <div class="col-md-12 p-0 area3">
                <?php get_template_part( 'template-parts/content', 'menu' ); ?>
            </div>
            <div class="col-md-12 px-md-2 area2">
                <?php get_template_part( 'template-parts/content', 'legal' ); ?>
            </div>
        </div>
    </div>
</div>
