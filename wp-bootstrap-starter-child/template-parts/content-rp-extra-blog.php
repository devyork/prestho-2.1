<!-- barra ações -->
<div id="rp-extra-blog">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-4 d-flex align-items-center">
				<h3>Acesse sua área
				na Prestho e
				acompanhe a 
				situação de suas
				solicitações de
				empréstimo.</h3>
			</div>
			<div class="col-12 col-md-4 d-flex align-items-center">
				<div>
					<p>Saiba sobre os valores disponíveis e taxas reais aplicadas e finalize seu pedido sem compromisso. </p>
					<p>A autorização para a contratação deve ser dada por você. </p>
					<p><strong>É seguro!</strong></p>
				</div>
			</div>
			<div class="col-12 col-md-4 d-flex align-items-center">
				<div class="container-faca-simulacao">
					<a href="#" class="faca-simulacao-extra">Faça uma simulação grátis</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- fim da barra ações -->