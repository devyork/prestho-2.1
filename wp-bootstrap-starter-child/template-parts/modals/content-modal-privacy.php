<!-- Modal Política de privacidade -->
<div class="modal fade" id="privacy-policy-modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static"
    data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="swal-icon swal-icon--info"></div>
                <h5>Termos de uso e políticas de privacidade da plataforma de serviços financeiros e crédito consignado
                    - Prestho Digital LTDA</h5>
            </div>
            <div class="modal-body">
                <?php
                  function get_page_ID_by_slug( $slug ) {
                    $page = get_page_by_path( $slug );
                    if ( $page ) {
                      return (int) $page->ID;
                    }
                    else {
                      return null;
                    }
                  }


                  $args = array(
                    'page_id' => get_page_ID_by_slug('termos-de-uso-politicas-de-privacidade')
                  );

                  $query_policypage = new WP_Query( $args );
                  while ( $query_policypage->have_posts() ) : $query_policypage->the_post();
                    the_content();
                  endwhile;
                  // reset post data (important!)
                  wp_reset_postdata();
                ?>
            </div>
            <div class="modal-footer">
                <a href="#" data-dismiss="modal" aria-label="Voltar">VOLTAR</a>
            </div>
        </div>
    </div>
</div>
