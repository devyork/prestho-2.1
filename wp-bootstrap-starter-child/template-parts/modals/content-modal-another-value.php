<!-- Modal outro valor -->
<div class="modal fade fundo-amarelo" id="outro-valor" tabindex="-1" role="dialog" aria-labelledby="outro-valor-abel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <a href="<?php echo get_home_url(); ?>" class="link-home">
            <svg><use xlink:href="#logo-prestho"></use></svg>
        </a>
      </div>
      <div class="modal-body">
        <h5>Fala pra gente.</h5>
        <h6>Qual valor de empréstimo 
        te ajudaria neste momento?</h6>
        <p>Digite o valor de que necessita:</p>
        <div id="campo-outro-valor">
          <span><input type="tel" data-min="300,00" data-max="85.000,00" placeholder="0,00"></span>
        </div>
        <p class="multilinha limites"><strong>Você pode escolher um novo valor 
entre R$300,00 e R$85.000,00.</strong></p>
    <p class="multilinha chamada">
      É rapidinho! Em 5 minutos você pode
      ter a ajuda de que precisa!
    </p>
    <p class="multilinha informacao">
      Atenção! A Prestho não solicita depósito antecipado para 
      empréstimo. Em caso de dúvida, entre em contato.
    </p>
    <a href="#" class="simular">Simular</a>
      </div>
    </div>
  </div>
</div>