<!-- Modal outro valor -->
<div class="modal fade fundo-amarelo" id="cadastro2" tabindex="-1" role="dialog" aria-labelledby="cadastro2-label" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <a href="<?php echo get_home_url(); ?>" class="link-home">
            <svg><use xlink:href="#logo-prestho"></use></svg>
        </a>
      </div>
      <div class="modal-body prestho-form">
        <h5>Certo<span></span>.</h5>
        <h6>Como você é Aposentado ou Pensionista do INSS, precisamos que confirme abaixo as informações para continuarmos com sua proposta de crédito consignado:</h6>
        <form action="#" id="stepBenefit">
          <fieldset>
            <label for="field-beneficio" class="field info">
              <span>Número do benefício</span>
              <input autocomplete="off" type="tel" name="beneficio" id="field-beneficio" value="" required>
              <a href="#" class="open-alert-beneficio"></a>
              <b class="response">Numero inválido</b>
            </label>
            <label for="field-valor" class="field opcional">
              <span>Valor do benefício</span>
              <input autocomplete="off" type="tel" name="valorBeneficio" id="field-valor" value="">
              <b class="response">Valor inválido</b>
            </label>
          </fieldset>        
          <div class="botoes">
            <a href="#" class="link-voltar" data-dismiss="modal" aria-label="Voltar">VOLTAR</a><button type="submit" id="submitBenefitNumber">CONTINUAR</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>