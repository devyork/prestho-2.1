<!-- Alterar e-mail -->
<div class="modal fade fundo-amarelo" id="trocar-email" tabindex="-1" role="dialog" aria-labelledby="outro-valor-abel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <a href="<?php echo get_home_url(); ?>" class="link-home">
            <svg><use xlink:href="#logo-prestho"></use></svg>
        </a>
      </div>
      <div class="modal-body">
        <h5>Trocar e-mail</h5>
        <h6><br>Informe o novo endereço de e-mail. Através dele você poderá receber notificações de suas solicitações em andamento.</h6>
        <p>Digite seu endereço de e-mail:</p>
        <form action="<?php echo home_url( $wp->request ); ?>" class="prestho-form" method="post" id="form-altera-email">
          <label for="novo-email" class="field">
            <span>E-mail</span>
            <input type="email" name="altera[email]" id="novo-email" required>
            <input type="hidden" name="current" class="current" value="">
            <b class="response">E-mail inválido</b>
          </label>
        </form>
        <p class="multilinha limites">

        </p>
    <p class="multilinha informacao">
      Não se preocupe! A gente não envia Spam. 
      Não queremos lotar sua caixa de e-mail.
    </p>
    <a href="#" id="salvar-novo-email">Trocar endereço de e-mail</a>
      </div>
    </div>
  </div>
</div>