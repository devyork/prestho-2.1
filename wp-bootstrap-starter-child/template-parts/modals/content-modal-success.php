<!-- Modal upload success -->
<div class="modal fade" id="modal-upload" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <?php 

          if (isset($_SESSION['request']['cadastro']['nome'])) {
            $nome = explode(' ', $_SESSION['request']['cadastro']['nome']);
            $nome = ', ' . $nome[0] . '!';
          } else {
            $nome = "!";
          }

        ?>
        <h5 class="icon">Supimpa<?php echo $nome; ?><br>Sua solicitação foi concluída.</h5>
      </div>
      <div class="modal-body">
        <ul>
          <li>
            <p><span>Aprovação da sua proposta:</span></p>
            <p>Recebemos seus documentos com sucesso. Vamos analisar e, <strong>estando tudo nos trinks</strong>, sua solicitação seguirá para análise de crédito do banco e, em seguida, para averbação do órgão pagador. <strong>Tudo com muita tranquilidade.</strong></p>
          </li>
          <li>
            <p><span>Depósito do seu crédito:</span></p>
            <p>Para que o depósito seja realizado na sua conta, é necessário que confirme o SMS que chegará no seu celular cadastrado. Caso o SMS não chegue em 12 horas, entre em contato conosco, através de nossos canais oficiais.</p>
          </li>
          <li>
            <p><span>Segurança:</span></p>
            <p><strong>Nunca faça nenhum depósito antecipado ou pague qualquer taxa para receber seu Empréstimo</strong>. Se receber qualquer contato com este intuito, nos informe imediatamente. <br><strong>Fique esperto!</strong></p>
          </li>
          <li>
            <p><span>Acompanhamento da sua proposta:</span></p>
            <p>Você pode acompanhar o status do seu Empréstimo pela área do cliente. Para acessar, bastar digitar o seu CPF e os 4 últimos dígitos do celular cadastrado como senha. Mantenha o aplicativo instalado em seu celular. Iremos notificá-lo a cada atualização de sua proposta. <br><strong>Fichinha, né?</strong></p>
          </li>
        </ul>

        <div class="buttons"><a href="<?php echo get_home_url(); ?>/acessar-area-cliente/" class="ir-area-cliente">Ir para área do cliente</a></div>
      </div>
    </div>
  </div>
</div>