<!-- Modal upload success -->
<div class="modal fade" id="modal-proposta" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <?php 

          if (isset($_SESSION['request']['step1']['cadastro']['nome'])) {
            $nome = explode(' ', $_SESSION['request']['step1']['cadastro']['nome']);
            $nome = ', ' . $nome[0] . '!';
          } else {
            $nome = "!";
          }

        ?>
        <h5 class="icon">Supimpa<?php echo $nome; ?><br><span>Sua solicitação foi concluída. E agora?</span></h5>
      </div>
      <div class="modal-body">
        <ul>
          <li>
            <p><span>Acesse sua Conta</span></p>
            <p>Acompanhe o status do seu Empréstimo pela área do cliente. Para acessar, bastar digitar o <strong>seu CPF e sua SENHA (que são os 4 últimos dígitos do SEU CELULAR cadastrado)</strong></p>
          </li>
          <li>
            <p><span>Acompanhamento de sua proposta</span></p>
            <p>Mantenha o aplicativo instalado em seu celular. Iremos notificá-lo a cada atualização de sua proposta. <br><strong>Fichinha, né?</strong></p>
          </li>
          <li>
            <p><span>Aprovação de sua proposta</span></p>
            <p>Para aprovação de sua proposta, envie pra gente uma foto do seu RG ou CNH e uma selfie. Vamos analisar e, estando tudo nos trinks, seguirá para análise de crédito do banco e, em seguida, para averbação do órgão pagador.<br><a href="#" class="enviar-fotos-agora">Enviar Fotos Agora</a></p>
          </li>
          <li>
            <p><span>Depósito do seu crédito</span></p>
            <p>Para que o depósito seja realizado na sua conta, é necessário que confirme o SMS que chegará no seu celular cadastrado. Caso o SMS não chegue, entre em contato conosco, através de nossos canais oficiais.</p>
          </li>
          <li>
            <p><span>Segurança</span></p>
            <p><strong>Nunca faça nenhum depósito antecipado ou pague qualquer taxa para receber seu Empréstimo.</strong> Se receber qualquer contato com este intuito, nos informe imediatamente. <br><strong>Fique esperto!</strong></p>
          </li>
        </ul>

        <div class="buttons"><a href="<?php echo get_home_url(); ?>/acessar-area-cliente/" class="ir-area-cliente">Ir para Minha Conta</a></div>
      </div>
    </div>
  </div>
</div>