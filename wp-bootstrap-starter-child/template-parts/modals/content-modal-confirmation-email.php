<!-- Modal confirmation email -->
<div class="modal fade" id="modal-confirmation-email" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <svg><use xlink:href="#checkbox"></use></svg>
      </div>
      <div class="modal-body">
        <h5>Seu endereço de e-mail foi atualizado.</h5>
        <p>Obrigado! <br>Continue utilizando a Prestho.</p>
        <a href="#" data-dismiss="modal" aria-label="Voltar">VOLTAR</a>
      </div>
    </div>
  </div>
</div> 