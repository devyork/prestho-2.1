<!-- Modal sem benefício -->
<div class="modal fade fundo-amarelo" id="sem-beneficio" tabindex="-1" role="dialog" aria-labelledby="outro-valor-abel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <a href="<?php echo get_home_url(); ?>" class="link-home">
            <svg><use xlink:href="#logo-prestho"></use></svg>
        </a>
      </div>
      <div class="modal-body">
        <h5>Sentimos muito.</h5>
        <h6>Não encontramos nenhuma oferta disponível pra você neste momento.</h6>
        <svg class="alerta"><use xlink:href="#alerta"></use></svg>
        <p>Não desanime!
        Se você é Servidor Federal, Aposentado ou Pensionista do INSS, entre em contato para tentarmos encontrar outra solução.</p>
    
        <?php get_template_part( 'template-parts/content', 'help' ); ?>

        <a class="link-voltar" href="<?php echo get_home_url(); ?>">VOLTAR</a>
      </div>
    </div>
  </div>
</div>