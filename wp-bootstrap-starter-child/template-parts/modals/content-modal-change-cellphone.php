<!-- Alterar celular -->
<div class="modal fade fundo-amarelo" id="trocar-celular" tabindex="-1" role="dialog" aria-labelledby="outro-valor-abel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <a href="<?php echo get_home_url(); ?>" class="link-home">
            <svg><use xlink:href="#logo-prestho"></use></svg>
        </a>
      </div>
      <div class="modal-body">
        <h5>Trocar número<br>
    de celular</h5>
        <h6><br>Informe o novo número de celular. Através dele você poderá receber notificações de suas solicitações em andamento.</h6>
        <p>Digite seu número de celular:</p>
        <form action="<?php echo home_url( $wp->request ); ?>" class="prestho-form" id="form-altera-celular" method="POST">
          <label for="novo-celular" class="field">
        <span>Celular</span>
        <input type="tel" name="altera[celular]" id="novo-celular" required>
        <input type="hidden" name="current" class="current" value="">
        <b class="response">Celular inválido</b>
      </label>
        </form>
        <p class="multilinha limites">

        </p>
    <p class="multilinha informacao">
      Não se preocupe! A gente entende como é ruim ligação 
      de telemarketing. Respeitamos sua tranquilidade.
    </p>
    <a href="#" id="salvar-novo-celular">Trocar número de celular</a>
      </div>
    </div>
  </div>
</div>