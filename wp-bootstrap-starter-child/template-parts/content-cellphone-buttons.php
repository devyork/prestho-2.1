<!-- lista com sugestões de crédito -->
<div id="balao-dica"></div>
<ul>
	<li>
		<a href="#" data-valor="1000">
			<span>R$</span>
			<strong>1.000</strong>
			<small>,00</small>
		</a>
	</li>
	<li>
		<a href="#" data-valor="2500">
			<span>R$</span>
			<strong>2.500</strong>
			<small>,00</small>
		</a>
	</li>
	<li>
		<a href="#" data-valor="5000">
			<span>R$</span>
			<strong>5.000</strong>
			<small>,00</small>
		</a>									
	</li>
	<li>
		<a href="#" data-valor="7500">
			<span>R$</span>
			<strong>7.500</strong>
			<small>,00</small>
		</a>
	</li>
	<li>
		<a href="#" data-valor="10000">
			<span>R$</span>
			<strong>10.000</strong>
			<small>,00</small>
		</a>
	</li>
	<li>
		<a href="#" data-valor="outro" data-toggle="modal" data-target="#outro-valor">
			<strong class="outro">Outro valor</strong>
		</a>
	</li>
</ul>
<!-- fim da lista com sugestões de crédito -->