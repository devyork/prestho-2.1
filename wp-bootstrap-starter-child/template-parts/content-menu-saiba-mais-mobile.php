<div id="menu-content">

    <button class="fecha-menu"><svg>
            <use xlink:href="#seta-navegacao"></use>
        </svg></button>

    <ul class="links">
        <!-- <li>
            <a href="#">
                <svg><use xlink:href="#notificacoes"></use></svg>NOTIFICAÇÕES
            </a>
        </li> -->
        <li>
            <a href="<?php echo get_home_url(); ?>/duvidas-frequentes/">
                <svg>
                    <use xlink:href="#icone-duvida"></use>
                </svg>DÚVIDAS FREQUENTES
            </a>
        </li>
        <li>
            <a href="<?php echo get_home_url(); ?>/contato/">
                <svg>
                    <use xlink:href="#email"></use>
                </svg>CONTATO
            </a>
        </li>
        <li>
            <a href="#" class="open-chat">
                <svg>
                    <use xlink:href="#icone-chat"></use>
                </svg>CHAT
            </a>
        </li>
        <li>
            <a href="<?php echo get_home_url(); ?>/sobre-a-prestho/">
                <svg>
                    <use xlink:href="#icone-prestho"></use>
                </svg>SOBRE A PRESTHO
            </a>
        </li>
        <li>
            <a href="<?php echo get_home_url(); ?>/blog/">
                <svg>
                    <use xlink:href="#icone-prestho"></use>
                </svg>BLOG
            </a>
        </li>
        <li>
            <a href="<?php echo get_home_url(); ?>/noticias/">
                <svg>
                    <use xlink:href="#icone-prestho"></use>
                </svg>NOTÍCIAS
            </a>
        </li>
        <li>
            <ul class="redes">
                <li>
                    <a href="https://www.facebook.com/Prestho.Digital/" target="_blank">
                        <svg>
                            <use xlink:href="#facebook"></use>
                        </svg>
                    </a>
                </li>
                <li>
                    <a href="https://www.instagram.com/prestho.digital/" target="_blank">
                        <svg>
                            <use xlink:href="#instagram"></use>
                        </svg>
                    </a>
                </li>
                <!-- <li>
                    <a href="https://api.whatsapp.com/send?phone=5534999859559&text" target="_blank">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26.415 26.415">
                            <path
                                d="M551.018,269.217a13.208,13.208,0,0,0-11.207,20.2c-.521,1.829-1.463,5.286-1.474,5.324a.506.506,0,0,0,.638.616l5.235-1.609a13.209,13.209,0,1,0,6.807-24.528Z"
                                transform="translate(-537.811 -269.217)" fill="#44de00"></path>
                            <path
                                d="M560.186,288c-.664-.37-1.231-.74-1.644-1.01-.317-.207-.544-.355-.711-.439a.823.823,0,0,0-.957.068.392.392,0,0,0-.045.054,5.058,5.058,0,0,1-1.3,1.464,8.668,8.668,0,0,1-2.215-1.436,4.793,4.793,0,0,1-1.753-2.215c.654-.672.888-1.094.888-1.576a10.243,10.243,0,0,0-1.367-2.778c-.21-.209-.683-.242-1.407-.1a.364.364,0,0,0-.184.1c-.086.088-2.136,2.177-1.162,4.71a12,12,0,0,0,2.869,4.231,8.32,8.32,0,0,0,4.441,2.3,7.514,7.514,0,0,0,1.121.088,3.316,3.316,0,0,0,3.613-3.09.358.358,0,0,0-.18-.373Z"
                                transform="translate(-540.796 -272.621)" fill="#fff"></path>
                        </svg>
                    </a>
                </li> -->
            </ul>
        </li>
    </ul>


</div>
