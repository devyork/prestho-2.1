<!-- bloco aplicativo -->
<div id="baixar-aplicativo">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-4 p-0">
                <h3>Baixe nosso aplicativo e tenha a ajuda da Prestho sempre que precisar</h3>
                <h4>Sua tranquilidade está em suas mãos.</h4>
                <p>Para ficar sossegado com as contas em dia ou cuidar da saúde, conte com a Prestho. Baixe grátis o nosso aplicativo em seu celular, contrate seu empréstimo quando precisar e acompanhe o andamento da sua proposta.</p>
                <?php get_template_part( 'template-parts/content', 'apps-links' ); ?>
            </div>
            <div class="d-md-none d-lg-block col-md-12 col-lg-8 p-0 imagem-celular">
                <!-- <a href="#" class="faca-simulacao-app"></a> -->
            </div>
        </div>
    </div>
</div>
<!-- fim do bloco aplicativo -->
