<!-- bloco celular -->
<div id="sugestoes-credito">
	<h2>Confira ofertas de crédito especiais pra você. <span>É só escolher um valor abaixo:</span></h2>

	<?php get_template_part( 'template-parts/content', 'cellphone-buttons' ); ?>

	<div id="chamada-acesse">
		Já se cadastrou na Prestho? <a href="<?php echo get_home_url(); ?>/meus-emprestimos/">Acesse sua conta</a>
	</div>
	
	<!-- destaque cartão de crédito -->
	<div id="destaque-cartao">
		<h3>Acha mais prático um
			Cartão de Crédito?
			Peça já o seu!</h3>
		<h4><span>Cartão de Crédito
			Consignado</span></h4>

		<p>Um <strong>cartão internacional</strong> completo, ideal para fazer <br>compras, pagar contas ou realizar saques em dinheiro.</p>
		<ul>
			<li><svg><use xlink:href="#checkbox"></use></svg>Grátis - sem adesão</li>
			<li><svg><use xlink:href="#checkbox"></use></svg>Sem consulta ao SPC/Serasa</li>
			<li><svg><use xlink:href="#checkbox"></use></svg>Sem taxa de anuidade</li>
		</ul>
		<div class="center">
			<a href="#">Quero Já!</a>
		</div>
	</div>
	<!-- fim do destaque cartão de crédito -->

</div>
<!-- fim do bloco celular -->