<!-- faixa alerta -->
<div class="faixa-alerta">
	<svg><use xlink:href="#alerta"></use></svg><p>Atenção! A Prestho não solicita depósito antecipado para empréstimo. <br>Em caso de dúvida, entre em contato.</p>
</div>
<!-- fim da faixa alerta -->