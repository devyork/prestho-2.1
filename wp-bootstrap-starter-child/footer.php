<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?>
<?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
</div><!-- .row -->
</div><!-- .container -->
</div><!-- #content -->
<?php get_template_part( 'footer-widget' ); ?>
<footer id="colophon" class="site-footer <?php echo wp_bootstrap_starter_bg_class(); ?>" role="contentinfo">
    <div <?php body_class("container-fluid pt-3 pb-3 container-rodape"); ?>>

        <?php get_template_part( 'template-parts/content', 'apps' ); ?>

        <?php get_template_part( 'template-parts/content', 'action-bar' ); ?>

        <?php get_template_part( 'template-parts/content', 'alert' ); ?>

        <?php get_template_part( 'template-parts/desktop/content', 'texto-consignado' ); ?>

        <?php get_template_part( 'template-parts/desktop/content', 'faixa-caregorias' ); ?>

        <?php get_template_part( 'template-parts/content', 'help' ); ?>

        <blockquote class="legal">
            <?php get_template_part( 'template-parts/content', 'legal' ); ?>
        </blockquote>

        <div class="links-privacidade">
            <?php get_template_part( 'template-parts/content', 'links-privacidade' ); ?>
        </div>
        <div class="site-info">
            <?php get_template_part( 'template-parts/content', 'site-info' ); ?>
        </div><!-- close .site-info -->

    </div>
    <?php get_template_part( 'template-parts/desktop/content', 'rodape-desktop' ); ?>
</footer><!-- #colophon -->
<?php endif; ?>
</div><!-- #page -->
</div><!-- #container-page -->

<?php get_template_part( 'template-parts/content', 'menu-saiba-mais-mobile' ); ?>

<?php wp_footer(); ?>


<!-- MODAIS -->
<?php get_template_part( 'template-parts/modals/content', 'modal-loading' ); ?>
<?php get_template_part( 'template-parts/modals/content', 'modal-success' ); ?>
<?php get_template_part( 'template-parts/modals/content', 'modal-proposal' ); ?>
<?php get_template_part( 'template-parts/modals/content', 'modal-another-value' ); ?>
<?php get_template_part( 'template-parts/modals/content', 'modal-limit-return' ); ?>
<?php get_template_part( 'template-parts/modals/content', 'modal-without-benefit' ); ?>
<?php get_template_part( 'template-parts/modals/content', 'modal-simulation-return' ); ?>
<?php get_template_part( 'template-parts/modals/content', 'modal-change-email' ); ?>
<?php get_template_part( 'template-parts/modals/content', 'modal-change-cellphone' ); ?>

<?php get_template_part( 'template-parts/modals/content', 'modal-confirmation-email' ); ?>
<?php get_template_part( 'template-parts/modals/content', 'modal-confirmation-phone' ); ?>
<?php get_template_part( 'template-parts/modals/content', 'modal-video' ); ?>
<?php get_template_part( 'template-parts/modals/content', 'modal-benefit-number' ); ?>
<?php get_template_part( 'template-parts/modals/content', 'modal-privacy' ); ?>

<!-- Script Huggy -->
<?php get_template_part( 'template-parts/content', 'script-huggy' ); ?>




</body>

</html>
